FROM node:14.16.0-alpine3.11

WORKDIR /usr/src/app

COPY package*.json npm-shrinkwrap.json ./

RUN npm install --no-optional && npm cache clean --force

COPY . .

RUN npm install pm2 -g

EXPOSE 5000

