'use strict';
var app = require('../server/server');
var sendMail = require('../helper/sendMail');
var constants = require('./constant');
var crypto = require('crypto');
var dateTime = require('node-datetime');

module.exports = {
  /*activity function start from here */
  activity: function (param, callback) {
    app.models.Activities.create({
      "activity": param.activity,
      "module": param.module,
      "moduleId": param.moduleId,
      "tenantKey": param.tenantKey,
      "user": param.user,
      "value1": param.value1,
      "value2": param.value2,
      "currentValue": param.currentValue,
      "oldValue": param.oldValue,
      "icon": param.icon,
      "note": param.note,
      "isVisible": param.isVisible ? param.isVisible : '1',
      "status": param.status,
      "createdBy": param.createdBy,
      "subModuleId": param.moduleField,
      "subModuleName": param.subModuleName,
      "dateTime": dateTime.create().format('Y-m-d H:M:S')
    }, function (err, obj) {

      if (err) return callback(err);
      return callback(null, obj);
    }
    );
  }, /*activity function end here */
  /*commonmsg function start from here */
  commonMsg: function (status, code, msg, resData, err) {
    
    var methodResponse = {};
    var data = this.responseMsg(code);
    methodResponse.status = status;
    methodResponse.code = code;
    methodResponse.msg = msg;
    methodResponse.statusMsg = data.statusMsg;
    methodResponse.description = data.description;
    
    if (code === 200 || code === 201){
      methodResponse['data'] = {};
    } else {
      methodResponse['error'] = {};
    }

    if (resData) {
      resData = JSON.stringify(resData);
      resData = JSON.parse(resData);
      methodResponse.data = resData;
    }
    if (err) {
      methodResponse.error = err;
    }

    return methodResponse;

  },
  /*commonmsg function ends here */
  /**
   * Takes one argument
   * id to Json array
   */
  convertIdToJson: function (id) {
    var jsonObj = [];

    id = id.toString();
    id = id.split(',');
    var allIds = '';

    for (var l = 0; l < id.length; l++) {

      jsonObj.push({
        "id": id[l]
      });

      allIds = JSON.stringify(jsonObj);
    }

    allIds = allIds.replace(/:\"/g, "\:");
    id = allIds.replace(/\"}/g, "}");
    return id;
    //callback(null, id);
  },
  /**
   * takes two parameter array and the element
   * Removes the duplicate from array
   */
  removeDuplicates: function (arr, prop) {
    var obj = {};
    for (var i = 0, len = arr.length; i < len; i++) {
      if (!obj[arr[i][prop]]) obj[arr[i][prop]] = arr[i];
    }
    var newArr = [];
    for (var key in obj) newArr.push(obj[key]);
    return newArr;
  },


  /**
   * @ to add escape charachters
   */
  addslashes: function (str) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
  },

  /**
   * @ Upadate row with duplicasi check
   */
  checkUpdate: function (table, where, whereParam, callback) {
    var app = require('../server/server');
    var ds = app.datasources.mysqldb;
    // if (id != null) {
    //     where = where + ' and ' + id;
    // }
    var sql = 'select * from ' + table + ' where ' + where;
    ds.connector.query(sql, whereParam ? whereParam : [], (err, res) => {
      if (err) {
        console.error(err);
      } else {
        if (res.length > 0) {
          callback(res);
        } else {
          callback(0);
        }
      }
    });
  },

  /**
   * @ Upadate row
   */
  setAction: function (where, value, table, callback) {
    var app = require('../server/server');
    var ds = app.datasources.mysqldb;

    var sql = 'UPDATE ' + table + ' SET ' + value + ' where ' + where;
    ds.connector.query(sql, null, (err, res) => {
      if (err) {
        console.error(err);
        callback(err);
      } else {
        if (res.changedRows > 0) {
          callback(1); // Row updated successfully
        } else if (res.changedRows == 0 && res.affectedRows == 1) {
          callback(0); // Something went wrong
        } else if (res.changedRows == 0 && res.affectedRows == 0) {
          callback(2); // No matching rows
        }
      }
    });
  },

  hashPassword: function (plain, callback) {
    var crypto = require('crypto');
    var hash = crypto.createHash(constants.passwordEncrpt.shaKey);
    hash.update(plain + constants.passwordEncrpt.key);
    var value = hash.digest(constants.passwordEncrpt.hex);
    callback(value);
  },

  /*
  Common function for config value
   */
  configItem: function getConfigDetailsFortenantKey(tenantKey, callback) {
    var ds = app.datasources.mysqldb;
    var configDetails = {};
    var sql = "SELECT * FROM `tbl_config` WHERE tenant_company_key = ? AND isDeleted = ?";
    ds.connector.query(sql, [tenantKey, 0], function (err, configDetailResponse) {
      if (err) {
        callback(err);
      } else {
        async.eachOfSeries(configDetailResponse, function (configDet, key, asyncCallback) {
          configDetails[configDet.config_key] = configDet.value;
          asyncCallback();
        }, function (error) {
          if (error) {
            callback(error);
          } else {
            callback(null, configDetails);
          }
        });
      }
    });
  },
  responseMsg: function (code) {
    let response;
    switch (code) {
      case 200:
        response = {
          statusMsg : "OK",
          description : "The request was successfully completed."
        };
        return response;
        break;
      case 201: 
        response = {
          statusMsg : "Created",
          description : "A new resource was successfully created."
        };
        return response;
        break;
      case 400: 
        response = {
          statusMsg : "Bad Request",
          description : "The request was invalid."
        };
        return response;
        break;
      case 401: 
        response = {
          statusMsg : "Unauthorized",
          description : "The request did not include an authentication token or the authentication token was expired."
        };
        return response;
        break;
      case 403: 
        response =  {
          statusMsg : "Forbidden",
          description : "The client did not have permission to access the requested resource."
        };
        return response;
        break;
      case 404: 
        response = {
          statusMsg : "Not Found",
          description : "The requested resource was not found."
        };
        return response;
        break;
      case 405: 
        response = {
          statusMsg : "Method Not Allowed",
          description : "The HTTP method in the request was not supported by the resource. For example, the DELETE method cannot be used with the Agent API"
        };
        return response;
        break;
      case 409: 
        response = {
          statusMsg : "Conflict",
          description : "The request could not be completed due to a conflict. For example,  POST ContentStore Folder API cannot complete if the given file or folder name already exists in the parent location."
        };
        return response;
        break;
      case 500: 
        response = {
          statusMsg : "Internal Server Error",
          description : "The request was not completed due to an internal error on the server side."
        };
        return response;
        break;
      case 503: 
        response = {
          statusMsg : "Service Unavailable",
          description : "The server was unavailable."
        };
        return response;
        break;
      default:
        response = {
          statusMsg : "",
          description : ""
        };
        return response;
        break
    }
  },
  generatePermGroupNo: function (callback) {

    PermissionGroups.findOne({
        fields: {
          'pgId': true
        },
        where: {
          teamId: teamId,
          isdeleted: 0,
          tenantKey: tenantKey,
        },  
        'order': 'pgId DESC',
        'limit': 1,
      },
      function (err, pgRes) {
        if (err) {
          callback(0);
        } else {
          var PGNo;
          if (pgRes.length > 0) {
            
            PGNo= refFind[0].pgId + 1;
            callback(PGNo);
          } else {
            PGNo = 1;
            callback(PGNo);
          }
        }
      });
  }
};

