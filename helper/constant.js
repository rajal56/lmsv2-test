'use strict';

module.exports = Object.freeze({
  secret: {
    key: '9vApxLk5G3PAsJrM',
    iv: 'FnJL7EDzjqWjcaY9',
    cypherCode: 'aes-128-cbc',
    encryption: 'utf8',
    decryption: 'binary',
    hex: 'hex',
  },
  sqlErrMsg: 'Database issue, Please contact administrator!',
  noData: 'No data found!',
  passwordEncrpt: {
    key: 'I6PnEPbQNLslYMj7ChKxDJ2yenuHLkXn',
    shaKey: 'sha512',
    hex: 'hex',
  },
  productName : 'The Philomath Team',
  productCompanyName : 'Philomath Digital LLP',
  currYear: (new Date()).getFullYear(),
  companyLink: 'https://www.philomathdigital.com/',
  facebookUrl: 'https://www.facebook.com/',
  twitterUrl:'https://www.twitter.com/',
  linkedInUrl: 'https://in.linkedin.com/company/',
  facebookImg: '',
  twitterImg: '',
  linkedinImg: '',
  defaultCompanyLogo: '',
  defaultFormats: {
    dateFormat : 'DD-MM-YYYY',
    currencyPosition: '1',
    defaultCurrencySymbol: 'INR',
    defaultCompanyName: 'Philomath'
  }
});

