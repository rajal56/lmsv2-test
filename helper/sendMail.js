'use strict';
const nodemailer = require('nodemailer');
var app = require('../server/server');
var constants = require('../helper/constant');
var emailDynamicTemplate = require('./emailDynamicTemplate');


module.exports = {
  sendMail: function (type, email, data, template, domain) {
    switch (type) {
      case 'activate':
        return module.exports.send_activation_email(email, data, template, domain);
        break;
      case 'forgot_password':
        return module.exports.send_reset_password_email(email, data, template);;
        break;
      case 'reset_password':
        return type;
        break;
      case 'welcome_email':
        return module.exports.send_welcome_email(email, data, template);
        break;
    }
  },
  send_reset_password_email: function (email, data, mailTemplate) {
    if(mailTemplate) {
    var string = JSON.stringify(mailTemplate);
    var json = JSON.parse(string);
    var templateBody = json[0].template_body;
    var baseUrl = app.get('url').replace(/\/$/, '');
    templateBody = templateBody.replace("{USERNAME}", data.username);
    templateBody = templateBody.replace("{EMAIL}", data.email);
    templateBody = templateBody.replace("{PASSWORD}", data.password);
    templateBody = templateBody.replace(/{PRODUCT_NAME}/gi, constants.productName);
    templateBody = templateBody.replace("{PRODUCT_COMPANY}", constants.productCompanyName);
    templateBody = templateBody.replace("{CURRENT_YEAR}", constants.currYear);
    templateBody = templateBody.replace('{FACEBOOK_LINK}', constants.facebookUrl);
    templateBody = templateBody.replace('{TWITTER_LINK}', constants.twitterUrl);
    templateBody = templateBody.replace('{LINKEDIN_LINK}', constants.linkedInUrl);
    templateBody = templateBody.replace('{FACEBOOK_IMG}', constants.facebookImg);
    templateBody = templateBody.replace('{TWITTER_IMG}', constants.twitterImg);
    templateBody = templateBody.replace('{LINKEDIN_IMG}', constants.linkedinImg);
    templateBody = templateBody.replace('{COMPANY_LINK}',constants.companyLink);
      var companyKey = data.tenantKey;
      templateBody = templateBody.replace(/{SITE_NAME}/gi, constants.companyLink);
      var emailContent = templateBody.replace(/\n/g, '');
      emailContent = emailContent.replace(/\t/g, '');
      var templateDesign = emailDynamicTemplate.getEmailDynamicTemplate(json[0].subject,constants.productCompanyName);
      var color =  {title: '#3f51b5', subTitle: '#4d5ebb'};
      templateDesign = templateDesign.replace(/{TITLE}/gi, color.title);
      templateDesign = templateDesign.replace(/{SUB_TITLE}/gi, color.subTitle);
      var content = templateDesign + emailContent;
      var params = {};
      params.template = content;
      params.subject = json[0].subject;
      params.resourceFile = '';
      params.recipient = email;
      module.exports.shootAcivationEmail(params, data.tenantKey);
     
    }
  },
  send_activation_email: function (email, data, mailTemplate, domain) {
    if(mailTemplate){
      var string = JSON.stringify(mailTemplate);
      var json = JSON.parse(string);
      var templateBody = json[0].template_body;
      // var baseUrl = app.get('url').replace(/\/$/, '');
      if(data.isWeb == '1'){
        let url = 'https://' + domain + '/login/?user=' + data.user_id + '&activationKey=' + data.new_email_key;
        templateBody = templateBody.replace(/{ACTIVATE_URL}/gi, url);
        templateBody =  templateBody.replace(/{REPLACE_CONTENT}/gi,'\n<a class=\"btnLink\" href=\"'+url+'" style=\"font-size: 14px;background-color: #1092ee;padding: 10px 20px;\n border-radius: 30px;color: white;text-decoration: none;\">Confirm My Email Address</a>');
      
        templateBody = templateBody.replace(/{CONFIRM_MESSAGE}/gi, 'It\'s time to confirm your email address.');
        templateBody = templateBody.replace(/{CONTENT_MESSAGE}/gi, 'just click the below.'); 
      
      }else{
        templateBody = templateBody.replace(/{ACTIVATE_URL}/gi, data.new_email_key);
        templateBody =  templateBody.replace(/{REPLACE_CONTENT}/gi,'<span class=\"btnLink\" style=\"padding: 10px 20px;\n color: black;text-decoration: unset;text-align: center;font-size: 24px;\"><strong>'+data.new_email_key+'</strong></span>');
        templateBody = templateBody.replace(/{CONFIRM_MESSAGE}/gi, 'It\'s time to confirm your One Time Password.');
        templateBody = templateBody.replace(/{CONTENT_MESSAGE}/gi, 'just use following OTP.'); 
      
      }
      templateBody = templateBody.replace("{ACTIVATION_PERIOD}", data.activation_period);
      templateBody = templateBody.replace("{USERNAME}", data.username);
      templateBody = templateBody.replace("{EMAIL}", data.email);
      templateBody = templateBody.replace("{PASSWORD}", data.password);
      templateBody = templateBody.replace("{PRODUCT_COMPANY}", constants.productCompanyName);
      templateBody = templateBody.replace("{CURRENT_YEAR}", constants.currYear);
      templateBody = templateBody.replace('{FACEBOOK_LINK}', constants.facebookUrl);
      templateBody = templateBody.replace('{TWITTER_LINK}', constants.twitterUrl);
      templateBody = templateBody.replace('{LINKEDIN_LINK}', constants.linkedInUrl);
      templateBody = templateBody.replace('{FACEBOOK_IMG}', constants.facebookImg);
      templateBody = templateBody.replace('{TWITTER_IMG}', constants.twitterImg);
      templateBody = templateBody.replace('{LINKEDIN_IMG}', constants.linkedinImg);
      templateBody = templateBody.replace('{COMPANY_LINK}',constants.companyLink);
      templateBody = templateBody.replace(/{PRODUCT_NAME}/gi, constants.productName);
      var companyKey = data.tenantKey;
      getCompanyName(companyKey, function (err, companyName) {
        if (err) {
          logger.error(moment(Date.parse(moment().format('YYYY-MM-DD'))).format() + 'Get company name for Tenant Activation Email - SQL Error ', err );
        } else {
          var companyName = companyName.company_name ? companyName.company_name : (companyName.website_name ? companyName.website_name : 'Philomath');
          templateBody = templateBody.replace(/{SITE_NAME}/gi, companyName);

          var emailContent = templateBody.replace(/\n/g, '');
          emailContent = emailContent.replace(/\t/g, '');
          var templateDesign = emailDynamicTemplate.getEmailDynamicTemplate(json[0].subject, companyName);
          var color =  {title: '#3f51b5', subTitle: '#4d5ebb'};
          templateDesign = templateDesign.replace(/{TITLE}/gi, color.title);
          templateDesign = templateDesign.replace(/{SUB_TITLE}/gi, color.subTitle);
          var content = templateDesign + emailContent;

          var params = {};
          params.template = content;
          params.subject = json[0].subject;
          params.resourceFile = '';
          params.recipient = email;
        
          var shootEmailRes = module.exports.shootAcivationEmail(params, data.tenantKey);
          return shootEmailRes;
        }
      });
    }
  },

  send_welcome_email: function (email, data, mailTemplate) {
    if(mailTemplate.length>0){
    var string = JSON.stringify(mailTemplate);
    var json = JSON.parse(string);
    var templateBody = json[0].templateBody;
    templateBody = templateBody.replace("{USERNAME}", data.fullname);
    templateBody = templateBody.replace("{EMAIL}", data.email);
    templateBody = templateBody.replace("{PASSWORD}", data.originalPass);
    templateBody = templateBody.replace('{FACEBOOK_LINK}', constants.facebookUrl);
    templateBody = templateBody.replace('{TWITTER_LINK}', constants.twitterUrl);
    templateBody = templateBody.replace('{LINKEDIN_LINK}', constants.linkedInUrl);
    templateBody = templateBody.replace('{FACEBOOK_IMG}', constants.facebookImg);
    templateBody = templateBody.replace('{TWITTER_IMG}', constants.twitterImg);
    templateBody = templateBody.replace('{LINKEDIN_IMG}', constants.linkedinImg);
    templateBody = templateBody.replace('{COMPANY_LINK}',constants.companyLink);
    templateBody = templateBody.replace("{PRODUCT_COMPANY}", constants.productCompanyName);
    templateBody = templateBody.replace("{CURRENT_YEAR}", constants.currYear);
    templateBody = templateBody.replace(/{PRODUCT_NAME}/gi, constants.productName);
      var companyKey = data.tenantKey;
      templateBody = templateBody.replace(/{SITE_NAME}/gi, constants.productCompanyName);
      templateBody = templateBody.replace('{COMPANY_EMAIL}', constants.companyLink);
      var emailContent = templateBody;
      emailContent = emailContent.replace(/\n/g, '');
      emailContent = emailContent.replace(/\t/g, '');
      var params = {};
      params.template = emailContent;
      params.subject = json[0].subject;
      params.resourceFile = '';
      params.recipient = email;
      module.exports.shootEmail(params, data.tenantKey);
     
    }
  },

  shootEmail: function (params, companyKey) {
    // todo developer's note
    // (If any changes done in this function, please do in shootCronEmail also, which is used in cron, And update
    // with respective person)

    var config = app.models.Config;
    var colorTheme;

    config.find({
      fields: {
        configKey: true,
        value: true
      },
      where: {
        and: [{
            or: [{
              configKey: {
                like: '%smtp%'
              }
            }, {
              configKey: {
                like: '%company%'
              }
            },{
              configKey: {
                like: '%sidebar%'
              }
            }]
          },
          {
            tenantKey: 'ac7153df458aefc2a93fd1a4f7513147'
          }
        ]

      }
    }, function (configError, configObject) {
      if (configError) {
        
        console.log(configError);
      } else {

        if (configObject !== '') {
         // getCompanyName(companyKey, function (err, companyName) {
            var companyName = constants.productCompanyName;
            var smtpHost, smtpPass, smtpPort, smtpUser, companyEmail;
            configObject.forEach(obj => {

              if (obj.configKey === 'smtp_host') {
                smtpHost = obj.value;
              }
              if (obj.configKey === 'smtp_pass') {
                smtpPass = obj.value;
              }
              if (obj.configKey === 'smtp_port') {
                smtpPort = obj.value;
              }
              if (obj.configKey === 'smtp_user') {
                smtpUser = obj.value;
              }
              if (obj.configKey === 'company_notification_email') {
                companyEmail = "kartik@test.com";//obj.value;
              }
            });
              var SUBJECT = params.subject;
              var emailContent = params.template.replace(/\n/g, '');
              emailContent = emailContent.replace(/\t/g, '');
              new Promise(function(resolve, reject) {
              var content = '';
              if(params.from){
                companyEmail = params.from;
                content = emailContent;
                resolve(content);
              } else{
                var templateDesign = emailDynamicTemplate.getEmailDynamicTemplate(SUBJECT, companyName);
               // getThemeColor(companyKey,function (err,color) {
                  templateDesign = templateDesign.replace(/{TITLE}/gi, "color.title");
                  templateDesign = templateDesign.replace(/{SUB_TITLE}/gi, "color.subTitle");

                  content = emailContent;
                  content = content.replace(/{PRODUCT_NAME}/gi, constants.productName);
                  content = content.replace(/{PRODUCT_COMPANY}/gi, constants.productCompanyName);
                  content = content.replace(/{CURRENT_YEAR}/gi, constants.currYear);
                  content = content.replace(/{SITE_NAME}/gi, companyName);
                  resolve(content);
               // });
              }
            }).then(function(content) {
              let mailOptions = {
                from: companyEmail,
                to: params.recipient,
                cc: params.cc,
                bcc: params.bcc,
                subject: params.subject,
                attachments: params.attachmentArr,
                html: content
              };
                // create reusable transporter object using the default SMTP transport
                let transporter = nodemailer.createTransport({
                  host: smtpHost,
                  port: smtpPort,
                  secure: true, // true for 465, false for other ports
                  auth: {
                    user: smtpUser, // generated ethereal user
                    pass: smtpPass // generated ethereal password
                  }
                });
                console.log("transporter--------------------->", transporter);
                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                   
                    return console.log(error);
                  } else {
                   
                    console.log(info);
                    return info;
                  }
                });

            });
          //});
        } else {
          
          return "Email Settings Empty";
        }
      }
    });

  },
  shootAcivationEmail: function (params, companyKey) {

    var config = app.models.Config;
    config.find({
      fields: {
        configKey: true,
        value: true
      },
      where: {
        and: [{
          or: [{
            configKey: {
              like: '%smtp%'
            }
          }, {
            configKey: {
              like: '%company%'
            }
          }]
        }, {
          tenantKey: 'ac7153df458aefc2a93fd1a4f7513147'
        }]
      }
    }, function (configError, configObject) {
    
      if (configError) {
        console.log(configError);
      } else {
       
        if (configObject != '') {
          var smtpHost, smtpPass, smtpPort, smtpUser, companyEmail;
          configObject.forEach(obj => {

            if (obj.configKey == 'smtp_host') {
              smtpHost = obj.value;
            }
            if (obj.configKey == 'smtp_pass') {
              smtpPass = obj.value;
            }
            if (obj.configKey == 'smtp_port') {
              smtpPort = obj.value;
            }
            if (obj.configKey == 'smtp_user') {
              smtpUser = obj.value;
            }
            if (obj.configKey == 'company_email') {
              companyEmail = obj.value;
            }
          });

          // create reusable transporter object using the default SMTP transport
          let transporter = nodemailer.createTransport({
            host: smtpHost,
            port: smtpPort,
            secure: true, // true for 465, false for other ports
            auth: {
              user: smtpUser, // generated ethereal user
              pass: smtpPass // generated ethereal password
            }
          });
          let mailOptions = {
            from: companyEmail,
            to: params.recipient,
            cc: params.cc,
            bcc: params.bcc,
            subject: params.subject,
            attachments: params.attachmentArr,
            html: params.template
          };
          // send mail with defined transport object
          transporter.sendMail(mailOptions, (error, info) => {
            console.log("mailOptions             ",error, info )
            if (error) {
              return console.log(error);
            } else {
              return info;
            }
          });

        } else {
          return "Email Settings Empty";
        }
      }
    });

  },

  shootBulkEmail: function (params, usersDetail, companyKey) {
    var config = app.models.Config;
    var colorTheme;

    config.find({
      fields: {
        configKey: true,
        value: true
      },
      where: {
        and: [{
            or: [{
              configKey: {
                like: '%smtp%'
              }
            }, {
              configKey: {
                like: '%company%'
              }
            },{
              configKey: {
                like: '%sidebar%'
              }
            }]
          },
          {
            tenantKey: 'ac7153df458aefc2a93fd1a4f7513147'
          }
        ]

      }
    }, function (configError, configObject) {
      if (configError) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Shoot bulk mail : failed to get config', configError);
      } else {

        if (configObject !== '') {
          getCompanyName(companyKey, function (err, companyName) {
            var companyName = companyName.company_name ? companyName.company_name : (companyName.website_name ? companyName.website_name : 'Philomath');
            var smtpHost, smtpPass, smtpPort, smtpUser, companyEmail;
            configObject.forEach(obj => {

              if (obj.configKey === 'smtp_host') {
              smtpHost = obj.value;
            }
            if (obj.configKey === 'smtp_pass') {
              smtpPass = obj.value;
            }
            if (obj.configKey === 'smtp_port') {
              smtpPort = obj.value;
            }
            if (obj.configKey === 'smtp_user') {
              smtpUser = obj.value;
            }
            if (obj.configKey === 'company_notification_email') {
              companyEmail = obj.value;
            }
          });
          let userListPromiser = [];
          for (var i = 0; i < usersDetail.templates.length; i++) {
            userListPromiser.push(new Promise(function(resolve, reject) {
            var SUBJECT = params.subject;
            var emailContent = usersDetail.templates[i].replace(/\n/g, '');
            emailContent = emailContent.replace(/\t/g, '');
            var content = '';
            var templateDesign = emailDynamicTemplate.getEmailDynamicTemplate(SUBJECT, companyName);
           getThemeColor(companyKey,function (err,color) {
            templateDesign = templateDesign.replace(/{TITLE}/gi, color.title);
            templateDesign = templateDesign.replace(/{SUB_TITLE}/gi, color.subTitle);

            content = templateDesign + emailContent;
             content = content.replace(/{PRODUCT_NAME}/gi, constants.productName);
             content = content.replace(/{PRODUCT_COMPANY}/gi, constants.productCompanyName);
             content = content.replace(/{CURRENT_YEAR}/gi, constants.currYear);
            content = content.replace(/{SITE_NAME}/gi, companyName);
             resolve(content);
            })
            }));
          }

          Promise.all(userListPromiser).then(function(userListPromiser) {
            let mails = [];
            for (var i = 0; i < userListPromiser.length; i++) {
              mails.push(new Promise(function(resolve, reject) {

                let mailOptions = {
                  from: companyEmail,
                  to: usersDetail.emails[i],
                  cc: params.cc,
                  bcc: params.bcc,
                  subject: params.subject,
                  attachments: params.attachmentArr,
                  html: userListPromiser[i],
                };
                // create reusable transporter object using the default SMTP transport
                let transporter = nodemailer.createTransport({
                  host: smtpHost,
                  port: smtpPort,
                  secure: true, // true for 465, false for other ports
                  auth: {
                    user: smtpUser, // generated ethereal user
                    pass: smtpPass // generated ethereal password
                  }
                });

                // send mail with defined transport object
                transporter.sendMail(mailOptions, (error, info) => {
                  if (error) {
                    logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Send mail - Shoot bulk mail : Failed to send mail', err);
                    reject (err);
                  } else {
                    logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Shoot mail - shoot bulk mail : Mail sent successfully');
                    resolve(info);
              }
              });

            }));
          }

            Promise.all(mails).then( function (infos) {
                return infos;
              }).catch( function (err) {
                return err;
              });

          }).catch(err => {
            return err;
          });
          });
        } else {
          return "Email Settings Empty";
        }
      }
    });

  }

};

