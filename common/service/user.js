var path = require('path');
var app = require('../../server/server');
var multer = require('multer');
var AccessToken = app.models.AccessToken;
var Users = app.models.Users;
// var session = app.model

module.exports.userLogin = (req, res) => {
    var email = (req.body.email != undefined) ? req.body.email : "";
    var password = (req.body.password != undefined) ? req.body.password : "";
    var domain = (req.body.domain != undefined) ? req.body.domain : "pbm.philomath.com";
    var body = {
        "email": email,
        "password": password,
        "domain": domain
    };
    // console.log("responses ------------------>",AccessToken);
    //return false;
    Users.login(email, password, domain, function (err, response) {
        if (!err) {
            
            req.session.ctx = response.data;
             //console.log(req.session.ctx.tenantKey);
             //return false;
            //res.redirect('/content');

            //console.log(req.session);
            //return false;
            res.redirect('/content');
            // res.redirect('/question/2');
        } else {
            // return res.status(401).send(err);
           res.redirect('/login');
        }
    });
}
module.exports.userLogout = (req, res) => {
    //var ctx = (req.session.ctx) ? req.session.ctx : '';
    var ctx = req;
    Users.logout(ctx, function (err, response) {
        if (!err) {
            console.log(response);
            req.session.destroy((err) => {
                if (err) {
                    return err;
                }
                res.redirect('/login');
            });
            // res.redirect('/login');
        } else {
            console.log(err);
            // res.redirect('/login');
        }
    });
}

module.exports.usersList = (req, res) => {
    //console.log(req.session);
    // console.log(req.session.ctx);
    if (req.session.ctx) {
        var ctx = req.session.ctx;
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 10;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalCount = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
       
        skip = (currentPage - 1) * pageLimit;
        totalPages = Math.ceil(totalCount / pageLimit);
        
        Users.list(ctx, filter, function (err, response) {
            if (response.code == 200 || 404) {
                if (req.session.msgstatus) {

                    delete req.session.msgstatus;
                    delete req.session.msg;

                }
                totalCount = response.data.totalCount;
                totalPages = Math.ceil(totalCount / pageLimit);

                var msg = (req.query.status) ? req.query.status : '';
                res.render('users', {
                    title: 'user list',
                    data: response.data.list,
                    msg: msg,
                    pageSize: pageLimit,
                    pageCount: totalPages,
                    currentPage: currentPage,
                    hasPreviousPage: currentPage > 1,
                    hasNextPage: (pageLimit * currentPage) < totalCount,
                    session: ss,
                    redirectTo: '/users'
                });

            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }
}

/* module.exports.userList = (req, res) => {
    var ctx = req;
    Users.list(ctx, function (err, response) {
        console.log(response);
        if (response.status == 'success') {
            res.render('users', {
                users: response.data
            });
        } else {
            return res.status(401).send(response.data);
        }
    });
} */


module.exports.userAdd = (req, res) => {
    //console.log(req.session);
    //return false;
    if (req.session.ctx) {
        res.render('user-create',{
            data: ''
        });
    } else {
        res.redirect('/');
    }
}

module.exports.add = (req, res) => {
   // console.log("request user", req.body);
   //console.log(req.session.ctx);
   //req.session.ctx.tenantKey
    if (req.session.ctx) {

        var ctx = req.session.ctx;
        var fullName = (req.body.fullName != undefined) ? req.body.fullName : '';
        var employeeId = (req.body.employeeId != undefined) ? req.body.employeeId : '';
        var mobile = (req.body.mobile != undefined) ? req.body.mobile : '';
        var email = (req.body.email != undefined) ? req.body.email : '';
        var password = (req.body.confirmPassword != undefined) ? req.body.confirmPassword : '';
        var userType = (req.body.userType != undefined) ? req.body.userType : '';
        console.log("request user", fullName, employeeId, mobile, email, password, userType);
        Users.add(ctx, fullName, employeeId, mobile, email, password, userType, function (err, response) {
            //console.log("response", response);
            //return false;
            if (response.code == 200 || 404) {
                res.render('users', {
                    users: response.data
                });
            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/create_user');
    }
}

module.exports.edit = (req, res) => {
    if (req.session.ctx) {
        var id = req.params.id;
        Users.detailsById(req.session.ctx, id, function (err, response) {
            if (response.status == 'success') {

                res.render('user-create',
                    {
                        data: response.data
                    });

            } else {
                return res.status(401).send(response.statusMsg);
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.update = (req, res) => {
    if (req.session.ctx) {

        var ctx = req.session.ctx;
        var id = req.params.id;
        var fullName = (req.body.fullName != undefined) ? req.body.fullName : '';
        var employeeId = (req.body.employeeId != undefined) ? req.body.employeeId : '';
        var mobile = (req.body.mobile != undefined) ? req.body.mobile : '';
       
        var userType = (req.body.userType != undefined) ? req.body.userType : '';
        console.log("request user", fullName, employeeId, mobile, userType);
        Users.modify(ctx, id, fullName, employeeId, mobile, userType, function (err, response) {
        
            if (response.code == 200 || 404) {

                res.redirect('/users');

            } else {
                return res.status(401).send(response.statusMsg);
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.delete = (req, res) => {
  
    if (req.session.ctx) {

        var userId = req.params.id;
       
        Users.delete(req.session.ctx, userId, function (err, response) {
            
            req.session.msgstatus = response.status;
            req.session.msg = response.msg;
            if (response.status == 'success') {

                res.redirect('/users');

            } else {
                // return res.status(401).send(response.statusMsg);
                res.redirect('/users');
            }
        });
    } else {
        res.redirect('/users');
    }
}