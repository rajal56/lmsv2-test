var app = require('../../server/server');
var Category = app.models.Category;
var moment = require('moment');
// var session = app.model

module.exports.list = (req, res) => {
    //console.log(req.session);
   
    if (req.session.ctx) {
        var ctx = req.session.ctx;
        // console.log(req.session);
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 10;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalCount = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
       
        skip = (currentPage - 1) * pageLimit;
        totalPages = Math.ceil(totalCount / pageLimit);

        Category.list(ctx, filter, function (err, response) {
            
            if (response.code == 200 || 404) {
                if (req.session.msgstatus) {

                    delete req.session.msgstatus;
                    delete req.session.msg;

                }
                totalCount = response.data.totalCount;
                totalPages = Math.ceil(totalCount / pageLimit);

                var msg = (req.query.status) ? req.query.status : '';
                res.render('category-list', {
                    title: 'Category list',
                    data: response.data.list,
                    msg: msg,
                    moment: moment,
                    pageSize: pageLimit,
                    pageCount: totalPages,
                    currentPage: currentPage,
                    hasPreviousPage: currentPage > 1,
                    hasNextPage: (pageLimit * currentPage) < totalCount,
                    session: ss,
                    redirectTo: '/category'
                });

            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.form = (req, res) => {
    
    if (req.session.ctx) {
       // res.render('category-form');
        res.render('category-form',
        {
            data: ''
        });
    } else {
        res.redirect('/category_list');
    }
}

module.exports.add = (req, res) => {
   
    if (req.session.ctx) {

        var ctx = req.session.ctx;
        var categoryName = (req.body.categoryName != undefined) ? req.body.categoryName : '';
        var status = (req.body.status != undefined) ? req.body.status : '';
        var description = (req.body.description != undefined) ? req.body.description : '';
        //console.log("request user", categoryName, status, description);
        Category.add(ctx, categoryName, status, description, function (err, response) {
          
            if (response.code == 200 || 404) {
                res.redirect('/category_list');
               /*  res.render('category-list', {
                    data: response.data
                }); */
            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/category_form');
    }
}

module.exports.delete = (req, res) => {
  
    if (req.session.ctx) {

        var categoryId = req.params.id;
       
        Category.delete(req.session.ctx, categoryId, function (err, response) {
            
            req.session.msgstatus = response.status;
            req.session.msg = response.msg;
            if (response.status == 'success') {

                res.redirect('/category_list');

            } else {
                // return res.status(401).send(response.statusMsg);
                res.redirect('/category_list');
            }
        });
    } else {
        res.redirect('/category_list');
    }
}

module.exports.edit = (req, res) => {
    if (req.session.ctx) {
        var id = req.params.id;
        Category.detailsById(req.session.ctx, id, function (err, response) {
            if (response.status == 'success') {

                res.render('category-form',
                    {
                        data: response.data
                    });

            } else {
                return res.status(401).send(response.statusMsg);
            }
        });
    } else {
        res.redirect('/');
    }
}