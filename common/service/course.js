var path = require('path');
var app = require('../../server/server');
var multer = require('multer');
var moment = require('moment');
const fs = require('fs');

const unzipper = require('unzipper');


var Course = app.models.Course;
var Modules = app.models.Modules;
var CourseSetting = app.models.courseSettings;
var courseImage = path.join(__dirname, '../../client/images/course/');


let uniqueId = (new Date()).getTime().toString(36);

var storage_course_image = multer.diskStorage({
    destination: function (req, file, callback) {

        if (!fs.existsSync(courseImage)) {
            fs.mkdirSync(courseImage, { recursive: true })
        }

        callback(null, courseImage);
    },
    filename: function (req, file, callback) {
        callback(null, uniqueId + path.extname(file.originalname));
    }
});


var upload_course_image = multer({ storage: storage_course_image }).single('courseImage');


module.exports.courseModule = (req, res) => {
    if (req.session.ctx) {
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var id = (req.params.id) ? req.params.id : '';
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 10;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalModules = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
        skip = (currentPage - 1) * pageLimit;
        if (id != '') {
            var courseDetails = '';
            Course.findCourseById(req.session.ctx, id, function (courseResponse) {

                Modules.courseModuleList(req.session.ctx, { "search": searchString }, id, function (moduleResponse1) {
                    // console.log(moduleResponse1);
                    totalModules = (moduleResponse1.data).length;
                    totalPages = Math.ceil(totalModules / pageLimit);
                    Modules.courseModuleList(req.session.ctx, filter, id, function (moduleResponse) {
                        // console.log(moduleResponse);
                        CourseSetting.findCourseSettingsById(req.session.ctx, courseResponse.data.courseId, function (settingResponse) {
                            // console.log(settingResponse);
                            if (req.session.msgstatus) {
                                delete req.session.msgstatus;
                                delete req.session.msg;
                            }
                            res.render('modules', {
                                content: courseResponse.data,
                                modules: moduleResponse.data,
                                settings: settingResponse.data,
                                moment: moment,
                                pageSize: pageLimit,
                                pageCount: totalPages,
                                currentPage: currentPage,
                                hasPreviousPage: currentPage > 1,
                                hasNextPage: (pageLimit * currentPage) < totalModules,
                                session: ss
                            });
                        });


                    });
                });



            });



        } else {
            res.send('bad request');
        }
    } else {
        res.redirect('/');
    }
}

module.exports.courseList = (req, res) => {
    // console.log(req.session);

    if (req.session.ctx) {

        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 12;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalCourses = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
        Course.getTotalCount(function (err, resultCourseCount) {
            totalPages = Math.ceil(resultCourseCount / pageLimit);
            totalCourses = resultCourseCount;
        });
        skip = (currentPage - 1) * pageLimit;

        Course.list(req.session.ctx, filter, function (response) {
            // console.log(response);
            if (response.status == 'success') {
                var msg = (req.query.status) ? req.query.status : ''
                res.render('index', {
                    title: 'course list',
                    content: response.data,
                    msg: msg,
                    imagePath: courseImage,
                    pageSize: pageLimit,
                    pageCount: totalPages,
                    currentPage: currentPage,
                    hasPreviousPage: currentPage > 1,
                    hasNextPage: (pageLimit * currentPage) < totalCourses,
                    redirectTo: '/index'
                });

            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }

}
module.exports.courseSave = (req, res) => {
    if (req.session.ctx) {
        upload_course_image(req, res, function (error) {
            if (error) {
                return res.status(401).send(error);
            } else {
                var name = (req.body.name != undefined) ? req.body.name : '';
                var description = (req.body.description != undefined) ? req.body.description : '';
                var referanceCode = (req.body.referanceCode != undefined) ? req.body.referanceCode : '';
                var activeStatus = (req.body.activeStatus != undefined) ? req.body.activeStatus : 'inactive';
                var courseImage = '';
                if (req.file) {
                    var filename = req.file.filename;

                } else {
                    var filename = '';
                }
                var courseImage = filename;
                Course.add(req.session.ctx, name, description, referanceCode, activeStatus, courseImage, function (courseResponse) {
                    // console.log(courseResponse);
                    req.session.msgstatus = courseResponse.status;
                    req.session.msg = courseResponse.msg;
                    if (courseResponse.status == 'success') {
                        var courseId = courseResponse.data.insertId;
                        var contentLiberary = (req.body.contentLiberary != undefined) ? req.body.varcontentLiberary : 0;
                        var courseCategory = (req.body.courseCategory != undefined) ? req.body.courseCategory : null;
                        var moduleOrder = (req.body.moduleOrder != undefined) ? req.body.moduleOrder : 0;
                        var newWindow = (req.body.newWindow != undefined) ? req.body.newWindow : 0
                        var duteDateOrCompliance = (req.body.duteDateOrCompliance != undefined) ? req.body.duteDateOrCompliance : null;
                        var complianceDueDate = (req.body.complianceDueDate != undefined) ? req.body.complianceDueDate : null;
                        var complianceTimeSpan = (req.body.complianceTimeSpan != undefined) ? req.body.complianceTimeSpan : null;
                        var complianceEndDate = (req.body.complianceEndDate != undefined) ? req.body.complianceEndDate : null;
                        var complianceFor = (req.body.complianceFor != undefined) ? req.body.complianceFor : null;
                        var dueDate = (req.body.dueDate != undefined) ? req.body.dueDate : null;
                        var dueDateTimeSpan = (req.body.dueDateTimeSpan != undefined) ? req.body.dueDateTimeSpan : null;
                        var automaticRetake = (req.body.automaticRetake != undefined) ? req.body.automaticRetake : null;
                        var additionalReminder = (req.body.additionalReminder != undefined) ? req.body.additionalReminder : null;
                        var courseInactivationDate = (req.body.courseInactivationDate != undefined) ? req.body.courseInactivationDate : null;
                        var removeAccessDate = (req.body.removeAccessDate != undefined) ? req.body.removeAccessDate : null;
                        var removeAccessTime = (req.body.removeAccessTime != undefined) ? req.body.removeAccessTime : null;
                        var certificateTemplate = (req.body.certificateTemplate != undefined) ? req.body.certificateTemplate : null;
                        var enableDescussionForum = (req.body.enableDescussionForum != undefined) ? req.body.enableDescussionForum : 0;
                        var wantToSellCourse = (req.body.wantToSellCourse != undefined) ? req.body.wantToSellCourse : 0;

                        CourseSetting.add(req.session.ctx, courseId, contentLiberary, courseCategory, moduleOrder, newWindow, duteDateOrCompliance, complianceDueDate, complianceTimeSpan, complianceEndDate, complianceFor, dueDate, dueDateTimeSpan, automaticRetake, additionalReminder, courseInactivationDate, removeAccessDate, removeAccessTime, certificateTemplate, enableDescussionForum, wantToSellCourse, function (response) {
                            req.session.msgstatus = response.status;
                            req.session.msg = response.msg;
                            if (response.status == "success") {

                                res.redirect('/modules/' + courseId);
                            } else {

                                res.redirect('/modules/' + courseId);
                            }

                        });



                    } else {

                        res.redirect('/content');
                        // return res.status(401).send(response.data);
                        // responseMsg = commonHelper.commonMsg('failure', 'Something is wrong', null, error);

                    }
                });
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.courseAdd = (req, res) => {
    if (req.session.ctx) {
        res.render('add-course');
    } else {
        res.redirect('/');
    }
}

module.exports.courseDelete = (req, res) => {
    if (req.session.ctx) {

        var id = req.params.id;
        Course.courseDelete(req.session.ctx, id, function (response) {
            // cosnole.log("status  ", response)
            req.session.msgstatus = response.status;
            req.session.msg = response.msg;
            // cosnole.log("req  ", req)
            if (response.status == 'success') {

                res.redirect('/content');

            } else {
                // return res.status(401).send(response.statusMsg);
                res.redirect('/content');
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.courseEdit = (req, res) => {
    if (req.session.ctx) {
        var id = req.params.id;
        Course.findCourseById(req.session.ctx, id, function (response) {
            if (response.status == 'success') {

                res.render('instructor-edit-course',
                    {
                        content: response.data
                    });

            } else {
                return res.status(401).send(response.statusMsg);
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.courseUpdate = (req, res) => {
    if (req.session.ctx) {

        upload_course_image(req, res, function (error) {
            if (error) {
                return res.status(401).send(error);
            } else {
                if (req.file) {
                    var filename = req.file.filename;
                    if (filename != '')
                        req.body['courseImage'] = filename;
                } else {
                    var filename = '';
                }
                Course.courseUpdate(req.session.ctx, req.body, function (response) {
                    // console.log(response);
                    req.session.msgstatus = response.status;
                    req.session.msg = response.msg;
                    if (response.status == 'success') {
                        if (req.body.courseSettingsId != undefined) {
                            CourseSetting.CoursesettingsUpdate(req.session.ctx, req.body, function (updateResponse) {
                                req.session.msgstatus = updateResponse.status;
                                req.session.msg = updateResponse.msg;
                                if (updateResponse.status == 'success') {

                                    res.redirect('/modules/' + req.body.courseId);
                                } else {
                                    res.redirect('/modules/' + req.body.courseId);
                                }
                            });
                        }


                    } else {
                        res.redirect('/modules/' + req.body.id);
                        // return res.status(401).send(response.statusMsg);
                    }
                });
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.courseContent = (req, res) => {
    if (req.session.ctx) {
        var activeTab = 1;
        if ((req.query.mpage != undefined) || (req.query.msearch != undefined)) {
            activeTab = 2;
        } else if ((req.query.lpage != undefined) || (req.query.lsearch != undefined)) {
            activeTab = 3;
        }
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 10;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalCourses = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };


        skip = (currentPage - 1) * pageLimit;
        var mcurrentPage = (req.query.mpage != undefined || req.query.mpage != 0) ? req.query.mpage : 1;
        var mpageLimit = (req.query.mlimit != undefined) ? req.query.mlimit : 10;
        var mtotalPages = 1;
        var mskip = 0;
        var morder = "createdDate DESC";
        var totalModules = 0;
        mskip = (mcurrentPage - 1) * mpageLimit;
        var msearchString = (req.query.msearch != undefined) ? req.query.msearch : '';
        var mfilter = { "limit": mpageLimit, "start": mskip, "order": morder, "search": msearchString };

        Modules.list(req.session.ctx, { "search": msearchString }, function (response) {
            if (response.status == 'success') {
                // console.log("inside module count");
                totalModules = (response.data).length;
                mtotalPages = Math.ceil(totalModules / pageLimit);
                Course.list(req.session.ctx, filter, function (resultCourseCount) {
                    // console.log(resultCourseCount);
                    if (resultCourseCount.status == "success") {
                        totalCourses = (resultCourseCount.data).length;
                        totalPages = Math.ceil(totalCourses / pageLimit);
                        // console.log("inside course count---" + totalPages);

                        Course.list(req.session.ctx, { "search": searchString }, function (responseCourse) {
                            if (responseCourse.status == 'success') {
                                // console.log("inside course list");
                                Modules.list(req.session.ctx, mfilter, function (moduleResponse) {
                                    if (moduleResponse.status == "success") {
                                        var msg = (req.query.status) ? req.query.status : '';

                                        res.render('content-list', {
                                            msg: msg,
                                            imagePath: courseImage,
                                            moment: moment,
                                            pageSize: pageLimit,
                                            pageCount: totalPages,
                                            currentPage: currentPage,
                                            hasPreviousPage: currentPage > 1,
                                            hasNextPage: (pageLimit * currentPage) < totalCourses,
                                            course_list: responseCourse.data,
                                            modules: moduleResponse.data,
                                            content: [],
                                            mpageSize: mpageLimit,
                                            mpageCount: mtotalPages,
                                            mcurrentPage: mcurrentPage,
                                            mhasPreviousPage: mcurrentPage > 1,
                                            mhasNextPage: (mpageLimit * mcurrentPage) < totalModules,
                                            activeTab: activeTab
                                        });
                                    } else {
                                        res.status(401).send(moduleResponse.data);
                                    }

                                });



                            } else {
                                res.status(401).send(responseCourse.data);
                            }
                        });

                    } else {
                        res.status(401).send(resultCourseCount);
                    }


                });
            } else {
                res.status(401).send(response.data);

            }
        });




    } else {
        res.redirect('/');
    }
}


module.exports.courseReport = (req, res) => {
    if (req.session.ctx) {
        var id = req.params.id;

        Course.findCourseById(req.session.ctx, id, function (response) {
            console.log(response);
            if (response.status == 'success') {
                res.render('Reports', {
                    content: response.data
                });
            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }
}
// async function getTotalModules(ctx, filter) {
//     await new Promise((resolve, reject) => {
//         Modules.list(ctx, filter, function (response) {
//             if (response.status == 'success') {
//                 resolve(response);
//             } else {
//                 // return res.status(401).send(response.data);
//                 reject(response)
//             }
//         });
//     }).then((result) => {
//         var totalModules = (result.data).length;
//         // 
//         return (totalModules);
//     }).catch((error) => {
//         return error;
//     });
// }

