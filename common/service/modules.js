var path = require('path');
var app = require('../../server/server');
var multer = require('multer');
var moment = require('moment');
const fs = require('fs');
// const unzipper = require('unzipper');
var unzip = require('unzip2');

var Modules = app.models.Modules;
var courseImage = path.join(__dirname, '../../client/images/course/');
var courseScormZip = path.join(__dirname, '../../client/uploads/course/Scorm/zipFiles');
var courseScormUnzip = path.join(__dirname, '../../client/uploads/course/Scorm/unzipFiles');
var courseVideo = path.join(__dirname, '../../client/uploads/course/video');
var coursePpt = path.join(__dirname, '../../client/uploads/course/ppt');
var coursePdf = path.join(__dirname, '../../client/uploads/course/pdf');
var courseDoc = path.join(__dirname, '../../client/uploads/course/doc');

let uniqueId = (new Date()).getTime().toString(36);
var filePath = "";
var video = [".m4v", ".avi", ".mpg", ".mp4"];
var scorm = [".zip", ".rar"];
var doc = [".ppt", ".pptx", ".pdf", ".doc", ".docx", ".jpg", ".jpeg", ".bmp", ".gif", ".png"];
var storage_course_scorm = multer.diskStorage({
    destination: function (req, file, callback) {
        var fileType = (file.mimetype).split('/');

        if (video.indexOf(path.extname(file.originalname)) > -1) {
            filePath = courseVideo;
        } else if (doc.indexOf(path.extname(file.originalname)) > -1) {
            console.log("doc");
            if (path.extname(file.originalname) == '.ppt' || path.extname(file.originalname) == '.pptx') {
                filePath = coursePpt;
            } else if (path.extname(file.originalname) == '.pdf') {
                filePath = coursePdf;
            } else {
                filePath = courseDoc;
            }

        } else if (scorm.indexOf(path.extname(file.originalname)) > -1) {
            filePath = courseScormZip;
        }
        if (!fs.existsSync(filePath)) {
            fs.mkdirSync(filePath, { recursive: true })
        }

        callback(null, filePath);
    },
    filename: function (req, file, callback) {

        callback(null, uniqueId + path.extname(file.originalname));

    }
});
var upload_course_scorm = multer({ storage: storage_course_scorm }).single('courseScorm');

module.exports.moduleSave = async (req, res) => {
    if (req.session.ctx) {
        var userId = req.session.ctx.userId;

        upload_course_scorm(req, res, function (error) {
            if (error) {
                return res.status(401).send(error);
            } else {
                var courseId = (req.body.courseId != undefined) ? req.body.courseId : '';
                var moduleType = (req.body.moduleType != undefined) ? req.body.moduleType : '';
                var moduleFile = '';
                var unzipfilepath = courseScormUnzip + '/' + uniqueId;
                if (req.file) {
                    var fileType = (req.file.mimetype).split('/');

                    if (fileType[1] == 'zip' || fileType[1] == 'rar') {
                        var filename = req.file.filename;

                        if (!fs.existsSync(unzipfilepath)) {
                            fs.mkdirSync(unzipfilepath, '0777', { recursive: true })
                        }
                        unzipFile(courseScormZip + "/" + filename, unzipfilepath);

                    } else {
                        if (doc.indexOf(path.extname(req.file.originalname) > -1)) {
                            moduleType = path.extname(req.file.originalname);
                            moduleType = moduleType.replace(/^./, '');
                        }

                        var filename = req.file.filename;
                    }

                } else {
                    var filename = "";
                }
                if (filename != "") {
                    if (fs.existsSync(courseScormZip + "/" + filename)) {
                        fs.unlinkSync(courseScormZip + "/" + filename);
                    }
                }

                moduleFile = uniqueId;
                Modules.moduleAdd(req.session.ctx, moduleType, moduleFile, function (response) {
                    req.session.msgstatus = response.status;
                    req.session.msg = response.msg;
                    if (response.status == 'success') {
                        var moduleId = response.data.insertId;
                        if (courseId != '') {
                            saveCourseModule(req.session.ctx, moduleId, courseId, function (courseModuleResponse) {
                                req.session.msgstatus = courseModuleResponse.status;
                                req.session.msg = courseModuleResponse.msg;
                                console.log(courseModuleResponse);
                                req.session.msgstatus = courseModuleResponse.status;
                                req.session.msg = courseModuleResponse.msg;
                                if (courseModuleResponse.status == 'success') {
                                    if (moduleType == "assessment" || moduleType == "survey") {
                                        res.redirect("/form-add/" + moduleType + "/" + moduleId);
                                    } else {

                                        res.redirect('/modules/' + courseId);
                                    }

                                } else {
                                    // return res.status(401).send("something went wrong.");
                                    res.sess
                                    res.redirect('/modules/' + courseId);
                                }
                            });


                        } else {
                            if (moduleType == "assessment" || moduleType == "survey") {
                                res.redirect("/form-add/" + moduleType + "/" + moduleId);
                            } else {
                                res.redirect('/content');
                            }
                        }


                    } else {
                        if (filename != "") {
                            // if (fs.existsSync(unzipfilepath) && (moduleType != "assessment" || moduleType != "survey")) {
                            //     fs.unlinkSync(unzipfilepath);
                            // }
                        }

                        // res.redirect('/modules');
                        // return res.status(401).send(response.data);
                        // responseMsg = commonHelper.commonMsg('failure', 'Something is wrong', null, error);

                    }
                });
            }
        });
    } else {
        res.redirect('/');
    }
}
module.exports.moduleList = (req, res) => {
    if (req.session.ctx) {
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var courseId = (req.params.courseId != undefined) ? req.params.courseId : '';
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 12;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalModules = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
        Modules.list(req.session.ctx, filter = "", function (response) {
            if (response.status == 'success') {
                totalModules = (response.data).length;
                totalPages = Math.ceil(totalModules / pageLimit);
            } else {
                return res.status(401).send(response.data);
            }
        });
        skip = (currentPage - 1) * pageLimit;
        Modules.list(req.session.ctx, filter, function (response) {
            if (response.status == 'success') {
                if (req.session.msgstatus) {

                    delete req.session.msgstatus;
                    delete req.session.msg;

                }
                res.render('modules', {
                    modules: response.data,
                    mpageSize: pageLimit,
                    mpageCount: totalPages,
                    mcurrentPage: currentPage,
                    mhasPreviousPage: currentPage > 1,
                    mhasNextPage: (pageLimit * currentPage) < totalModules,
                    session: ss
                });
            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }
}
function saveCourseModule(ctx, moduleId, courseId, callback) {

    Modules.courseModuleAdd(ctx, moduleId, courseId, function (response) {
        callback(response);
        // console.log(response);
        // if (response.status == 'success') {
        //     return response;
        // } else {

        //     return false;
        // }
    });
}
async function unzipFile(readFile, writeFile) {
    await new Promise(resolve =>
        fs.createReadStream(readFile)
            .pipe(unzip.Extract({ path: writeFile }))
            .on('close', () => {

                resolve();
            }));
}