var app = require('../../server/server');
var moment = require('moment');
var Form = app.models.Form;
var Modules = app.models.Modules;
module.exports.formAdd = (req, res) => {
    if (req.session.ctx) {
        var type = req.params.type;
        var moduleId = req.params.id;
        //  Modules.add()
        res.render('form', {
            type: type,
            moduleId: moduleId
        });
    } else {
        res.redirect('/');
    }
}

module.exports.formSave = (req, res) => {
    if (req.session.ctx) {
        var moduleType = (req.body.formType) ? req.body.formType : '';
        var nmbrStr = '';
        if (moduleType == 'assessment') {
            nmbrStr = 'ASM';
        } else {
            nmbrStr = 'SRV';
        }
        nmbrStr = nmbrStr + generateRandomNumber(4);
        var formNumber = nmbrStr;
        var formName = (req.body.formName) ? req.body.formName : '';
        var formDesc = (req.body.formDesc) ? req.body.formDesc : '';
        var noOfQuestions = (req.body.noOfQuestions) ? req.body.noOfQuestions : '';
        var formExpiration = (req.body.formExpiration) ? req.body.formExpiration : '';
        var failingScore = (req.body.failingScore) ? req.body.failingScore : '';
        var formEndTime = (req.body.formEndTime) ? req.body.formEndTime : '';
        var thankYouMessage = (req.body.thankYouMessage) ? req.body.thankYouMessage : '';

        var moduleId = (req.body.moduleId) ? req.body.moduleId : '';
        // var moduleType = formType;
        // console.log(req.body);
        Form.add(req.session.ctx, formNumber, formName, formDesc, noOfQuestions, formExpiration, failingScore, thankYouMessage, moduleId, moduleType, function (response) {
            console.log(response);
            if (response.status == 'success') {
                var formId = response.data.insertId;
                res.redirect('/question/' + formId);

            } else {
                return res.status(401).send(response.data);
                // responseMsg = commonHelper.commonMsg('failure', 'Something is wrong', null, error);

            }
        });
    } else {
        res.redirect('/');
    }
}

function generateRandomNumber(n) {
    var add = 1, max = 12 - add;   // 12 is the min safe number Math.random() can generate without it starting to pad the end with zeros.   

    if (n > max) {
        return generateRandomNumber(max) + generateRandomNumber(n - max);
    }

    max = Math.pow(10, n + add);
    var min = max / 10; // Math.pow(10, n) basically
    var number = Math.floor(Math.random() * (max - min + 1)) + min;

    return ("" + number).substring(add);
}