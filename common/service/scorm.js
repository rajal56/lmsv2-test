var path = require('path');
var fs = require('fs');
var app = require('../../server/server');
var server = require('http').Server(app);
app.server = server;
var io = require('socket.io')(server);
var sockets = require("../../common/scorm/sockets/sockets").load(io);
console.log(path.join(__dirname));
var scormModel = require('../../common/scorm/models/scorm');
var uuid = require('node-uuid');
var parseString = require('xml2js').parseString;
module.exports.getScormFile = (req, res) => {
    if (req.session.ctx) {
        var url = req.url.split('/sco/')[1];
        console.log(url);
        url = url.split('?')[0];

        var file = path.resolve(__dirname + '../../../client/uploads/course/Scorm/unzipFiles/' + url);

        console.log("Loading scorm file: " + file);

        // At this point we need to check if the file is in scorm package imsmanifest as public file.

        if (fs.existsSync(file)) {
            console.log("file exist");
            res.set('X-Frame-Options', 'SAMEORIGIN');
            // res.send('X-Frame-Options: ' + res.get('X-Frame-Options'))
            // res.set('X-Frame-Options', '')
            res.sendFile(file,
                function (err) {
                    if (err) {
                        res.status(404);
                        res.render('error', {
                            message: 'File not found',
                            error: {
                                'status': '404'
                            }
                        });
                    }
                }
            );
        } else {
            res.status(404);
            res.render('error', {
                message: 'File not found',
                error: {
                    'status': '404'
                }
            });
        }
    } else {
        res.redirect('/');
    }
}
module.exports.scormCommit = (req, res) => {
    if (req.session.ctx) {
        var user_id = req.session.ctx.userId;
        var sco = (req.params.sco == undefined ? false : req.params.sco);
        var SCOInstanceID = (req.params.SCOInstanceID == undefined ? false : req.params.SCOInstanceID);
        var courseId = (req.params.courseId == undefined ? false : req.params.courseId);
        var session = (req.params.session == undefined ? false : req.params.session);
        var data = (req.body.data == undefined ? false : req.body.data);
        console.log("side route");
        if (Object.keys(data).length > 0) {
            var dataTmp = {};
            for (var i in data) {
                dataTmp['data.' + i.toString().replace(/\./g, 'UFF0E')] = data[i].value;
            }
            data = dataTmp;
            // console.log(data);

            if (data['data.cmiUFF0EcoreUFF0Esession_time'] !== null && data['data.cmiUFF0EcoreUFF0Esession_time'] !== undefined) {
                data['sessions.' + session + '.time'] = data['data.cmiUFF0EcoreUFF0Esession_time'];
                delete data['data.cmiUFF0EcoreUFF0Esession_time'];
            }

            scormModel.update({
                'query': {
                    'sco': sco,
                    'SCOInstanceID': SCOInstanceID,
                    'courseId': courseId,
                    'data.cmiUFF0EcoreUFF0Estudent_id': user_id,
                    'ctx': req.session.ctx
                },
                'set': data,
                'upsert': false,
                'success': function () {
                    res.json({
                        success: true
                    });
                },
                'error': function (error) {
                    res.json({
                        success: false,
                        error: error
                    });
                }
            });
        } else {
            res.json({
                'success': true
            });
        }
    } else {
        res.redirect('/');
    }
}

module.exports.getScormPlayer = (req, res) => {
    if (req.session.ctx) {
        var id = (req.params.id == undefined ? false : req.params.id);
        var SCOInstanceID = (req.params.SCOInstanceID == undefined ? false : req.params.SCOInstanceID);
        var courseId = (req.params.courseId == undefined ? false : req.params.courseId);
        console.log(id);
        var imsmanifestPath = path.join(__dirname, '../../client/uploads/course/Scorm/unzipFiles/' + id + '/imsmanifest.xml');
        console.log(imsmanifestPath);
        if (fs.existsSync(imsmanifestPath)) {
            console.log("inside if");
            var imsmanifest = fs.readFileSync(imsmanifestPath);

            parseString(imsmanifest, function (err, result) {
                if (!err) {
                    imsmanifest = result.manifest;
                    console.log(JSON.stringify(imsmanifest));
                    res.render('scorm', {
                        title: 'Scorm - Player',
                        id: id,
                        SCOInstanceID: SCOInstanceID,
                        courseId: courseId,
                        imsmanifest: JSON.stringify(imsmanifest)
                    });
                }
            });
        } else {
            res.status(404);

            res.render('error', {
                message: 'File not found',
                error: {
                    'status': '404'
                }
            });
        }
    } else {
        res.redirect('/');
    }
}
module.exports.scormInitialize = (req, res) => {
    if (req.session.ctx) {
        console.log(req.session);
        var user_id = req.session.ctx.userId;
        var sco = (req.params.sco == undefined ? false : req.params.sco);
        var SCOInstanceID = (req.params.SCOInstanceID == undefined ? false : req.params.SCOInstanceID);
        var courseId = (req.params.courseId == undefined ? false : req.params.courseId);
        // console.log("-----------------" + sco);
        // console.log(scormModel.getOne());
        scormModel.getOne({
            'query': {
                'sco': sco.toString(),
                'SCOInstanceID': SCOInstanceID,
                'courseId': courseId,
                'cmiUFF0EcoreUFF0Estudent_id': user_id,
                'ctx': req.session.ctx
            },
            'scope': [],
            'success': function (result) {
                console.log("inside getOne success");
                console.log(Object.keys(result.data).length);
                var data = {};
                if (Object.keys(result.data).length > 0) {
                    // console.log(result);

                    data = result;
                    // console.log(data);
                } else {
                    data = {
                        'cmi.core.lesson_status': 'not attempted',
                        'cmi.core.student_id': user_id
                    }
                }

                var dataTmp = {};
                for (var i in data) {
                    data[i] = {
                        'value': data[i],
                        'status': 0
                    };

                    dataTmp[i.toString().replace(/UFF0E/g, '.')] = data[i];
                }

                data = dataTmp;
                console.log(data);

                var session = uuid.v4();
                if (data['cmi.core.lesson_status'] != undefined) {
                    if (data['cmi.core.lesson_status'].value == 'not attempted') {
                        scormModel.insert({
                            'data': {
                                'sco': sco,
                                'data': {
                                    'SCOInstanceID': SCOInstanceID,
                                    'courseId': courseId,
                                    'cmiUFF0EcoreUFF0Elesson_status': 'not attempted',
                                    'cmiUFF0EcoreUFF0Estudent_id': user_id,
                                    'ctx': req.session.ctx
                                }
                            },
                            'success': function (id) {
                                res.json({
                                    success: true,
                                    data: data,
                                    session: session
                                });
                            },
                            'error': function (error) {
                                res.json({
                                    success: false,
                                    error: error
                                });
                            }
                        });
                    }
                } else if (data['cmi.core.lesson_status'] == 'not attempted') {
                    scormModel.insert({
                        'data': {
                            'sco': sco,
                            'data': {
                                'SCOInstanceID': SCOInstanceID,
                                'courseId': courseId,
                                'cmiUFF0EcoreUFF0Elesson_status': 'not attempted',
                                'cmiUFF0EcoreUFF0Estudent_id': user_id,
                                'ctx': req.session.ctx
                            }
                        },
                        'success': function (id) {
                            res.json({
                                success: true,
                                data: data,
                                session: session
                            });
                        },
                        'error': function (error) {
                            res.json({
                                success: false,
                                error: error
                            });
                        }
                    });
                } else {
                    data = {};
                    data['sessions.' + session] = {
                        'insert_at': (new Date())
                    };

                    scormModel.update({
                        'query': {
                            'sco': sco,
                            'SCOInstanceID': SCOInstanceID,
                            'courseId': courseId,
                            'data.cmiUFF0EcoreUFF0Estudent_id': user_id,
                            'ctx': req.session.ctx
                        },
                        'set': data,
                        'upsert': true,
                        'success': function () {
                            res.json({
                                success: true,
                                data: data,
                                session: session
                            });
                        },
                        'error': function () {
                            res.json({
                                success: false,
                                'error': error
                            });
                        }
                    });
                }
            },
            'error': function (error) {
                res.json({
                    success: false,
                    'error': error
                });
            }
        });
    } else {
        res.redirect('/');
    }
}