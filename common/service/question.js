var app = require('../../server/server');
var moment = require('moment');
var path = require('path');
var multer = require('multer');
var Formquestions = app.models.formQuestions;
var Formquestionanswer = app.models.formQuestionAnswer;
var fs = require('fs');
var Formcategory = app.models.formCategory;
var courseQuestionBank = path.join(__dirname, '../../client/uploads/course/');
const XLSX = require('xlsx');
// var waterfall = require('async-waterfall');
var async = require('async');
let uniqueId = (new Date()).getTime().toString(36);
var storage_question_File = multer.diskStorage({
    destination: function (req, file, callback) {

        if (!fs.existsSync(courseQuestionBank)) {
            fs.mkdirSync(courseQuestionBank, { recursive: true })
        }

        callback(null, courseQuestionBank);
    },
    filename: function (req, file, callback) {
        callback(null, uniqueId + path.extname(file.originalname));
    }
});
var upload_question_File = multer({ storage: storage_question_File }).single('questionFile');
module.exports.questionAdd = (req, res) => {
    if (req.session.ctx) {
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var formId = req.params.formId;
        Formquestions.list(req.session.ctx, formId, function (response) {
            if (response.status == 'success') {
                if (req.session.msgstatus) {

                    delete req.session.msgstatus;
                    delete req.session.msg;

                }

                res.render('question', {
                    formId: formId,
                    questions: response.data,
                    session: ss

                });

            } else {
                return res.status(401).send(response.data);
                // responseMsg = commonHelper.commonMsg('failure', 'Something is wrong', null, error);

            }
        });


    } else {
        res.redirect('/');
    }
}

module.exports.questionSave = (req, res) => {
    if (req.session.ctx) {

        console.log(req.body);
        var uploadType = (req.body.uploadType) ? req.body.uploadType : '';
        if (uploadType == 'single') {
            var name = (req.body.question) ? req.body.question : '';
            var type = (req.body.question_type) ? req.body.question_type : '';
            var isRequired = (req.body.isRequired) ? req.body.isRequired : 0;

            var options = null;
            var correct_answer = (req.body.correct_answer) ? req.body.correct_answer : '';
            if (type == 'multiple_choice') {
                correct_answer = req.body.correct_answer;
            } else if (type == "true_false") {
                correct_answer = req.body.correct_answer;
            }

            if (type == 'multiple_choice') {
                options = [{ "label": req.body.option1, "score": "" }, { "label": req.body.option2, "score": "" }, { "label": req.body.option3, "score": "" }, { "label": req.body.option4, "score": "" }];
            } else if (type == "true_false") {
                options = [{ "label": req.body.option1, "score": "" }, { "label": req.body.option2, "score": "" }];
            }
            // var correctAnswer = 
            // var options = (req.body.options) ? req.body.options : '';
            var category = (req.body.formCategoryName) ? req.body.formCategoryName : '';
            var categoryId = (req.body.categoryId) ? req.body.categoryId : null;
            var formId = (req.body.formId) ? req.body.formId : '';
            // var isRequired = (req.body.isRequired) ? req.body.isRequired : '';
            var categorySuggestions = (req.body.categorySuggestions) ? req.body.categorySuggestions : '';
            var questionId = "";
            var formAnswerScore = (req.body.formAnswerScore) ? req.body.formAnswerScore : null;
            var data = "";
            if (category != "") {

                async.waterfall([
                    function (done) {
                        Formcategory.add(req.session.ctx, category, formId, categorySuggestions, function (categoryResponse) {

                            if (categoryResponse.status == 'success') {
                                var insertedId = categoryResponse.data.insertId
                                done(null, insertedId)
                            } else {
                                done(categoryResponse.data);
                            }
                        });

                    },
                    function (categoryId, done) {
                        Formquestions.add(req.session.ctx, name, type, isRequired, options, categoryId, formId, function (response) {
                            if (response.status == 'success') {
                                questionId = response.data.insertId;
                                done(null, [categoryId, questionId]);

                            } else {
                                done(response);
                            }

                        });
                    }, function (data, done) {
                        var categoryId = data[0];
                        var questionId = data[0];
                        Formquestionanswer.add(req.session.ctx, correct_answer, formId, categoryId, questionId, type, formAnswerScore, function (answerResponse) {
                            if (answerResponse.status == "success") {
                                done(null, answerResponse);
                            } else {

                                done(answerResponse);
                            }
                        })
                    }
                ], function (err, result) {
                    if (err) {

                        data = err;
                        req.session.msgstatus = data.status;
                        req.session.msg = data.msg;
                        res.redirect('/question/' + formId);

                    } else {

                        data = result;
                        req.session.msgstatus = data.status;
                        req.session.msg = data.msg;
                        res.redirect('/question/' + formId);
                    }
                });

            } else {
                // data = addQuestion(uploadType, req.session.ctx, res, req, name, type, isRequired, options, categoryId, formId, correct_answer, formAnswerScore);
                async.waterfall([
                    function (done) {
                        Formquestions.add(req.session.ctx, name, type, isRequired, options, categoryId, formId, function (response) {
                            if (response.status == 'success') {
                                questionId = response.data.insertId;
                                done(null, [categoryId, questionId]);

                            } else {
                                done(response);
                            }

                        });
                    }, function (data, done) {
                        var categoryId = data[0];
                        var questionId = data[0];
                        Formquestionanswer.add(req.session.ctx, correct_answer, formId, categoryId, questionId, type, formAnswerScore, function (answerResponse) {
                            if (answerResponse.status == "success") {
                                done(null, answerResponse);
                            } else {
                                // console.log(answerResponse);
                                done(answerResponse);
                            }
                        })
                    }
                ], function (err, result) {
                    if (err) {
                        data = err;
                        req.session.msgstatus = data.status;
                        req.session.msg = data.msg;
                        res.redirect('/question/' + formId);
                    } else {
                        data = result;
                        req.session.msgstatus = data.status;
                        req.session.msg = data.msg;
                        res.redirect('/question/' + formId);
                    }
                });


            }
            console.log("data value");
            console.log(data);

        } else {
            upload_question_File(req, res, function (error) {
                if (error) {
                    return res.status(401).send(error);
                } else {
                    // console.log(req.file);
                    var filename = req.file.filename;
                    var workbook = XLSX.readFile(courseQuestionBank + '/' + filename);
                    var sheet_name_list = workbook.SheetNames;
                    data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
                    // console.log(typeof (data));
                    // console.log(Object.keys(data).length);
                    var i = 1;
                    var noElements = Object.keys(data).length;
                    for (var key of Object.keys(data)) {

                        addQuestion(data[key], req, res, i, noElements);
                        i = i + 1;


                    }
                }
            });
        }

    } else {
        res.redirect('/');
    }
}
function addQuestion(data, req, res, i, noElements) {
    setTimeout(function () {
        console.log(data);
        var name = (data['name'] != undefined) ? data['name'] : "";
        var type = (data['type'] != undefined) ? data['type'] : "";
        var isRequired = (data['is_required'] != undefined) ? data['is_required'] : "";
        var correct_answer = (data['correct_answer'] != undefined) ? data['correct_answer'] : "";
        var options = null;
        if (type == 'multiple_choice') {
            options = [{ "label": data['option1'], "score": "" }, { "label": data['option2'], "score": "" }, { "label": data['option3'], "score": "" }, { "label": data['option4'], "score": "" }];
        } else if (type == "true_false") {
            options = [{ "label": data['option1'], "score": "" }, { "label": data['option2'], "score": "" }];
        }
        var category = (data['category'] != undefined) ? data['category'] : "";
        var categoryId = (req.body.categoryId) ? req.body.categoryId : null;
        var formId = (req.body.formId != undefined) ? req.body.formId : '';

        var categorySuggestions = (req.body.categorySuggestions != undefined) ? req.body.categorySuggestions : '';
        var formAnswerScore = (data['score'] != undefined) ? data['score'] : null;
        console.log(type);
        Formquestions.add(req.session.ctx, name, type, isRequired, options, categoryId, formId, function (response) {
            if (response.status == 'success') {
                questioId = response.data.insertId;
                Formquestionanswer.add(req.session.ctx, correct_answer, formId, categoryId, questioId, type, formAnswerScore, function (answerResponse) {
                    if (answerResponse.status == "success") {
                        if (i == noElements) {
                            // console.log("equal success");
                            req.session.msgstatus = data.status;
                            req.session.msg = data.msg;
                            res.redirect('/question/' + formId);
                        }
                    } else {

                        // console.log(answerResponse);
                        return res.status(401).send(answerResponse.data);

                    }
                });

            } else {
                // console.log(response);
                return res.status(401).send(response.data);
            }

        })
    }, 2000 * i);
}



