var app = require('../../server/server');
var Team = app.models.Team;
var Users = app.models.Users;
var moment = require('moment');
// var session = app.model

module.exports.list = (req, res) => {
    //console.log(req.session);
   
    if (req.session.ctx) {
        var ctx = req.session.ctx;
        // console.log(req.session);
        var ss = [{ msgstatus: req.session.msgstatus, msg: req.session.msg }];
        var currentPage = (req.query.page != undefined || req.query.page != 0) ? req.query.page : 1;
        var pageLimit = (req.query.limit != undefined) ? req.query.limit : 10;
        var totalPages = 1;
        var skip = 0;
        var order = "createdDate DESC";
        var totalCount = 0;
        var searchString = (req.query.search != undefined) ? req.query.search : '';
        var filter = { "limit": pageLimit, "start": skip, "order": order, "search": searchString };
       
        skip = (currentPage - 1) * pageLimit;
        totalPages = Math.ceil(totalCount / pageLimit);

        Team.list(ctx, filter, function (err, response) {
            
            if (response.code == 200 || 404) {
                if (req.session.msgstatus) {

                    delete req.session.msgstatus;
                    delete req.session.msg;

                }
                totalCount = response.data.totalCount;
                totalPages = Math.ceil(totalCount / pageLimit);

                var msg = (req.query.status) ? req.query.status : '';
                res.render('team-list', {
                    title: 'Team list',
                    data: response.data.list,
                    msg: msg,
                    moment: moment,
                    pageSize: pageLimit,
                    pageCount: totalPages,
                    currentPage: currentPage,
                    hasPreviousPage: currentPage > 1,
                    hasNextPage: (pageLimit * currentPage) < totalCount,
                    session: ss,
                    redirectTo: '/team'
                });

            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/');
    }
}

module.exports.form = (req, res) => {
    
    if (req.session.ctx) {
        Users.dropdown(req.session.ctx, function (err, response) {
            req.session.msgstatus = response.status;
            req.session.msg = response.msg;
            if (response.status == 'success') {

                res.render('team-form',
                {
                    data: '',
                    ddList: response.data.list
                });
            }else{
                res.redirect('/team_list');
            }
        });
    } else {
        res.redirect('/team_list');
    }
}

module.exports.add = (req, res) => {
   
    if (req.session.ctx) {

        var ctx = req.session.ctx;
        var teamName = (req.body.teamName != undefined) ? req.body.teamName : '';
        var status = (req.body.status != undefined) ? req.body.status : '';
        var users = (req.body.users != undefined) ? req.body.users : '';
        var description = (req.body.description != undefined) ? req.body.description : '';
        //console.log("request user", teamName, status, description);
        Team.add(ctx, teamName, status, users, description, function (err, response) {
          
            if (response.code == 200 || 404) {
                res.redirect('/team_list');
               /*  res.render('team-list', {
                    data: response.data
                }); */
            } else {
                return res.status(401).send(response.data);
            }
        });
    } else {
        res.redirect('/team_form');
    }
}

module.exports.delete = (req, res) => {
  
    if (req.session.ctx) {

        var teamId = req.params.id;
       
        Team.delete(req.session.ctx, teamId, function (err, response) {
            
            req.session.msgstatus = response.status;
            req.session.msg = response.msg;
            if (response.status == 'success') {

                res.redirect('/team_list');

            } else {
                // return res.status(401).send(response.statusMsg);
                res.redirect('/team_list');
            }
        });
    } else {
        res.redirect('/team_list');
    }
}

module.exports.edit = (req, res) => {
    if (req.session.ctx) {
        var id = req.params.id;
        Team.detailsById(req.session.ctx, id, function (err, response) {
            Users.dropdown(req.session.ctx, function (userDDerr, usrDDres) {
                
                if (response.status == 'success' && usrDDres.status == 'success') {
              
                    res.render('team-form',
                        {
                            data: response.data,
                            ddList: usrDDres.data.list
                        });

                } else {
                    res.redirect('/team_list');
                    //return res.status(401).send(response.statusMsg);
                }
            });
        });
    } else {
        res.redirect('/');
    }
}

module.exports.update = (req, res) => {
    if (req.session.ctx) {

        var ctx = req.session.ctx;
        var id = req.params.id;
        var teamName = (req.body.teamName != undefined) ? req.body.teamName : '';
        var status = (req.body.status != undefined) ? req.body.status : '';
        var users = (req.body.users != undefined) ? req.body.users : '';
        var description = (req.body.description != undefined) ? req.body.description : '';
       
        Team.modify(ctx, id, teamName, status, users, description, function (err, response) {
        
            if (response.code == 200 || 404) {

                res.redirect('/team_list');

            } else {
                return res.status(401).send(response.statusMsg);
            }
        });
    } else {
        res.redirect('/');
    }
}