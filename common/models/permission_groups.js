'use strict';

module.exports = function (PermissionGroups) {
  PermissionGroups.disableRemoteMethodByName('create');
  PermissionGroups.disableRemoteMethodByName('replaceOrCreate');
  PermissionGroups.disableRemoteMethodByName('upsertWithWhere');
  PermissionGroups.disableRemoteMethodByName('change_stream');
  PermissionGroups.disableRemoteMethodByName('upsert');
  PermissionGroups.disableRemoteMethodByName('updateAll');
  PermissionGroups.disableRemoteMethodByName('prototype.updateAttributes');
  PermissionGroups.disableRemoteMethodByName('find');
  PermissionGroups.disableRemoteMethodByName('findById');
  PermissionGroups.disableRemoteMethodByName('findOne');
  PermissionGroups.disableRemoteMethodByName('deleteById');
  PermissionGroups.disableRemoteMethodByName('confirm');
  PermissionGroups.disableRemoteMethodByName('count');
  PermissionGroups.disableRemoteMethodByName('exists');
  PermissionGroups.disableRemoteMethodByName('resetPassword');
  PermissionGroups.disableRemoteMethodByName('prototype.__count__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__create__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__delete__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__findById__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__get__accessTokens');
  PermissionGroups.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  PermissionGroups.disableRemoteMethodByName('replaceById');
  PermissionGroups.disableRemoteMethodByName('createChangeStream');
  PermissionGroups.disableRemoteMethodByName('change-password');
  PermissionGroups.disableRemoteMethodByName('reset-password');
  PermissionGroups.disableRemoteMethodByName('verify');

};
