'use strict';
var commonHelper = require('../../helper/commonhelper');
var constants = require('../../helper/constant');

module.exports = function (Activities) {

  Activities.disableRemoteMethodByName('create');
  Activities.disableRemoteMethodByName('replaceOrCreate');
  Activities.disableRemoteMethodByName('upsertWithWhere');
  Activities.disableRemoteMethodByName('change_stream');
  Activities.disableRemoteMethodByName('upsert');
  Activities.disableRemoteMethodByName('updateAll');
  Activities.disableRemoteMethodByName('prototype.updateAttributes');
  Activities.disableRemoteMethodByName('find');
  Activities.disableRemoteMethodByName('findById');
  Activities.disableRemoteMethodByName('findOne');
  Activities.disableRemoteMethodByName('deleteById');
  Activities.disableRemoteMethodByName('createChangeStream');
  Activities.disableRemoteMethodByName('replaceById');
  Activities.disableRemoteMethodByName('confirm');
  Activities.disableRemoteMethodByName('count');
  Activities.disableRemoteMethodByName('exists');
  Activities.disableRemoteMethodByName('prototype.__count__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__create__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__delete__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__get__accessTokens');
  Activities.disableRemoteMethodByName('prototype.__updateById__accessTokens');

  Activities.remoteMethod(
    'list', {
      http: {
        status: 200,
        errorStatus: 400,
        verb: 'get'
      },
      description: 'Get Activities list',
      accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'moduleId',
        type: 'number',
        required: true,
        description: 'Enter module id ie.opportunities or leads or estimates or invoice or tasks or bug'
      },
      {
        arg: 'type',
        type: 'string',
        required: true,
        description: 'Enter type ie. opportunities or leads or estimates or invoice or tasks or bug'
      },
        {
          arg: 'filter',
          type: 'object',
          required: true,
          description: 'Filter defining start and limit- must be a JSON - encoded string Example format :{"start":0,"limit":10}',
        },
      ],
      returns: {
        arg: 'data',
        type: [],
        root: true
      },
    }
  );

  Activities.list = function(ctx, moduleId, type, filter, callback) {

    var companyKey = ctx.req.userDetail.tenantKey;
    var fields, secondJoin, fromParam, qryParam, sql, whereParams, responseMsg,
      orderClause,limitParam, whereParamsWithLimit;

    var fieldCount = 'count(*) AS count';

    fields = ' tbl_activities.sub_module_name AS subModuleName, tbl_activities.currentValue, tbl_activities.oldValue, tbl_activities.activity, tbl_activities.activity_date AS activityDate, ' + 'tbl_users.fullName, IF(tbl_users.user_type = 1 , \'#Admin\' , (IF(tbl_users.user_type = 2 , \'#Instructor\' , IF(tbl_users.user_type = 3 , \'#User\', \'\')))) AS userType ';

    fromParam = ' tbl_activities LEFT JOIN tbl_users ON tbl_users.user_id = tbl_activities.user ';
    qryParam = ' tbl_activities.module = ? AND tbl_activities.module_id = ? AND tbl_activities.tenant_company_key = ? AND tbl_activities.is_visible = \'1\' ';

    orderClause = ' `tbl_activities`.`activities_id` DESC ';
    limitParam = [ctx.args.filter.start, ctx.args.filter.limit];
    whereParams = [type, moduleId, companyKey];

    sql = 'SELECT ' + fields + ' FROM ' + fromParam + ' WHERE ' + qryParam + ' ORDER BY ' + orderClause + 'LIMIT ?,?';
    whereParamsWithLimit = whereParams.concat(limitParam);
    var ds = Activities.dataSource;
    ds.connector.query(sql, whereParamsWithLimit, function(err, savedActivities) {
      if (err) {
        responseMsg = commonHelper.commonMsg('failure', 400, constants.sqlErrMsg, '', err);
        callback(null, responseMsg);
      } else {
        if (savedActivities.length > 0) {
          sql = 'SELECT ' + fieldCount + ' FROM ' + fromParam + ' WHERE ' + qryParam;
          ds.connector.query(sql, whereParams, function(err1, totalCount){
            if(err1){
              responseMsg = commonHelper.commonMsg('failure', 400, constants.sqlErrMsg, '', err);
              callback(null, responseMsg);
            } else {
              savedActivities.forEach(function (activity, index) {
              
                if (activity.activity && savedActivities[index]) {
                  var activities = activity.activity.replace(/activity[_-]|[_-]/g, " ");
                  activities = activities.trim();
                  activities = activities.charAt(0).toUpperCase() + activities.slice(1);
                  savedActivities[index].activity = activities;
                }
              });
              responseMsg = commonHelper.commonMsg('success', 200, 'Activities list fetched successfully', {
                list: savedActivities,
                totalCount: totalCount[0].count
              },  '');
              callback(null, responseMsg);
            }
          });
        } else {
          responseMsg = commonHelper.commonMsg('failure', 400, constants.noData, {
            list: [],
            totalCount: 0
          },  '');
          callback(null, responseMsg);
        }
      }
    });
  };

};
