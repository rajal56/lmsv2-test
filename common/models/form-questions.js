'use strict';
var commonHelper = require('../../helper/commonhelper');
module.exports = function (Formquestions) {
    Formquestions.disableRemoteMethodByName('create');
    Formquestions.disableRemoteMethodByName('replaceOrCreate');
    Formquestions.disableRemoteMethodByName('upsertWithWhere');
    Formquestions.disableRemoteMethodByName('change_stream');
    Formquestions.disableRemoteMethodByName('upsert');
    Formquestions.disableRemoteMethodByName('updateAll');
    Formquestions.disableRemoteMethodByName('prototype.updateAttributes');
    Formquestions.disableRemoteMethodByName('find');
    Formquestions.disableRemoteMethodByName('findById');
    Formquestions.disableRemoteMethodByName('findOne');
    Formquestions.disableRemoteMethodByName('deleteById');
    Formquestions.disableRemoteMethodByName('confirm');
    Formquestions.disableRemoteMethodByName('count');
    Formquestions.disableRemoteMethodByName('exists');
    Formquestions.disableRemoteMethodByName('verify');
    Formquestions.disableRemoteMethodByName('replaceById');
    Formquestions.disableRemoteMethodByName('createChangeStream');
    Formquestions.disableRemoteMethodByName('create');


    var responseMsg = '';

    Formquestions.add = function (ctx, name, type, isRequired, options, categoryId, formId, callback) {
        var ds = Formquestions.dataSource;
        // var userId = ctx.userId;
        // console.log(options);
        var option = JSON.stringify(options);
        // console.log(option);
        var tenantKey = ctx.tenantKey;

        var sql = `INSERT INTO tbl_form_questions(name, type, isRequired, options, categoryId, formId, createdDate, questionOrder, tenantKey) VALUES(?,?,?,?,?,?,now(),questionOrder+1,?)`;
        ds.connector.query(sql, [name, type, isRequired, option, categoryId, formId, tenantKey], function (err, result) {
            // console.log(result);
            // console.log(err);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                // console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Question added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Formquestions.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'name',
                type: 'string',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'type',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'isRequired',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'options',
                type: 'object',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'categoryId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'questionOrder',
                type: 'number',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });


    Formquestions.list = function (ctx, formId, callback) {
        var ds = Formquestions.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        // console.log(ds);
        var sql = `SELECT tfq.*,tfc.formCategoryName FROM tbl_form_questions tfq LEFT JOIN (tbl_form_category tfc) ON (tfc.formId = tfq.formId AND tfc.isDeleted=0 AND tfc.formCategoryId=tfq.categoryId) WHERE tfq.formId = ${formId} AND tfq.tenantKey='${tenantKey}'`;
        // console.log(sql);
        ds.connector.query(sql, function (err, result) {
            // console.log(result);
            // console.log(err);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                // console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Question list', result, '');
                callback(responseMsg);
            }
        });
    };
    Formquestions.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of questions',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formId',
                type: 'number',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

};
