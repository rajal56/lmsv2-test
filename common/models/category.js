'use strict';
var crypto = require('crypto');
var app = require('../../server/server');
var commonHelper = require('../../helper/commonhelper');
var jwt = require('jsonwebtoken');
var constants = require('../../helper/constant');
var sendMail = require('../../helper/sendMail');
var config = require('./../../server/config');
var dateTime = require('node-datetime');
var responseMsg;

module.exports = function (Category) {
  Category.disableRemoteMethodByName('create');
  Category.disableRemoteMethodByName('replaceOrCreate');
  Category.disableRemoteMethodByName('upsertWithWhere');
  Category.disableRemoteMethodByName('change_stream');
  Category.disableRemoteMethodByName('upsert');
  Category.disableRemoteMethodByName('updateAll');
  Category.disableRemoteMethodByName('prototype.updateAttributes');
  Category.disableRemoteMethodByName('find');
  Category.disableRemoteMethodByName('findById');
  Category.disableRemoteMethodByName('findOne');
  Category.disableRemoteMethodByName('deleteById');
  Category.disableRemoteMethodByName('confirm');
  Category.disableRemoteMethodByName('count');
  Category.disableRemoteMethodByName('exists');
  Category.disableRemoteMethodByName('resetPassword');
  Category.disableRemoteMethodByName('prototype.__count__accessTokens');
  Category.disableRemoteMethodByName('prototype.__create__accessTokens');
  Category.disableRemoteMethodByName('prototype.__delete__accessTokens');
  Category.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  Category.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Category.disableRemoteMethodByName('prototype.__get__accessTokens');
  Category.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Category.disableRemoteMethodByName('replaceById');
  Category.disableRemoteMethodByName('createChangeStream');
  Category.disableRemoteMethodByName('change-password');
  Category.disableRemoteMethodByName('reset-password');
  Category.disableRemoteMethodByName('verify');

  Category.remoteMethod('list', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get list of Category',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'filter',
        type: 'object',
        'http': {
          source: 'query'
        },
        description: 'Filter defining order, start, limit and search - must be a JSON - encoded string Example format :  {"limit":10,"start":0,"order":"date DESC","search":"xyz"}',
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Category.list = function (ctx, filter, callback) {

    var loginUser = ctx.userId;
    var tenantKey = ctx.tenantKey;
    var responseMsg;
    var ds = Category.dataSource;

    let promise = new Promise(function (resolve, reject) {
       var userQry = 'select tbl_category.categoryId, tbl_category.categoryName, tbl_category.createdDate, tbl_category.status, tbl_users.fullName from tbl_category LEFT JOIN `tbl_users` ON `tbl_users`.`userId` = `tbl_category`.`createdBy` where tbl_category.isDeleted = ? AND tbl_category.tenantKey = ? Order by tbl_category.categoryId DESC';
       var params = [0, tenantKey];
       ds.connector.query(userQry, params, function (err, catList) {
        catList = JSON.parse(JSON.stringify(catList)); 
         if (err) {
           reject(err);
         } else {
           resolve(catList);
         }
       });
    }).then(function (catList) {
      if (catList.length > 0) {
        var sql = 'select count(*) AS count from tbl_category LEFT JOIN `tbl_users` ON `tbl_users`.`userId` = `tbl_category`.`createdBy` where tbl_category.isDeleted = ? AND tbl_category.tenantKey = ?';
        var params = [0, tenantKey];
        ds.connector.query(sql, params, function (err, catCount) {
          if (err) {
            responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', null, err);
            callback(null, responseMsg);
          } else {
            var totalCount = catCount[0].count;
            responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', {
              list: catList,
              totalCount: totalCount
            }, '');
            callback(null, responseMsg);
          }
        });
      } else {
        responseMsg = commonHelper.commonMsg('failure', 404, 'No data found', {
          list: [],
          totalCount: 0
        });
        callback(null, responseMsg);
      }
    }).catch(function (err) {
      responseMsg = commonHelper.commonMsg('failure', 500, 'Error!', null, err);
      callback(null, responseMsg);
    });
  }
  
  Category.remoteMethod('add', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Create a new instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'categoryName',
        type: 'string',
        description: 'Please provide name of category',
        required: true
      },
      {
        arg: 'categoryDescription',
        type: 'string',
        description: 'More info regarding Description',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Category.add = function (ctx, categoryName, status, categoryDescription, callback) {
    
    var loginUser = ctx.userId; 
    var tenantKey=ctx.tenantKey;
    var catStatus = status ? status : "inactive";
    
    Category.create({
      "categoryName": categoryName,
      "categoryDescription": categoryDescription,
      "createdBy": loginUser,
      "status": catStatus,
      "isDeleted": 0,
      "tenantKey": tenantKey
    },
    function (err1, catRes) {
    
      if (err1) {
        var responseMsg = commonHelper.commonMsg('failure', 400, 'Category create failed', '', err1);
        callback(err1, responseMsg);
      }else{
        var activities = {
          "activity": 'New Category created',
          "module": 'category',
          "moduleId": catRes.categoryId,
          "tenantKey": tenantKey,
          "user": loginUser,
          "value1": categoryName,
          "value2": null,
          "icon": 'fa-user',
          "note": null,
          "status": null,
          'createdBy': loginUser,
        };
        commonHelper.activity(activities, function (err, rows) {
         
          if (err) {
            responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
            callback(err, responseMsg)
          } else {
            responseMsg = commonHelper.commonMsg('success', 201, 'Category added successfully', catRes);
            callback(null, responseMsg);
          }
        });
      }
    });
     
  }

  Category.remoteMethod(
    'delete', {
      http: {
        status: 200,
        errorStatus: 400,
        path: '/delete/:id',
        verb: 'delete'
      },
      description: 'Delete Category',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'id',
          type: 'string',
          description: 'Enter Category id\'s that need to be deleted (use comma separated values for multiple Category)',
          required: true
        }
      ],
      returns: [

        {
          arg: "result",
          http: {
            source: 'result'
          }
        }
      ]
    }
  );

  Category.delete = function (ctx, categoryId, callback) {
    var loginUser = ctx.userId; 
    var tenantKey=ctx.tenantKey;
    var responseMsg = '';
    var dt = dateTime.create();
    var formattedDateTime = dt.format('Y-m-d H:M:S');
    Category.update({
      'categoryId': categoryId,
      'tenantKey': tenantKey
    }, {
      'isDeleted': 1,
      'deletedDateTime': formattedDateTime,
      'activated': 0
    },
      function (err, result) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'Category delete failed ', '', err);
          callback(null, responseMsg);
        } else {
          if (result.count === 1) {

            var activities = {
              'user': loginUser,
              'module': 'Category',
              'moduleId': categoryId,
              'activity': 'activity_delete_Category',
              'value1': categoryId,
              "value2": null,
              "icon": 'fa-circle-o',
              "note": null,
              "status": null,
              "tenantKey": tenantKey,
              'createdBy': loginUser,
            };
            //save Acitivty
            commonHelper.activity(activities, function (err1, rows) {
              if (err1) {
                responseMsg = commonHelper.commonMsg('failure', 500, ' Category delete failed ', '', err1);
              } else {
                responseMsg = commonHelper.commonMsg('success', 200, ' Category deleted successfully ', result);
              }
              callback(null, responseMsg);
            });
          } else {
            responseMsg = commonHelper.commonMsg('failure', 404, 'Category not found ');
            callback(null, responseMsg);
          }
        }
      });
  }

  Category.remoteMethod('status',
  {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/status/:status/:id',
      verb: 'post'
    },
    description: 'Change Category status (Active/Deactive)',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'status',
        type: 'number',
        description: 'Enter status to be changed (1:Activate 0:Deactivate)',
        required: true
      },
      {
        arg: 'id',
        type: 'number',
        description: 'Enter Category id whos status should be changed',
        required: true
      }
    ],
    returns: [{
      arg: 'result'
    }]
  });

  Category.status = function (ctx, status, CategoryId, callback) {
    var loginUser = ctx.req.userDetail.userId;
    var tenantKey = ctx.req.CategoryDetail.tenantKey;
    Category.update({
      'CategoryId': CategoryId,
      'tenantKey': tenantKey
    }, {
      'activated': status,
    },
      (err, res) => {
        if (err) {
          var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
          callback(null, responseMsg);
        } else if (res.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'Category',
            'moduleField': CategoryId,
            'activity': 'activity_change_status',
            'icon': 'fa-user',
            'value1': status,
            "value2": null,
            "icon": 'fa-user',
            "note": null,
            "status": null,
            "tenantKey": tenantKey,
            'createdBy': loginUser,
          };

          commonHelper.activity(activities, function (err, rows) {
            if (err) {
              var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
              callback(null, responseMsg);
            } else {
              if (status) {
                var msg = "activated";
              } else {
                var msg = "deactivated";
              }
              var responseMsg = commonHelper.commonMsg('success', 200, 'Category ' + msg + ' successfully!', '', err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 404, 'Category not found!', '', err);
          callback(null, responseMsg);
        }
      }
    );
  }

  Category.remoteMethod('detailsById', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get Category details',
    accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'CategoryId',
        type: 'string',
        description: 'Enter Category id to fetch details',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Category.detailsById = function (ctx, categoryId, callback) {
    var loginUser = ctx.userId; 
    var tenantKey=ctx.tenantKey;
    var responseMsg;
    Category.findOne({
      fields: { tenantKey: false, isdeleted: false, deletedatetime: false },
      where: {
        categoryId: categoryId,
        isdeleted: 0,
        tenantKey: tenantKey,
      },
    }, function (err, response) {
      if (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'error', '', err);
      } else {
        if (response) {
          responseMsg = commonHelper.commonMsg('success', 200, 'Category details fetched successfully', response);
        } else {
          responseMsg = commonHelper.commonMsg('success', 404, 'No Category found', response);
        }
      }
      callback(null, responseMsg);
    });
  };

};
