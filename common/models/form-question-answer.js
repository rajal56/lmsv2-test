'use strict';
var commonHelper = require('../../helper/commonhelper');
module.exports = function (Formquestionanswer) {
    Formquestionanswer.disableRemoteMethodByName('create');
    Formquestionanswer.disableRemoteMethodByName('replaceOrCreate');
    Formquestionanswer.disableRemoteMethodByName('upsertWithWhere');
    Formquestionanswer.disableRemoteMethodByName('change_stream');
    Formquestionanswer.disableRemoteMethodByName('upsert');
    Formquestionanswer.disableRemoteMethodByName('updateAll');
    Formquestionanswer.disableRemoteMethodByName('prototype.updateAttributes');
    Formquestionanswer.disableRemoteMethodByName('find');
    Formquestionanswer.disableRemoteMethodByName('findById');
    Formquestionanswer.disableRemoteMethodByName('findOne');
    Formquestionanswer.disableRemoteMethodByName('deleteById');
    Formquestionanswer.disableRemoteMethodByName('confirm');
    Formquestionanswer.disableRemoteMethodByName('count');
    Formquestionanswer.disableRemoteMethodByName('exists');
    Formquestionanswer.disableRemoteMethodByName('verify');
    Formquestionanswer.disableRemoteMethodByName('replaceById');
    Formquestionanswer.disableRemoteMethodByName('createChangeStream');
    Formquestionanswer.disableRemoteMethodByName('create');


    var responseMsg = '';

    Formquestionanswer.add = function (ctx, formAnswer, formId, formCategoryId, formQuestionId, formQuestionType, formAnswerScore, callback) {
        var ds = Formquestionanswer.dataSource;
        // var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = `INSERT INTO tbl_form_answer(formAnswer, formId, formCategoryId, formQuestionId, formQuestionType, formAnswerScore,createdDate, tenantKey) VALUES(?,?,?,?,?,?,now(),?)`;
        ds.connector.query(sql, [formAnswer, formId, formCategoryId, formQuestionId, formQuestionType, formAnswerScore, tenantKey], function (err, result) {
            console.log(result);
            console.log(err);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'correct answer added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Formquestionanswer.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formAnswer',
                type: 'number',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formCategoryId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formQuestionId',
                type: 'object',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formQuestionType',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formAnswerScore',
                type: 'number',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Formquestionanswer.list = function (ctx, formId, callback) {
        var ds = Formquestionanswer.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        // console.log(ds);
        var sql = `SELECT tfa.* FROM tbl_form_answer tfa WHERE tfa.formId = ${formId} AND tfa.tenantKey='${tenantKey}'`;
        // console.log(sql);
        ds.connector.query(sql, function (err, result) {
            // console.log(result);
            // console.log(err);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Question list', result, '');
                callback(responseMsg);
            }
        });
    };
    Formquestionanswer.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of questions',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formId',
                type: 'number',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

};
