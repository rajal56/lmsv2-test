'use strict';
var commonHelper = require('../../helper/commonhelper');
module.exports = function (Form) {
    Form.disableRemoteMethodByName('create');
    Form.disableRemoteMethodByName('replaceOrCreate');
    Form.disableRemoteMethodByName('upsertWithWhere');
    Form.disableRemoteMethodByName('change_stream');
    Form.disableRemoteMethodByName('upsert');
    Form.disableRemoteMethodByName('updateAll');
    Form.disableRemoteMethodByName('prototype.updateAttributes');
    Form.disableRemoteMethodByName('find');
    Form.disableRemoteMethodByName('findById');
    Form.disableRemoteMethodByName('findOne');
    Form.disableRemoteMethodByName('deleteById');
    Form.disableRemoteMethodByName('confirm');
    Form.disableRemoteMethodByName('count');
    Form.disableRemoteMethodByName('exists');
    Form.disableRemoteMethodByName('verify');
    Form.disableRemoteMethodByName('replaceById');
    Form.disableRemoteMethodByName('createChangeStream');
    Form.disableRemoteMethodByName('create');


    var responseMsg = '';

    Form.add = function (ctx, formNumber, formName, formDesc, noOfQuestions, formExpiration, failingScore, thankYouMessage, moduleId, moduleType, callback) {
        var ds = Form.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = `INSERT INTO tbl_forms(formNumber,formName,formDesc,noOfQuestions,formExpiration,failingScore,createdBy,thankYouMessage,createdDate,tenantKey,moduleId,moduleType) VALUES(?,?,?,?,?,?,?,?,now(),?,?,?)`;
        ds.connector.query(sql, [formNumber, formName, formDesc, noOfQuestions + 1, formExpiration, failingScore, userId, thankYouMessage, tenantKey, moduleId, moduleType], function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Form added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Form.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formNumber',
                type: 'string',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formName',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formDesc',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'noOfQuestions',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'formExpiration',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'failingScore',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'thankYouMessage',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'moduleId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'moduleType',
                type: 'string',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });
};
