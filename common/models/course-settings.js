'use strict';
var commonHelper = require('../../helper/commonhelper');
var moment = require('moment');
module.exports = function (Coursesettings) {
    Coursesettings.disableRemoteMethodByName('create');
    Coursesettings.disableRemoteMethodByName('replaceOrCreate');
    Coursesettings.disableRemoteMethodByName('upsertWithWhere');
    Coursesettings.disableRemoteMethodByName('change_stream');
    Coursesettings.disableRemoteMethodByName('upsert');
    Coursesettings.disableRemoteMethodByName('updateAll');
    Coursesettings.disableRemoteMethodByName('prototype.updateAttributes');
    Coursesettings.disableRemoteMethodByName('find');
    Coursesettings.disableRemoteMethodByName('findById');
    Coursesettings.disableRemoteMethodByName('findOne');
    Coursesettings.disableRemoteMethodByName('deleteById');
    Coursesettings.disableRemoteMethodByName('confirm');
    Coursesettings.disableRemoteMethodByName('count');
    Coursesettings.disableRemoteMethodByName('exists');
    Coursesettings.disableRemoteMethodByName('verify');
    Coursesettings.disableRemoteMethodByName('replaceById');
    Coursesettings.disableRemoteMethodByName('createChangeStream');
    Coursesettings.disableRemoteMethodByName('create');

    var responseMsg = '';
    Coursesettings.add = function (ctx, courseId, contentLiberary, courseCategory, moduleOrder, newWindow, duteDateOrCompliance, complianceDueDate, complianceTimeSpan, complianceEndDate, complianceFor, dueDate, dueDateTimeSpan, automaticRetake, additionalReminder, courseInactivationDate, removeAccessDate, removeAccessTime, certificateTemplate, enableDescussionForum, wantToSellCourse, callback) {
        var ds = Coursesettings.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = `INSERT INTO tbl_course_settings(courseId,tenantKey,contentLiberary, courseCategory, moduleOrder, newWindow, duteDateOrCompliance,complianceDueDate,complianceTimeSpan,complianceEndDate,complianceFor,dueDate,dueDateTimeSpan,automaticRetake,additionalReminder,courseInactivationDate,removeAccessDate,removeAccessTime,certificateTemplate,enableDescussionForum,wantToSellCourse) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
        ds.connector.query(sql, [courseId, tenantKey, contentLiberary, courseCategory, moduleOrder, newWindow, duteDateOrCompliance, complianceDueDate, complianceTimeSpan, complianceEndDate, complianceFor, dueDate, dueDateTimeSpan, automaticRetake, additionalReminder, courseInactivationDate, removeAccessDate, removeAccessTime, certificateTemplate, enableDescussionForum, wantToSellCourse], function (err, result) {
            if (err) {
                console.log(err);
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Course settings added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Coursesettings.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'add course settings',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'courseId',
                type: 'number',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'contentLiberary',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'courseCategory',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'moduleOrder',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'newWindow',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'duteDateOrCompliance',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'complianceDueDate',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'complianceTimeSpan',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'complianceEndDate',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'complianceFor',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'dueDate',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'dueDateTimeSpan',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'automaticRetake',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'additionalReminder',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'courseInactivationDate',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'removeAccessDate ',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'removeAccessTime',
                type: 'date',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'certificateTemplate',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'enableDescussionForum',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'wantToSellCourse',
                type: 'number',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Coursesettings.delete = function (ctx, id, callback) {
        var ds = Coursesettings.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        var sql = `UPDATE tbl_course_settings SET modifiedBy=${userId}, isDeleted=1, deletedDateTime=now() WHERE courseSettingsId=${id} AND tenantKey='${tenantKey}'`;
        ds.connector.query(sql, function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                responseMsg = commonHelper.commonMsg('success', '200', 'Course settings added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Coursesettings.remoteMethod('delete', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'delete course setting',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'id',
                type: 'number',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Coursesettings.findCourseSettingsById = function (ctx, courseId, callback) {
        var tenantKey = ctx.tenantKey;
        Coursesettings.find({ courseid: courseId, tenantKey: tenantKey }, function (err, CoursesettingsData) {
            console.log(CoursesettingsData);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                var result = [];
                Object.keys(CoursesettingsData).forEach((key) => {
                    var rowData = CoursesettingsData[key];
                    var rawResult = {};
                    rawResult['courseSettingsId'] = rowData.coursesettingsid;
                    rawResult['courseId'] = rowData.courseid;
                    rawResult['contentLiberary'] = rowData.contentliberary;
                    rawResult['courseCategoryceCode'] = rowData.coursecategory;
                    rawResult['moduleOrder'] = rowData.moduleorder;
                    rawResult['newWindow'] = rowData.newwindow;
                    rawResult['duteDateOrCompliance'] = rowData.dutedateorcompliance;
                    rawResult['complianceDueDate'] = rowData.complianceduedate;
                    rawResult['complianceTimeSpan'] = rowData.compliancetimespan;
                    rawResult['complianceEndDate'] = rowData.complianceenddate;
                    rawResult['complianceFor'] = rowData.compliancefor;
                    rawResult['dueDate'] = rowData.duedate;
                    rawResult['dueDateTimeSpan'] = rowData.duedatetimespan;
                    rawResult['automaticRetake'] = rowData.automaticretake;
                    rawResult['additionalReminder'] = rowData.additionalreminder;
                    rawResult['courseInactivationDate'] = rowData.courseinactivationdate;
                    rawResult['removeAccessDate'] = rowData.removeaccessdate;
                    rawResult['removeAccessTime'] = rowData.removeaccesstime;
                    rawResult['certificateTemplate'] = rowData.certificatetemplate;
                    rawResult['enableDescussionForum'] = rowData.enabledescussionforum;
                    rawResult['wantToSellCourse'] = rowData.wanttosellcourse;
                    rawResult['modifiedDate'] = rowData.modifieddate;
                    result.push(rawResult);
                });



                responseMsg = commonHelper.commonMsg('success', '200', 'Coursesettings data', result, '');
                callback(responseMsg);
            }
        });
    };
    Coursesettings.remoteMethod('findCourseSettingsById', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of Coursesettingss',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'id',
                type: 'number',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });


    Coursesettings.list = function (ctx, filter, callback) {

        var tenantKey = ctx.tenantKey;
        Coursesettings.find({
            where: {
                and: [
                    { isdeleted: '0', tenantKey: tenantKey },
                    { or: [{ name: { like: '%' + filter.search + '%' } }, { description: { like: '%' + filter.search + '%' } }] }

                ]

            },
            limit: filter.limit, skip: filter.start, order: filter.order
        }, function (err, CoursesettingsList) {
            if (err) {
                console.error(err);
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something went wrong!', null, err);
                callback(responseMsg);
            } else {
                var result = [];
                Object.keys(CoursesettingsList).forEach((key) => {
                    var rowData = CoursesettingsList[key];
                    var temp = []
                    // var rawResult = {};
                    var rawResult = {};
                    rawResult['courseSettingsId'] = rowData.coursesettingsid;
                    rawResult['courseId'] = rowData.courseid;
                    rawResult['contentLiberary'] = rowData.contentliberary;
                    rawResult['courseCategoryceCode'] = rowData.coursecategory;
                    rawResult['moduleOrder'] = rowData.moduleorder;
                    rawResult['newWindow'] = rowData.newwindow;
                    rawResult['duteDateOrCompliance'] = rowData.dutedateorcompliance;
                    rawResult['complianceDueDate'] = rowData.complianceduedate;
                    rawResult['complianceTimeSpan'] = rowData.compliancetimespan;
                    rawResult['complianceEndDate'] = rowData.complianceenddate;
                    rawResult['complianceFor'] = rowData.compliancefor;
                    rawResult['dueDate'] = rowData.duedate;
                    rawResult['dueDateTimeSpan'] = rowData.duedatetimespan;
                    rawResult['automaticRetake'] = rowData.automaticretake;
                    rawResult['additionalReminder'] = rowData.additionalreminder;
                    rawResult['courseInactivationDate'] = rowData.courseinactivationdate;
                    rawResult['removeAccessDate'] = rowData.removeaccessdate;
                    rawResult['removeAccessTime'] = rowData.removeaccesstime;
                    rawResult['certificateTemplate'] = rowData.certificatetemplate;
                    rawResult['enableDescussionForum'] = rowData.enabledescussionforum;
                    rawResult['wantToSellCourse'] = rowData.wanttosellcourse;
                    rawResult['modifiedDate'] = rowData.modifieddate;

                    result.push(rawResult);

                });
                responseMsg = commonHelper.commonMsg('success', '200', 'Coursesettings list', result, '');
                callback(responseMsg);
            }
        });
    };
    Coursesettings.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of Coursesettingss',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },

            {
                arg: 'filter',
                type: 'object',
                'http': {
                    source: 'query'
                },
                description: 'Filter defining order, start, limit and search - must be a JSON - encoded string Example format :  {"limit":10,"start":0,"order":"date DESC","search":"xyz"}',
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });



    Coursesettings.CoursesettingsUpdate = function (ctx, data, callback) {

        var ds = Coursesettings.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        var str_CoursesettingsImage = (data.CoursesettingsImage != undefined && data.CoursesettingsImage != '') ? `, CoursesettingsImage='${data.CoursesettingsImage}'` : '';
        var str_activeStatus = (data.activeStatus == undefined) ? `, activeStatus='inactive'` : ` ,activeStatus='${data.activeStatus}'`;
        // console.log(data);
        data.contentLiberary = (data.contentLiberary != null) ? data.contentLiberary : 0;
        data.moduleOrder = (data.moduleOrder != null) ? data.moduleOrder : 0;
        data.newWindow = (data.newWindow != null) ? data.newWindow : 0;
        data.enableDescussionForum = (data.enableDescussionForum != null) ? data.enableDescussionForum : 0;
        data.wantToSellCourse = (data.wantToSellCourse != null) ? data.wantToSellCourse : 0;

        // console.log('------------' + moment(data.complianceDueDate).format());

        data.complianceDueDate = (data.complianceDueDate == "Invalid date") ? null : moment(data.complianceDueDate).format('YYYY-MM-DD');
        data.complianceEndDate = (data.complianceEndDate != 'Invalid date') ? moment(data.complianceEndDate).format('YYYY-MM-DD') : null;
        data.dueDate = (data.dueDate != 'Invalid date') ? moment(data.dueDate).format('YYYY-MM-DD') : null;
        data.courseInactivationDate = (data.courseInactivationDate != 'Invalid date') ? moment(data.courseInactivationDate).format('YYYY-MM-DD') : null;
        data.removeAccessDate = (data.removeAccessDate != 'Invalid date') ? moment(data.removeAccessDate).format('YYYY-MM-DD') : null;

        var sql = `UPDATE tbl_course_settings SET modifiedBy=?,contentLiberary=?, courseCategory=?, moduleOrder=?, newWindow=?, duteDateOrCompliance=?,complianceDueDate=?,complianceTimeSpan=?,complianceEndDate=?,complianceFor=?,dueDate=?,dueDateTimeSpan=?,automaticRetake=?,additionalReminder=?,courseInactivationDate=?,removeAccessDate=?,removeAccessTime=?,certificateTemplate=?,enableDescussionForum=?,wantToSellCourse=? WHERE courseSettingsId=? AND tenantKey='${tenantKey}' AND courseId=?`;
        ds.connector.query(sql, [userId, data.contentLiberary, data.courseCategory, data.moduleOrder, data.newWindow, data.duteDateOrCompliance, data.complianceDueDate, data.complianceTimeSpan + ' ' + data.complianceTimeSpanType, data.complianceEndDate, data.complianceFor, data.dueDate, data.dueDateTimeSpan + ' ' + data.dueDateTimeSpanType, data.automaticRetake, data.additionalReminder, data.courseInactivationDate, data.removeAccessDate, data.removeAccessDate, data.certificateTemplate, data.enableDescussionForum, data.wantToSellCourse, data.courseSettingsId, data.courseId], function (err, result) {
            // console.log(err);
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something went wrong!', null, err);
                callback(responseMsg);
            } else {
                responseMsg = commonHelper.commonMsg('success', '200', 'Updated course settings list successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Coursesettings.remoteMethod('CoursesettingsUpdate', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Coursesettings update data',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },
            {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });
};
