'use strict';
var crypto = require('crypto');
var app = require('../../server/server');
var commonHelper = require('../../helper/commonhelper');
var jwt = require('jsonwebtoken');
var constants = require('../../helper/constant');
var sendMail = require('../../helper/sendMail');
var config = require('./../../server/config');
var dateTime = require('node-datetime');
var responseMsg;

module.exports = function (Team) {
  Team.disableRemoteMethodByName('create');
  Team.disableRemoteMethodByName('replaceOrCreate');
  Team.disableRemoteMethodByName('upsertWithWhere');
  Team.disableRemoteMethodByName('change_stream');
  Team.disableRemoteMethodByName('upsert');
  Team.disableRemoteMethodByName('updateAll');
  Team.disableRemoteMethodByName('prototype.updateAttributes');
  Team.disableRemoteMethodByName('find');
  Team.disableRemoteMethodByName('findById');
  Team.disableRemoteMethodByName('findOne');
  Team.disableRemoteMethodByName('deleteById');
  Team.disableRemoteMethodByName('confirm');
  Team.disableRemoteMethodByName('count');
  Team.disableRemoteMethodByName('exists');
  Team.disableRemoteMethodByName('resetPassword');
  Team.disableRemoteMethodByName('prototype.__count__accessTokens');
  Team.disableRemoteMethodByName('prototype.__create__accessTokens');
  Team.disableRemoteMethodByName('prototype.__delete__accessTokens');
  Team.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  Team.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Team.disableRemoteMethodByName('prototype.__get__accessTokens');
  Team.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Team.disableRemoteMethodByName('replaceById');
  Team.disableRemoteMethodByName('createChangeStream');
  Team.disableRemoteMethodByName('change-password');
  Team.disableRemoteMethodByName('reset-password');
  Team.disableRemoteMethodByName('verify');
  
  Team.remoteMethod('list', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get list of Team',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'filter',
        type: 'object',
        'http': {
          source: 'query'
        },
        description: 'Filter defining order, start, limit and search - must be a JSON - encoded string Example format :  {"limit":10,"start":0,"order":"date DESC","search":"xyz"}',
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });
  
  Team.list = function (ctx, filter, callback) {
    
    var loginUser = ctx.req.userDetail.userId;
    var userRole = ctx.req.userDetail.userType;
    var tenantKey = ctx.req.userDetail.tenantKey;
    var responseMsg;
    var ds = Team.dataSource;
    
    let limit = filter.pageStart +","+filter.pageLimit;
    let order, where; 
    if(filter.order == "teamId"){
      order = "tbl_team.teamId";
    }else if(filter.order == "teamName"){
      order = "tbl_team.teamId";
    }else if(filter.order == "createdBy"){
      order = "tbl_team.teamId";
    }else if(filter.order == "status"){
      order = "tbl_team.teamId";
    }else if(filter.order == "createdDate"){
      order = "tbl_team.teamId";
    }else{
      order = "";
    }
    let direction = filter.direction
    let search = '%' + filter.searchKey + '%';
    if(userRole === 1){
      if(filter.searchKey){
        where = '(tbl_team.isDeleted = ? AND tbl_team.tenantKey = ?) AND tbl_team.teamName LIKE ? OR tbl_team.createdDate LIKE ? OR tbl_team.status LIKE ? OR tbl_users.fullName LIKE ? ';
        var params = [0, tenantKey, search, search, search, search];
      }else{
        where = 'tbl_team.isDeleted = ? AND tbl_team.tenantKey = ?';
        var params = [0, tenantKey];
      }
    }else{
      if(filter.searchKey){
        where = '(tbl_team.createdBy = ? AND tbl_team.isDeleted = ? AND tbl_team.tenantKey = ?) AND tbl_team.teamName LIKE ? OR tbl_team.createdDate LIKE ? OR tbl_team.status LIKE ? OR tbl_users.fullName LIKE ? ';
        var params = [loginUser, 0, tenantKey, search, search, search, search];
      }else{
        where = 'tbl_team.createdBy = ? AND tbl_team.isDeleted = ? AND tbl_team.tenantKey = ?';
        var params = [loginUser, 0, tenantKey];
      }
    }

    var userQry = 'SELECT COUNT(*) OVER() AS total_count, tbl_team.teamId, tbl_team.teamName, tbl_team.createdDate, tbl_team.status, tbl_users.fullName AS createdBy FROM tbl_team LEFT JOIN `tbl_users` ON `tbl_users`.`userId` = `tbl_team`.`createdBy` WHERE tbl_team.isDeleted = ? AND tbl_team.tenantKey = ? ORDER BY tbl_team.teamId DESC LIMIT 3';
    let fields = 'COUNT(*) OVER() AS total_count, tbl_team.teamId, tbl_team.teamName, tbl_team.createdDate, tbl_team.status, tbl_users.fullName AS createdBy';
    let from = 'tbl_team LEFT JOIN `tbl_users` ON `tbl_users`.`userId` = `tbl_team`.`createdBy`';
    let fliter = 'ORDER BY '+order+' '+ direction +' LIMIT '+limit;
    
    let sqlQry = 'SELECT ' + fields + ' FROM ' + from + ' WHERE ' + where + ' ' + fliter;
    ds.connector.query(sqlQry, params, function (err, teamList) {
      if(err){
        responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', null, err);
        callback(null, responseMsg);
      }else{
        if(teamList.length > 0){
          teamList = JSON.parse(JSON.stringify(teamList)); 
          var totalCount = teamList[0].total_count;
          responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', {
            list: teamList,
            totalCount: totalCount
          }, '');
        }else{
          responseMsg = commonHelper.commonMsg('failure', 404, 'List Not found', {
            list: teamList,
            totalCount: totalCount
          }, '');
        }
        callback(null, responseMsg);
      }
    });
  }
  
  Team.remoteMethod('add', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Create a new instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'teamName',
        type: 'string',
        description: 'Please provide name of team',
        required: true
      },
      {
        arg: 'status',
        type: 'string',
        description: 'Please select status of team',
        required: true
      },
      {
        arg: 'users',
        type: 'string',
        description: 'Please select users of team',
        required: true
      },
      {
        arg: 'teamDescription',
        type: 'string',
        description: 'More info regarding Description',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });
  
  Team.add = function (ctx, teamName, status, users, teamDescription, callback) {
    
    var loginUser = ctx.userId; 
    var tenantKey=ctx.tenantKey;
    var teamStatus = status ? status : "inactive";
    var jsonObj = [];
    var userIds = '';
    
    if(users){
      
      users = users.toString();
      users = users.split(',');
      
      for (var l = 0; l < users.length; l++) {
        
        jsonObj.push({ "id": users[l] });
        
        userIds = JSON.stringify(jsonObj);
      }
      
      userIds = userIds.replace(/:\"/g, "\:");
      userIds = userIds.replace(/\"}/g, "}");
      
    }
    
    Team.create({
      "teamName": teamName,
      "teamDescription": teamDescription,
      "createdBy": loginUser,
      "status": teamStatus,
      "userIds": userIds,
      "isDeleted": 0,
      "tenantKey": tenantKey
    },
    function (err1, teamRes) {
      
      if (err1) {
        var responseMsg = commonHelper.commonMsg('failure', 400, 'team create failed', '', err1);
        callback(err1, responseMsg);
      }else{
        var activities = {
          "activity": 'New Team created',
          "module": 'team',
          "moduleId": teamRes.teamId,
          "tenantKey": tenantKey,
          "user": loginUser,
          "value1": teamName,
          "value2": null,
          "icon": 'fa-user',
          "note": null,
          "status": null,
          'createdBy': loginUser,
        };
        commonHelper.activity(activities, function (err, rows) {
          
          if (err) {
            responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
            callback(err, responseMsg)
          } else {
            responseMsg = commonHelper.commonMsg('success', 201, 'team added successfully', teamRes);
            callback(null, responseMsg);
          }
        });
      }
    });
    
  }
  
  Team.remoteMethod(
    'delete', {
      http: {
        status: 200,
        errorStatus: 400,
        path: '/delete/:id',
        verb: 'delete'
      },
      description: 'Delete Team',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'id',
          type: 'string',
          description: 'Enter Team id\'s that need to be deleted (use comma separated values for multiple Team)',
          required: true
        }
      ],
      returns: [
        
        {
          arg: "result",
          http: {
            source: 'result'
          }
        }
      ]
    }
    );
    
    Team.delete = function (ctx, teamId, callback) {
      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      var responseMsg = '';
      var dt = dateTime.create();
      var formattedDateTime = dt.format('Y-m-d H:M:S');
      Team.update({
        'teamId': teamId,
        'tenantKey': tenantKey
      }, {
        'isDeleted': 1,
        'deletedDateTime': formattedDateTime,
        'activated': 0
      },
      function (err, result) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'team delete failed ', '', err);
          callback(null, responseMsg);
        } else {
          if (result.count === 1) {
            
            var activities = {
              'user': loginUser,
              'module': 'Team',
              'moduleId': teamId,
              'activity': 'activity_delete_team',
              'value1': teamId,
              "value2": null,
              "icon": 'fa-circle-o',
              "note": null,
              "status": null,
              "tenantKey": tenantKey,
              'createdBy': loginUser,
            };
            //save Acitivty
            commonHelper.activity(activities, function (err1, rows) {
              if (err1) {
                responseMsg = commonHelper.commonMsg('failure', 500, ' team delete failed ', '', err1);
              } else {
                responseMsg = commonHelper.commonMsg('success', 200, ' Team deleted successfully ', result);
              }
              callback(null, responseMsg);
            });
          } else {
            responseMsg = commonHelper.commonMsg('failure', 404, 'Team not found ');
            callback(null, responseMsg);
          }
        }
      });
    }
    
    Team.remoteMethod('status',
    {
      http: {
        status: 201,
        errorStatus: 400,
        path: '/status/:status/:id',
        verb: 'post'
      },
      description: 'Change Team status (Active/Deactive)',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'status',
          type: 'number',
          description: 'Enter status to be changed (1:Activate 0:Deactivate)',
          required: true
        },
        {
          arg: 'id',
          type: 'number',
          description: 'Enter Team id whos status should be changed',
          required: true
        }
      ],
      returns: [{
        arg: 'result'
      }]
    });
    
    Team.status = function (ctx, status, teamId, callback) {
      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      Team.update({
        'teamId': teamId,
        'tenantKey': tenantKey
      }, {
        'status': status,
      },
      (err, res) => {
        if (err) {
          var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
          callback(null, responseMsg);
        } else if (res.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'team',
            'moduleField': teamId,
            'activity': 'activity_change_status',
            'icon': 'fa-user',
            'value1': status,
            "value2": null,
            "icon": 'fa-user',
            "note": null,
            "status": null,
            "tenantKey": tenantKey,
            'createdBy': loginUser,
          };
          
          commonHelper.activity(activities, function (err, rows) {
            if (err) {
              var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
              callback(null, responseMsg);
            } else {
              if (status) {
                var msg = "activated";
              } else {
                var msg = "deactivated";
              }
              var responseMsg = commonHelper.commonMsg('success', 200, 'team ' + msg + ' successfully!', '', err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 404, 'Team not found!', '', err);
          callback(null, responseMsg);
        }
      }
      );
    }
    
    Team.remoteMethod('detailsById', {
      http: {
        status: 200,
        errorStatus: 400,
        path: '/detailsById/:id',
        verb: 'get'
      },
      description: 'Get Team details',
      accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'id',
        type: 'string',
        description: 'Enter team id to fetch details',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });
  
  Team.detailsById = function (ctx, teamId, callback) {
    var loginUser = ctx.userId; 
    var tenantKey=ctx.tenantKey;
    var responseMsg;
    Team.findOne({
      fields: { tenantKey: false, isdeleted: false, deletedatetime: false },
      where: {
        teamId: teamId,
        isdeleted: 0,
        tenantKey: tenantKey,
      },
    }, function (err, response) {
      if (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'error', '', err);
      } else {
        if (response) {
          responseMsg = commonHelper.commonMsg('success', 200, 'team details fetched successfully', response);
        } else {
          responseMsg = commonHelper.commonMsg('success', 404, 'No Team found', response);
        }
      }
      callback(null, responseMsg);
    });
  };
  
  Team.remoteMethod('modify', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/modify/:id',
      verb: 'post'
    },
    description: 'Update a instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'id',
        type: 'number',
        description: 'Please provide user ID of user',
        required: true
      },
      {
        arg: 'teamName',
        type: 'string',
        description: 'Please provide name of team',
        required: true
      },
      {
        arg: 'status',
        type: 'string',
        description: 'Please select status of team',
        required: true
      },
      {
        arg: 'users',
        type: 'string',
        description: 'Please select users of team',
        required: true
      },
      {
        arg: 'teamDescription',
        type: 'string',
        description: 'More info regarding Description',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });
  
  Team.modify = function ( ctx, teamId, teamName, status, users, teamDescription, callback) {
    
    var loginUser = ctx.userId;
    //var loginUser = 1;
    var tenantKey=ctx.tenantKey;
    //var tenantKey='ac7153df458aefc2a93fd1a4f7513147';
    var jsonObj = [];
    var userIds = '';
    
    if(users){
      
      users = users.toString();
      users = users.split(',');
      
      for (var l = 0; l < users.length; l++) {
        
        jsonObj.push({ "id": users[l] });
        
        userIds = JSON.stringify(jsonObj);
      }
      
      userIds = userIds.replace(/:\"/g, "\:");
      userIds = userIds.replace(/\"}/g, "}");
      
    }
    var teamStatus = status ? status : "inactive";
    var saveData = {
      'teamName': teamName,
      'status': teamStatus,
      'userIds': userIds,
      'teamDescription': teamDescription
    };
    Team.update({
      'teamId': teamId,
      'tenantKey': tenantKey
    }, saveData, (err1, updateRes)=> {
      
      if (err1) {
        var responseMsg = commonHelper.commonMsg('failure', 400, 'Team update failed', '', err1);
        callback(err1, responseMsg);
      }else{
        var activities = {
          "activity": ' Team updated',
          "module": 'team',
          "moduleId": updateRes.teamId,
          "tenantKey": tenantKey,
          "user": loginUser,
          "value1": null,
          "value2": null,
          "icon": 'fa-user',
          "note": null,
          "status": null,
          'createdBy': loginUser,
        };
        commonHelper.activity(activities, function (err, rows) {
          
          if (err) {
            
            responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
            callback(err, responseMsg)
          } else {
            
            responseMsg = commonHelper.commonMsg('success', 201, 'Team updated successfully', updateRes);
            callback(null, responseMsg);
          }
        });
      }
    });
  }
};
