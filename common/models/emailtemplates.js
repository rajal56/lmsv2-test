/* eslint-disable max-len */
'use strict';
var commonHelper = require('../../helper/commonhelper');
var responseMsg;
var app = require('../../server/server');
var ds = app.datasources.mysqldb;
var emailGroupName = 'proposal_template';
var moment = require('moment');
var constants = require('../../helper/constant');

module.exports = function(EmailTemplates) {
  EmailTemplates.disableRemoteMethodByName('replaceOrCreate');
  EmailTemplates.disableRemoteMethodByName('upsertWithWhere');
  EmailTemplates.disableRemoteMethodByName('change_stream');
  EmailTemplates.disableRemoteMethodByName('upsert');
  EmailTemplates.disableRemoteMethodByName('updateAll');
  EmailTemplates.disableRemoteMethodByName('prototype.updateAttributes');
  EmailTemplates.disableRemoteMethodByName('find');
  EmailTemplates.disableRemoteMethodByName('findById');
  EmailTemplates.disableRemoteMethodByName('findOne');
  EmailTemplates.disableRemoteMethodByName('createChangeStream');
  EmailTemplates.disableRemoteMethodByName('replaceById');
  EmailTemplates.disableRemoteMethodByName('deleteById');
  EmailTemplates.disableRemoteMethodByName('confirm');
  EmailTemplates.disableRemoteMethodByName('count');
  EmailTemplates.disableRemoteMethodByName('exists');
  EmailTemplates.disableRemoteMethodByName('resetPassword');
  EmailTemplates.disableRemoteMethodByName('prototype.__count__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__create__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__delete__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__findById__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__get__accessTokens');
  EmailTemplates.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  EmailTemplates.disableRemoteMethodByName('create');

  EmailTemplates.remoteMethod('listByEGroup', {
    http: {status: 201, errorStatus: 400, verb: 'get'},
    description: 'Get list by email group',
    accepts: [
      {arg: 'ctx', type: 'object', http: {source: 'context'}},
      {arg: 'eGroup', type: 'string', required: false, description: 'Enter email group like invoice_message'},
    ],
    returns: {arg: 'data', type: 'string', root: true},
  });

  EmailTemplates.listByEGroup = function(ctx, eGroup, callback) {

    EmailTemplates.find({
      fields: {tenantKey: false, isdeleted: false, deletedatetime: false},
      where: {
        emailGroup: eGroup,
        isdeleted: 0,
        tenantKey: ctx.req.userDetail.tenantKey,
      },
    }, function(err, eGroupInfo) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
        responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
        callback(null, responseMsg);
      } else {
        if (eGroupInfo.length > 0) {
          logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - fetched successfully ');
          responseMsg = commonHelper.commonMsg('success', 'Template fetched successfully', eGroupInfo);
          callback(null, responseMsg);
        } else {
          logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - no template found ');
          responseMsg = commonHelper.commonMsg('success', 'No template found', eGroupInfo);
          callback(null, responseMsg);
        }
      }

    });
  };
  EmailTemplates.remoteMethod(
    'delete', {
      http: {status: 200, errorStatus: 400, verb: 'delete',
        path: '/delete/:id'},
      description: 'Delete Email Template.',
      accepts: [
        {arg: 'ctx', type: 'object', http: {source: 'context'}},
        {arg: 'id', type: 'Number', required: true},],
      returns: {arg: 'data', type: 'string', root: true},
    });

  EmailTemplates.delete = function(ctx, id, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;
    var loginUser = ctx.req.userDetail.userId;
    EmailTemplates.findOne({
      where: {
        emailTemplatesId: id,
        isDeleted: 0,
        tenantKey: companyKey,
      },
    }, function(err, checkDefault) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
        responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, err);
        callback(null, responseMsg);
      } else {
        // if (checkDefault.defaultAgreementTemplate === '1') {
        //  //need to ask kartik
        // }
        EmailTemplates.update({
          emailTemplatesId: id,
          isDeleted: 0,
          tenantKey: companyKey,
        }, {
          isdeleted: 1,
          deletedatetime: Date.now(),
        }, function(err, dltEmailTemplate) {
          if (err) {
            logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
            responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, err);
            callback(null, responseMsg);
          } else if (dltEmailTemplate.count > 0) {
            var activities = {
              'user': loginUser,
              'module': 'agreements_template',
              'moduleField': id,
              'activity': 'Delete Agreement Template',
              'value1': id,
              'value2': null,
              'icon': 'fa-circle-o',
              'note': null,
              'status': null,
              'tenantKey': companyKey,
              'createdBy' : loginUser
            };
            commonHelper.activity(activities, function(err, rows) {
              if (err) {
                logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
                responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, '', err);
                callback(null, responseMsg);
              } else {
                logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - deleted successfully ');
                responseMsg = commonHelper.commonMsg('success', 'Email template deleted successfully');
                callback(null, responseMsg);
              }
            });
          } else {
            logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - deletion failed ');
            responseMsg = commonHelper.commonMsg('failure', 'No email template deleted');
            callback(null, responseMsg);
          }
        });
      }
    });
  };
  
  EmailTemplates.remoteMethod(
    'add', {
      http: {status: 201, errorStatus: 400, verb: 'post'},
      description: 'Add Email Template.',
      accepts: [
        {arg: 'ctx', type: 'object', http: {source: 'context'}},
        {arg: 'subject', type: 'string', required: true},
        {arg: 'templateBody', type: 'string', required: true},
        {arg: 'emailGroup', type: 'string', required: true},
        {arg: 'customAgreementTemplateType', type: 'string', required: true}],
      returns: {arg: 'data', type: 'string', root: true},
    });

  EmailTemplates.add = function(ctx, subject, templateBody, emailGroup, customAgreementTemplateType, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;
    var loginUser = ctx.req.userDetail.userId;
    var data = {
      subject: subject,
      templateBody: templateBody,
      emailGroup: emailGroup,
      customAgreementTemplateType: customAgreementTemplateType,
      tenantKey: companyKey,
    };
    EmailTemplates.create(data, function(err, creaEmailTemp) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD HH:mm:ss') + ' Email templates - Failed to create ', err);
        responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
        callback(null, responseMsg);
      } else {
        if (creaEmailTemp.emailTemplatesId) {
          var activities = {
            'user': loginUser,
            'module': 'agreements_template',
            'moduleId': creaEmailTemp.emailTemplatesId,
            'activity': 'Agreement Template Created',
            'value1': data['emailGroup'],
            'value2': data['subject'],
            'icon': 'fa-circle-o',
            'note': null,
            'status': null,
            'tenantKey': companyKey,
            'createdBy': loginUser,
          };
          commonHelper.activity(activities, function(err, rows) {
            if (err) {
              logger.error(moment().format('YYYY-MM-DD HH:mm:ss') + ' Email templates - Failed to add activity ', err);
              responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
              callback(null, responseMsg);
            } else {
              logger.info(moment().format('YYYY-MM-DD HH:mm:ss') + ' Email templates - added successfully ');
              responseMsg = commonHelper.commonMsg('success', 'Email template added successfully', {emailTemplatesId : creaEmailTemp.emailTemplatesId});
              callback(null, responseMsg);
            }
          });
        } else {
          logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - not added ');
          responseMsg = commonHelper.commonMsg('failure', 'Email template not added');
          callback(null, responseMsg);
        }
      }
    });
  };

  EmailTemplates.remoteMethod('listByEmailTemplateId', {
    http: {
      status: 200,
      errorStatus: 400,
      path: '/listByEmailTemplateId/:id',
      verb: 'get'
    },
    description: "Display Proposal Template",
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },{
      arg: 'id',
      type: 'number',
      description: 'Enter email template id',
      required: true
    }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  EmailTemplates.listByEmailTemplateId = function(ctx, id, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;
    EmailTemplates.findOne({
      where: {
        emailTemplatesId: id,
        emailGroup: emailGroupName,
        tenantKey: companyKey,
      },
    }, function(err, response) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
        responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, '', err);
        callback(null, responseMsg);
      } else {
        logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - fetched successfully ');
        responseMsg = commonHelper.commonMsg('success', 'Template fetched successfully', response, '');
        callback(null, responseMsg);
      }
    });
  };

  EmailTemplates.remoteMethod(
    'modify', {
      http: {status: 201, errorStatus: 400, verb: 'post'},
      description: 'Update Email Template.',
      accepts: [
        {arg: 'ctx', type: 'object', http: {source: 'context'}},
        {arg: 'id', type: 'number', required: true},
        {arg: 'subject', type: 'string', required: true},
        {arg: 'templateBody', type: 'string', required: true},
        {arg: 'emailGroup', type: 'string', required: true},
        {arg: 'customAgreementTemplateType', type: 'string', required: true}],
      returns: {arg: 'data', type: 'string', root: true},
    });
  
  EmailTemplates.modify = function(ctx, emailTemplatesId, subject, templateBody, emailGroup, customAgreementTemplateType, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;
    var loginUser = ctx.req.userDetail.userId;
    var data = {
      subject: subject,
      templateBody: templateBody,
      emailGroup: emailGroup,
      customAgreementTemplateType: customAgreementTemplateType,
      tenantKey: companyKey,
    };
    EmailTemplates.update({
      emailTemplatesId: emailTemplatesId,
      isDeleted: 0,
      tenantKey: companyKey,
    }, data, function(err, updEmailTemplate) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
        responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, '', err);
        callback(null, responseMsg);
      } else {
        if (updEmailTemplate.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'agreements_template',
            'moduleField': emailTemplatesId,
            'activity': 'Agreement Template Updated',
            'value1': data['emailGroup'],
            'value2': data['subject'],
            'icon': 'fa-circle-o',
            'note': null,
            'status': null,
            'tenantKey': companyKey,
            'createdBy': loginUser,
          };
          commonHelper.activity(activities, function(err, rows) {
            if (err) {
              logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - ' + constants.sqlErrMsg, err);
              responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, '', err);
              callback(null, responseMsg);
            } else {
              logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - updated successfully ');
              responseMsg = commonHelper.commonMsg('success', 'Email template updated successfully');
              callback(null, responseMsg);
            }
          });
        } else {
          logger.info(moment().format('YYYY-MM-DD hh:mm:ss') + ' Email templates - not updated ');
          responseMsg = commonHelper.commonMsg('failure', 'Email template not updated');
          callback(null, responseMsg);
        }
      }
    });
  };
 
};
