'use strict';

module.exports = function (Menu) {
  Menu.disableRemoteMethodByName('create');
  Menu.disableRemoteMethodByName('replaceOrCreate');
  Menu.disableRemoteMethodByName('upsertWithWhere');
  Menu.disableRemoteMethodByName('change_stream');
  Menu.disableRemoteMethodByName('upsert');
  Menu.disableRemoteMethodByName('updateAll');
  Menu.disableRemoteMethodByName('prototype.updateAttributes');
  Menu.disableRemoteMethodByName('find');
  Menu.disableRemoteMethodByName('findById');
  Menu.disableRemoteMethodByName('findOne');
  Menu.disableRemoteMethodByName('deleteById');
  Menu.disableRemoteMethodByName('confirm');
  Menu.disableRemoteMethodByName('count');
  Menu.disableRemoteMethodByName('exists');
  Menu.disableRemoteMethodByName('resetPassword');
  Menu.disableRemoteMethodByName('prototype.__count__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__create__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__delete__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__get__accessTokens');
  Menu.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Menu.disableRemoteMethodByName('replaceById');
  Menu.disableRemoteMethodByName('createChangeStream');
  Menu.disableRemoteMethodByName('change-password');
  Menu.disableRemoteMethodByName('reset-password');
  Menu.disableRemoteMethodByName('verify');

  Menu.remoteMethod(
    'list',
    {
      http: { verb: 'get' },
      description: 'Get All Menu user has access to',
      accepts: [
      ],
      returns: { arg: 'data', type: [], root: true },
    }
  );

    Menu.list = function(callback) {
      Menu.find(
        {
          where: {
          },
        }, function (err, res) {
          if (err) {
            responseMsg = commonHelper.commonMsg('failure', 'Something went wrong');
            callback(null, responseMsg);
          } else {
            responseMsg = commonHelper.commonMsg('success', 'Menu list fetched successfully', {list: res});
            callback(null, responseMsg);
          }
        }
      )
    }

};
