'use strict';
var commonHelper = require('../../helper/commonhelper');

var responseMsg;
module.exports = function (Modules) {
    Modules.disableRemoteMethodByName('create');
    Modules.disableRemoteMethodByName('replaceOrCreate');
    Modules.disableRemoteMethodByName('upsertWithWhere');
    Modules.disableRemoteMethodByName('change_stream');
    Modules.disableRemoteMethodByName('upsert');
    Modules.disableRemoteMethodByName('updateAll');
    Modules.disableRemoteMethodByName('prototype.updateAttributes');
    Modules.disableRemoteMethodByName('find');
    Modules.disableRemoteMethodByName('findById');
    Modules.disableRemoteMethodByName('findOne');
    Modules.disableRemoteMethodByName('deleteById');
    Modules.disableRemoteMethodByName('confirm');
    Modules.disableRemoteMethodByName('count');
    Modules.disableRemoteMethodByName('exists');
    Modules.disableRemoteMethodByName('verify');
    Modules.disableRemoteMethodByName('replaceById');
    Modules.disableRemoteMethodByName('createChangeStream');
    Modules.disableRemoteMethodByName('create');

    var responseMsg = '';

    Modules.courseModuleList = function (ctx, filter, courseId, callback) {
        var ds = Modules.dataSource;
        var tenantKey = ctx.tenantKey;
        var orderBy = (filter.order != undefined) ? 'order by ' + filter.order : '';
        var likeCondition = (filter.search != undefined) ? 'AND (moduleType like "%' + filter.search + '%" OR moduleFile like "%' + filter.search + '%")' : '';
        var limit = (filter.limit != undefined && filter.skip != undefined) ? 'limit' + filter.skip + ',' + filter.limit : '';

        var sql = `SELECT tmm.*,tcm.courseId,tcm.id as courseModuleId FROM tbl_module tmm INNER JOIN tbl_course_module tcm ON(tcm.moduleId = tmm.moduleId AND tcm.isDeleted =0 AND tcm.courseId=${courseId}) WHERE tmm.isDeleted=0 ${likeCondition} ${limit}  ${orderBy}`;
        ds.connector.query(sql, function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'course module data', result, '');
                callback(responseMsg);
            }
        });
    };

    Modules.remoteMethod('courseModuleList', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Add module',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },
            {
                arg: 'courseId',
                type: 'number',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Modules.list = function (ctx, filter, callback) {
        // console.log("mfilter -------- options");
        // console.log(filter);
        var tenantKey = ctx.tenantKey;
        Modules.find((filter != "") ? ({
            where: {
                and: [
                    { isdeleted: '0', tenantKey: tenantKey },
                    { or: [{ moduletype: { like: '%' + filter.search + '%' } }, { modulefile: { like: '%' + filter.search + '%' } }] }
                ]

            },
            limit: filter.limit, skip: filter.start, order: filter.order
        }) : ({ where: { isdeleted: '0', tenantKey: tenantKey } }), function (err, moduleList
        ) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                var result = [];
                Object.keys(moduleList).forEach((key) => {
                    var rowData = moduleList[key];
                    var temp = []
                    var rawResult = {};
                    rawResult['moduleId'] = rowData.moduleid;
                    rawResult['moduleType'] = rowData.moduletype;
                    rawResult['moduleFile'] = rowData.modulefile;
                    rawResult['createdDate'] = rowData.createddate;
                    rawResult['modifiedDate'] = rowData.modifieddate;

                    result.push(rawResult);

                });

                responseMsg = commonHelper.commonMsg('success', '200', 'module data', result, '');
                callback(responseMsg);
            }
        });

    };
    Modules.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Add module',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });




    Modules.courseModuleAdd = function (ctx, moduleId, courseId, callback) {
        var ds = Modules.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        var sql = `INSERT INTO tbl_course_module(moduleId,courseId,tenantKey,createdDate,CreatedBy) VALUES('${moduleId}','${courseId}','${tenantKey}',now(),${userId})`;
        ds.connector.query(sql, function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Course module added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Modules.remoteMethod('courseModuleAdd', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Add module for course',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },
            {
                arg: 'moduleId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'courseId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Modules.moduleAdd = function (ctx, moduleType, moduleFile, callback) {
        var ds = Modules.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        var sql = `INSERT INTO tbl_module(moduleType,moduleFile,tenantKey,createdDate,CreatedBy) VALUES('${moduleType}','${moduleFile}','${tenantKey}',now(),${userId})`;
        ds.connector.query(sql, function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {


                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'module added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Modules.remoteMethod('moduleAdd', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Add module',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },
            {
                arg: 'moduleType',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'moduleFile',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

};
