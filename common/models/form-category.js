'use strict';
var commonHelper = require('../../helper/commonhelper');
module.exports = function (Formcategory) {
    Formcategory.disableRemoteMethodByName('create');
    Formcategory.disableRemoteMethodByName('replaceOrCreate');
    Formcategory.disableRemoteMethodByName('upsertWithWhere');
    Formcategory.disableRemoteMethodByName('change_stream');
    Formcategory.disableRemoteMethodByName('upsert');
    Formcategory.disableRemoteMethodByName('updateAll');
    Formcategory.disableRemoteMethodByName('prototype.updateAttributes');
    Formcategory.disableRemoteMethodByName('find');
    Formcategory.disableRemoteMethodByName('findById');
    Formcategory.disableRemoteMethodByName('findOne');
    Formcategory.disableRemoteMethodByName('deleteById');
    Formcategory.disableRemoteMethodByName('confirm');
    Formcategory.disableRemoteMethodByName('count');
    Formcategory.disableRemoteMethodByName('exists');
    Formcategory.disableRemoteMethodByName('verify');
    Formcategory.disableRemoteMethodByName('replaceById');
    Formcategory.disableRemoteMethodByName('createChangeStream');
    Formcategory.disableRemoteMethodByName('create');


    var responseMsg = '';

    Formcategory.add = function (ctx, formCategoryName, formId, suggestions, callback) {
        var ds = Formcategory.dataSource;
        // var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = `INSERT INTO tbl_form_category(formCategoryName, formId, createdDate, tenantKey, suggestions) VALUES(?,?,now(),?,?)`;
        ds.connector.query(sql, [formCategoryName, formId, tenantKey, suggestions], function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                // console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'category added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Formcategory.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formCategoryName',
                type: 'string',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formId',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'suggestions',
                type: 'string',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Formcategory.list = function (ctx, formId, callback) {
        var ds = Formcategory.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = `SELECT tfq.*,tfc.formCategoryName FROM tbl_form_question tfq LEFT JOIN (tbl_form_category tfc) ON (tfc.formId = tfq.formId AND tfc.isDeleted=0) WHERE tfq.formId = ${formId} AND tenantKey='${tenantKey}'`;
        ds.connector.query(sql, function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                // console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'Question list', result, '');
                callback(responseMsg);
            }
        });
    };
    Formcategory.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of questions',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'formId',
                type: 'number',
                http: {
                    source: 'context'
                }
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });
};
