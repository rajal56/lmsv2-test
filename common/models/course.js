'use strict';
var crypto = require('crypto');
var app = require('../../server/server');
var commonHelper = require('../../helper/commonhelper');
var jwt = require('jsonwebtoken');
var constants = require('../../helper/constant');
var sendMail = require('../../helper/sendMail');
var config = require('./../../server/config');
var dateTime = require('node-datetime');
var responseMsg;

// var responseMsg;
module.exports = function (Course) {
    Course.disableRemoteMethodByName('create');
    Course.disableRemoteMethodByName('replaceOrCreate');
    Course.disableRemoteMethodByName('upsertWithWhere');
    Course.disableRemoteMethodByName('change_stream');
    Course.disableRemoteMethodByName('upsert');
    Course.disableRemoteMethodByName('updateAll');
    Course.disableRemoteMethodByName('prototype.updateAttributes');
    Course.disableRemoteMethodByName('find');
    Course.disableRemoteMethodByName('findById');
    Course.disableRemoteMethodByName('findOne');
    Course.disableRemoteMethodByName('deleteById');
    Course.disableRemoteMethodByName('confirm');
    Course.disableRemoteMethodByName('count');
    Course.disableRemoteMethodByName('exists');
    Course.disableRemoteMethodByName('resetPassword');
    Course.disableRemoteMethodByName('prototype.__count__accessTokens');
    Course.disableRemoteMethodByName('prototype.__create__accessTokens');
    Course.disableRemoteMethodByName('prototype.__delete__accessTokens');
    Course.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
    Course.disableRemoteMethodByName('prototype.__findById__accessTokens');
    Course.disableRemoteMethodByName('prototype.__get__accessTokens');
    Course.disableRemoteMethodByName('prototype.__updateById__accessTokens');
    Course.disableRemoteMethodByName('replaceById');
    Course.disableRemoteMethodByName('createChangeStream');
    Course.disableRemoteMethodByName('change-password');
    Course.disableRemoteMethodByName('reset-password');
    Course.disableRemoteMethodByName('verify');
    
    var responseMsg = '';
    
Course.add = function (ctx, name, description, referanceCode, activeStatus,callback) {

        var loginUser = ctx.req.userDetail.userId;
        var tenantKey = ctx.req.userDetail.tenantKey;
       
        // if(activeStatus == 1)
        // {
        //     var courseStatus = 'active';
        // }
        // if(activeStatus == 0)
        // {
        //     var courseStatus = 'inactive';
        // }
       
    var dt = dateTime.create();
    var formattedDateTime = dt.format('Y-m-d H:M:S');
       
    Course.create({
      "name": name,
      "description": description,
      "referancecode":referanceCode,
      "activestatus": activeStatus,
      "createddate": formattedDateTime,
      "courseimage":"aaa.jpg",
      "tenantKey": tenantKey,
      "createdby": loginUser,
      "isdeleted": 0, 
      
    },
    function (err1, courseRes) {
      
      if (err1) {
        var responseMsg = commonHelper.commonMsg('failure', 400, 'course create failed', '', err1);
        callback(err1, responseMsg);
      }else{
        var activities = {
          "activity": 'New course created',
          "module": 'course',
          "moduleId": courseRes.courseid,
          "tenantKey": tenantKey,
          "user": loginUser,
          "value1": name,
          "value2": null,
          "icon": 'fa-book',
          "note": null,
          "status": null,
          'createdBy': loginUser,
        };
        commonHelper.activity(activities, function (err, rows) {
          
          if (err) {
            responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
            callback(err, responseMsg)
          } else {
            responseMsg = commonHelper.commonMsg('success', 201, 'course  added successfully', courseRes);
            callback(null, responseMsg);
          }
        });
      }
    }); 

    };
    Course.remoteMethod('add', {
        http: {
            // status: 201,
            // errorStatus: 400,
            verb: 'post'
        },
        description: 'Create a new instance of the model and persist it into the data source. ',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'name',
                type: 'string',
                description: 'Please provide Course Name',
                required: true
            },
            {
                arg: 'description',
                type: 'string',
                description: 'Please provide Course Description',
                required: true
            },
            {
                arg: 'referanceCode',
                type: 'string',
                description: 'Please provide Referance Code',
                required: true
            },
            {
                arg: 'activeStatus',
                type: 'number',
                description: 'Enter status to be changed (1:Active 0:InActive)',
                required: true
            }
            // ,
            // {
            //     arg: 'courseImage',
            //     type: 'file',
            //     description: 'Upload Course Image',
            //     required: true
                
            // }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });

    Course.remoteMethod(
    'delete', {
      http: {
        status: 200,
        errorStatus: 400,
        path: '/delete/:id',
        verb: 'delete'
      },
      description: 'Delete Course',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'id',
          type: 'string',
          description: 'Enter Course id that need to be deleted',
          required: true
        }
      ],
      returns: [
        
        {
          arg: "result",
          http: {
            source: 'result'
          }
        }
      ]
    }
    );
    
    Course.delete = function (ctx, courseId, callback) {
      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      var responseMsg = '';
      var dt = dateTime.create();
      var formattedDateTime = dt.format('Y-m-d H:M:S');
      Course.update({
        'courseid': courseId,
        'tenantKey': tenantKey
      }, {
        'isdeleted': 1,
        'deleteddatetime': formattedDateTime
      },
      function (err, result) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'Course delete failed ', '', err);
          callback(null, responseMsg);
        } else {
          if (result.count === 1) {
            
            var activities = {
              'user': loginUser,
              'module': 'Course',
              'moduleId': courseId,
              'activity': 'activity_delete_course',
              'value1': courseId,
              "value2": null,
              "icon": 'fa-book',
              "note": null,
              "status": null,
              "tenantKey": tenantKey,
              'createdBy': loginUser,
            };
            //save Acitivty
            commonHelper.activity(activities, function (err1, rows) {
              if (err1) {
                responseMsg = commonHelper.commonMsg('failure', 500, ' course delete failed ', '', err1);
              } else {
                responseMsg = commonHelper.commonMsg('success', 200, ' course deleted successfully ', result);
              }
              callback(null, responseMsg);
            });
          } else {
            responseMsg = commonHelper.commonMsg('failure', 404, 'course not found ');
            callback(null, responseMsg);
          }
        }
      });
    }
  
    
Course.remoteMethod('detailsById', {
    http: {
      status: 200,
      errorStatus: 400,
      path: '/detailsById/:id',
      verb: 'get'
    },
    description: 'Get courses details',
    accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'Id',
        type: 'string',
        description: 'Enter course id to fetch details',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Course.detailsById = function (ctx, Id, callback) {
    var tenantKey = ctx.req.userDetail.tenantKey;
    var responseMsg;
  //console.log(Id); console.log("=======");
 // console.log(tenantKey);
    Course.findOne({
      fields: { tenantKey: false, isdeleted: false, deletedatetime: false },
      where: {
        courseid:Id,
        isdeleted: 0,
        tenantKey: tenantKey,
      },
    }, function (err, response) {
      if (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'error', '', err);
      } else { 
        if (response) {
          responseMsg = commonHelper.commonMsg('success', 200, 'Course details fetched successfully', response);
        } else {
          responseMsg = commonHelper.commonMsg('success', 404, 'No Course found', response);
        }
      }
      callback(null, responseMsg);
    });
  };


    Course.remoteMethod('list', {
        http: {
          status: 200,
          errorStatus: 400,
          verb: 'get'
        },
        description: 'Get list of courses',
        accepts: [
          {
            arg: 'ctx',
            type: 'object',
            http: {
              source: 'context'
            }
          },
          {
            arg: 'filter',
            type: 'object',
            'http': {
              source: 'query'
            },
            description: 'Filter defining order, start, limit and search - must be a JSON - encoded string Example format :  {"limit":10,"start":0,"order":"date DESC","search":"xyz"}',
          }
        ],
        returns: {
          arg: 'data',
          type: [],
          root: true
        }
      });

    Course.list = function (ctx, filter, callback) {

      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      var responseMsg, whereCondition;
      let promise = new Promise(function (resolve, reject) {
        
        if(filter.searchKey){
          whereCondition ={
            and: [{
                or: [{
                  name: {
                    like: '%'+filter.searchKey+'%'
                  }
                }, {
                 description: {
                    like: '%'+filter.searchKey+'%'
                  }
                },{
                  referancecode: {
                    like: '%'+filter.searchKey+'%'
                  }
                }]
              },
              {
                tenantKey: tenantKey, isDeleted: 0
              }
            ],
          }
        }else{
          whereCondition = {tenantKey: tenantKey, isDeleted: 0};
        }
  
        Course.find({
          fields: {
            courseid: true, name: true, description: true, activestatus:true , referancecode :true
          },
          where: whereCondition,
          limit: filter.pageLimit, skip: filter.pageStart, order: filter.order
        }, 
          function (err, courseDetails) { 
            if (err) {
              reject(err);
            } else {
              resolve(courseDetails);
            }
         }); 
      }).then(function (courseDetails) {
        if (courseDetails.length > 0) {
          Course.count({  isDeleted: 0, tenantKey: tenantKey }, function (err, result) { 
            if (err) {
              responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', null, err);
              callback(null, responseMsg);
            } else {
              responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', {
                list: courseDetails,
                totalCount: result
              }, '');
              callback(null, responseMsg);
            }
          });
        } else {
          
          responseMsg = commonHelper.commonMsg('failure', 404, 'No data found', {
            list: [],
            totalCount: 0
          });
          callback(null, responseMsg);
        }
  
      }).catch(function (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'Error!', null, err);
        callback(null, responseMsg);
      });

  }


  Course.remoteMethod('status',
    {
      http: {
        status: 201,
        errorStatus: 400,
        path: '/status/:status/:id',
        verb: 'post'
      },
      description: 'Change Course status (Active/Inactive)',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'status',
          type: 'number',
          description: 'Enter status to be changed (1:Active 0:Inactive)',
          required: true
        },
        {
          arg: 'id',
          type: 'number', 
          description: 'Enter Course id whose status should be changed',
          required: true
        }
      ],
      returns: [{
        arg: 'result'
      }]
    });
    
    Course.status = function (ctx, status, courseId, callback) {
      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      Course.update({
        'courseid': courseId,
        'tenantKey': tenantKey
      }, {
        'activestatus': status,
      },
      (err, res) => {
        if (err) {
          var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
          callback(null, responseMsg);
        } else if (res.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'Course',
            'moduleField': courseId,
            'activity': 'activity_change_status',
            'icon': 'fa-book',
            'value1': status,
            "value2": null,
            "note": null,
            "status": null,
            "tenantKey": tenantKey,
            'createdBy': loginUser,
          };
          
          commonHelper.activity(activities, function (err, rows) {
            if (err) {
              var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
              callback(null, responseMsg);
            } else {
              if (status) {
                var msg = "activated";
              } else {
                var msg = "deactivated";
              }
              var responseMsg = commonHelper.commonMsg('success', 200, 'Course ' + msg + ' successfully!', '', err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 404, 'Course not found!', '', err);
          callback(null, responseMsg);
        }
      }
      );
    }
    
    // Course.getTotalCount = function (ctx, callback) {
    //     var tenantKey = ctx.req.userDetail.tenantKey;
    //     Course.count({ tenantKey: tenantKey }, function (err, result) {
    //         if (err) {
    //             responseMsg = commonHelper.commonMsg('failure', '500', 'Something went wrong!', null, err);
    //             callback(responseMsg);
    //         } else {
    //             responseMsg = commonHelper.commonMsg('success', '200', 'Course list', result, '');
    //             callback(responseMsg);
    //         }
    //     });
    // };
    // Course.remoteMethod('getTotalCount', {
    //     http: {
    //         status: 200,
    //         errorStatus: 500,
    //         verb: 'get'
    //     },
    //     description: 'Get total no of courses',
    //     accepts: [
    //     {
    //           arg: 'ctx',
    //         type: 'object',
    //         http: {
    //           source: 'context'
    //         }
    //       }
    //     ],
    //     returns: {
    //         arg: 'data',
    //         type: [],
    //         root: true
    //     }
    // });
    
  
 Course.remoteMethod('modify', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/modify/:id',
      verb: 'post'
    },
    description: 'Update a instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'id',
        type: 'number',
        description: 'Please provide Course Id ',
        required: true
      },
      {
        arg: 'name',
        type: 'string',
        description: 'Enter Course Name',
        required: true
      },
      {
        arg: 'description',
        type: 'string',
        description: 'Enter Course Description',
        required: true
      },
      {
        arg: 'referanceCode',
        type: 'string',
        description:'Enter Course ReferanceCode',
        required: false
      },
      {
        arg: 'activeStatus',
        type: 'number',
        description: 'Enter status to be changed (1:Active 0:InActive)',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Course.modify = function ( ctx, courseId,name, description, referanceCode, activeStatus, callback) {
    
    var loginUser = ctx.req.userDetail.userId;
    var tenantKey = ctx.req.userDetail.tenantKey;
    var dt = dateTime.create();
    var formattedDateTime = dt.format('Y-m-d H:M:S');
    //var tenantKey='ac7153df458aefc2a93fd1a4f7513147';
    var saveData = {
      'name': name,
      'description': description,
      'referancecode': referanceCode,
      'activestatus': activeStatus,
      'modifieddate':formattedDateTime,
      'modifiedby':loginUser
    };
    Course.update({
        'courseid': courseId,
        'tenantKey': tenantKey
      }, saveData, (err1, updateRes)=> {
        // createRes = JSON.parse(JSON.stringify(createRes)); 
        //console.log("createRes    ------------->",createRes);
        // console.log("createRes", updateRes)
        if (err1) {
          // console.log("createRes err1", err1)
          var responseMsg = commonHelper.commonMsg('failure', 400, 'Course update failed', '', err1);
          callback(err1, responseMsg);
        }else{
          var activities = {
            "activity": ' Course updated',
            "module": 'course',
            "moduleId": updateRes.courseId,
            "tenantKey": tenantKey,
            "user": loginUser,
            "value1": null,
            "value2": null,
            "icon": 'fa-book',
            "note": null,
            "status": null,
            'createdBy': loginUser,
          };
          commonHelper.activity(activities, function (err, rows) {
            // console.log("activities", rows)
          
            if (err) {
              // console.log("activities err", err)
                responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
                callback(err, responseMsg)
            } else {
             
              responseMsg = commonHelper.commonMsg('success', 201, 'Course updated successfully', updateRes);
              callback(null, responseMsg);
            }
          });
        }
    });
  }
  

};
