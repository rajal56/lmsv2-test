'use strict';
var commonHelper = require('../../helper/commonhelper');
module.exports = function (Scorm) {
    Scorm.disableRemoteMethodByName('create');
    Scorm.disableRemoteMethodByName('replaceOrCreate');
    Scorm.disableRemoteMethodByName('upsertWithWhere');
    Scorm.disableRemoteMethodByName('change_stream');
    Scorm.disableRemoteMethodByName('upsert');
    Scorm.disableRemoteMethodByName('updateAll');
    Scorm.disableRemoteMethodByName('prototype.updateAttributes');
    Scorm.disableRemoteMethodByName('find');
    Scorm.disableRemoteMethodByName('findById');
    Scorm.disableRemoteMethodByName('findOne');
    Scorm.disableRemoteMethodByName('deleteById');
    Scorm.disableRemoteMethodByName('confirm');
    Scorm.disableRemoteMethodByName('count');
    Scorm.disableRemoteMethodByName('exists');
    Scorm.disableRemoteMethodByName('verify');
    Scorm.disableRemoteMethodByName('replaceById');
    Scorm.disableRemoteMethodByName('createChangeStream');
    Scorm.disableRemoteMethodByName('create');

    var responseMsg = '';

    Scorm.add = function (ctx, courseId, type, attempt, SCOInstanceID, status, callback) {
        var ds = Scorm.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var sql = "INSERT INTO tbl_scorm_details(courseId,userId,type,attempt,SCOInstanceID,`cmi.core._children`,`cmi.core.student_id`,`cmi.core.lesson_status`,tenantKey,createdDate) VALUES(?,?,?,?,?,?,?,?,?,now())";
        ds.connector.query(sql, [courseId, userId, type, attempt, SCOInstanceID, userId, userId, status, tenantKey], function (err, result) {
            if (err) {
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                callback(responseMsg);
            } else {
                console.log(result);
                responseMsg = commonHelper.commonMsg('success', '200', 'scorm added successfully', result, '');
                callback(responseMsg);
            }
        });
    };
    Scorm.remoteMethod('add', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'courseId',
                type: 'number',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'type',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'attempt',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'SCOInstanceID',
                type: 'number',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'status',
                type: 'string',
                'http': {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });


    Scorm.updateScorm = function (ctx, courseId, SCOInstanceID, data, callback) {
        var ds = Scorm.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;

        var where = "";
        where += (data.set['data.cmiUFF0EcoreUFF0Elesson_status'] != undefined) ? "`cmi.core.lesson_status`='" + data.set['data.cmiUFF0EcoreUFF0Elesson_status'] + "'," : '';
        where += (data.set['data.cmiUFF0EcoreUFF0Eexit'] != undefined) ? "`cmi.core.exit`='" + data.set['data.cmiUFF0EcoreUFF0Eexit'] + "'," : '';
        where += (data.set['data.cmiUFF0Esuspend_data'] != undefined) ? "`cmi.suspend_data`='" + data.set['data.cmiUFF0Esuspend_data'] + "'" : '';
        where = where.replace(/,\s*$/, "");
        var student_id = (data.set['data.cmiUFF0EcoreUFF0Estudent_id'] != undefined) ? data.set['data.cmiUFF0EcoreUFF0Estudent_id'] : data.query['data.cmiUFF0EcoreUFF0Estudent_id']
        if (where.length > 0) {
            var sql = "UPDATE tbl_scorm_details SET" + where + " WHERE `cmi.core.student_id` = " + student_id + " AND SCOInstanceID=" + SCOInstanceID + " AND courseId=" + courseId + " AND userId=" + userId + " AND tenantKey='" + tenantKey + "'";
            ds.connector.query(sql, function (err, result) {
                if (err) {
                    responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
                    callback(responseMsg);
                } else {
                    console.log(result);
                    responseMsg = commonHelper.commonMsg('success', '200', 'scorm updated successfully', data, '');
                    callback(responseMsg);
                }
            });
        } else {
            // data.success();
            responseMsg = commonHelper.commonMsg('success', '200', 'scorm updated successfully', data, '');
            callback(responseMsg);
        }


    };
    Scorm.remoteMethod('updateScorm', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of users',
        accepts: [
            {
                arg: 'ctx',
                type: 'object',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'courseId',
                type: 'number',
                http: {
                    source: 'context'
                }
            },
            {
                arg: 'SCOInstanceID',
                type: 'string',
                'http': {
                    source: 'context'
                },

            },
            {
                arg: 'data',
                type: 'object',
                http: {
                    source: 'context'
                },

            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });


    Scorm.list = function (ctx, SCOInstanceID, courseId, callback) {
        var ds = Scorm.dataSource;
        var userId = ctx.userId;
        var tenantKey = ctx.tenantKey;
        var sql = `SELECT * FROM tbl_scorm_details WHERE SCOInstanceID=${SCOInstanceID} AND courseId=${courseId} AND userId=${userId} AND tenantKey='${tenantKey}'`;
        ds.connector.query(sql, function (err, ScormList) {
            if (err) {
                console.error(err);
                responseMsg = commonHelper.commonMsg('failure', '500', 'Something went wrong!', null, err);
                callback(responseMsg);
            } else {
                var result = [];
                Object.keys(ScormList).forEach((key) => {
                    var rowData = ScormList[key];
                    var temp = []
                    var rawResult = {};
                    rawResult['scormId'] = rowData.scormid;
                    rawResult['courseId'] = rowData.courseid;
                    rawResult['userId'] = rowData.userid;
                    rawResult['attempt'] = rowData.attempt;
                    rawResult['SCOInstanceID'] = rowData.scoinstanceid;
                    rawResult['video_status'] = rowData.video_status;
                    rawResult['createdDate'] = rowData.createddate;
                    rawResult['modifiedDate'] = rowData.modifieddate;

                    result.push(rawResult);

                });
                responseMsg = commonHelper.commonMsg('success', '200', 'Scorm list', result, '');
                callback(responseMsg);
            }
            // if (err) {
            //     responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, err);
            //     callback(responseMsg);
            // } else {
            //     console.log(result);
            //     responseMsg = commonHelper.commonMsg('success', '200', 'scorm added successfully', result, '');
            //     callback(responseMsg);
            // }
        });
        // Scorm.find({
        //     where: { scoinstanceid: SCOInstanceID, courseid: courseId, userid: userId, tenantKey: tenantKey }
        // }, function (err, ScormList) {
        //     if (err) {
        //         console.error(err);
        //         responseMsg = commonHelper.commonMsg('failure', '500', 'Something went wrong!', null, err);
        //         callback(responseMsg);
        //     } else {
        //         var result = [];
        //         Object.keys(ScormList).forEach((key) => {
        //             var rowData = ScormList[key];
        //             var temp = []
        //             var rawResult = {};
        //             rawResult['scormId'] = rowData.scormid;
        //             rawResult['courseId'] = rowData.courseid;
        //             rawResult['userId'] = rowData.userid;
        //             rawResult['attempt'] = rowData.attempt;
        //             rawResult['SCOInstanceID'] = rowData.scoinstanceid;
        //             rawResult['video_status'] = rowData.video_status;
        //             rawResult['createdDate'] = rowData.createddate;
        //             rawResult['modifiedDate'] = rowData.modifieddate;

        //             result.push(rawResult);

        //         });
        //         responseMsg = commonHelper.commonMsg('success', '200', 'Scorm list', result, '');
        //         callback(responseMsg);
        //     }
        // });
    };
    Scorm.remoteMethod('list', {
        http: {
            status: 200,
            errorStatus: 500,
            verb: 'get'
        },
        description: 'Get list of Scorms',
        accepts: [
            {
                are: 'ctx',
                type: 'object',
                http: {
                    sourcr: 'context'
                }
            },

            {
                arg: 'SCOInstanceID',
                type: 'number',
                'http': {
                    source: 'contex'
                },
                description: '',
            },

            {
                arg: 'courseId',
                type: 'number',
                'http': {
                    source: 'contex'
                },
                description: '',
            }
        ],
        returns: {
            arg: 'data',
            type: [],
            root: true
        }
    });


};
