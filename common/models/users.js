'use strict';
var crypto = require('crypto');
var app = require('../../server/server');
var commonHelper = require('../../helper/commonhelper');
var jwt = require('jsonwebtoken');
var constants = require('../../helper/constant');
var sendMail = require('../../helper/sendMail');
var config = require('./../../server/config');
var dateTime = require('node-datetime');
var responseMsg;

module.exports = function (Users) {
  Users.disableRemoteMethodByName('create');
  Users.disableRemoteMethodByName('replaceOrCreate');
  Users.disableRemoteMethodByName('upsertWithWhere');
  Users.disableRemoteMethodByName('change_stream');
  Users.disableRemoteMethodByName('upsert');
  Users.disableRemoteMethodByName('updateAll');
  Users.disableRemoteMethodByName('prototype.updateAttributes');
  Users.disableRemoteMethodByName('find');
  Users.disableRemoteMethodByName('findById');
  Users.disableRemoteMethodByName('findOne');
  Users.disableRemoteMethodByName('deleteById');
  Users.disableRemoteMethodByName('confirm');
  Users.disableRemoteMethodByName('count');
  Users.disableRemoteMethodByName('exists');
  Users.disableRemoteMethodByName('resetPassword');
  Users.disableRemoteMethodByName('prototype.__count__accessTokens');
  Users.disableRemoteMethodByName('prototype.__create__accessTokens');
  Users.disableRemoteMethodByName('prototype.__delete__accessTokens');
  Users.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
  Users.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Users.disableRemoteMethodByName('prototype.__get__accessTokens');
  Users.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Users.disableRemoteMethodByName('replaceById');
  Users.disableRemoteMethodByName('createChangeStream');
  Users.disableRemoteMethodByName('change-password');
  Users.disableRemoteMethodByName('reset-password');
  Users.disableRemoteMethodByName('verify');

  // check password login time ;
  Users.prototype.hasPassword = function (plain, fn) {
    fn = fn || utils.createPromiseCallback();
    if (this.password && plain) {
      var hash = crypto.createHash(constants.passwordEncrpt.shaKey);
      hash.update(plain + constants.passwordEncrpt.key);
      var value = hash.digest(constants.passwordEncrpt.hex);
      const isMatch = this.password === value;
      fn(null, isMatch);
    } else {
      fn(null, false);
    }
    return fn.promise;
  };

  Users.login = function (ctx, email, password, domain, callback) {
    
    app.models.TenantCompanies.findOne({
      fields: {
        'tenantKey': true, 'isSetupDone': true,
        'subscriptionEndDate': true,
        'domain': true,
        'name': true
      },
      where: { 'domain': domain }
    }, function (error, resDomain) {
      if (error) {
        responseMsg = commonHelper.commonMsg('failure', 500, constants.sqlErrMsg, null, error);
        callback(null, responseMsg);
      } else {
      
       
        if (resDomain) {
          commonHelper.hashPassword(password, function (password) {
            if (password) {
              Users.findOne({
                where: {
                  'email': email,
                  'password': password,
                  'tenantKey': resDomain.companyKey
                }
              },
                function (err, user) {
                 
                  if (err) {
                    responseMsg = commonHelper.commonMsg('failure', 500, constants.sqlErrMsg, null, err);
                    callback(null, responseMsg);
                  } else if (user) {
                    
                    if (user.activated && !user.banned) {
                      var userDetails = {
                        userId: user.userId,
                        tenantKey: user.tenantKey,
                        isOwner: user.isOwner,
                        userType: user.userType
                      };
                      var dateTime = new Date();
                      //moment().format('Y-MM-DD H:mm:ss')
                      Users.update({ userId: user.userId, tenantKey: user.tenantKey, isDeleted: 0 }, {
                        lastLogin: dateTime
                      }, function (err, updateLoginTime) {
                        if (err) {
                          responseMsg = commonHelper.commonMsg('failure', '500', constants.sqlErrMsg, null, err);
                          callback(null, responseMsg);
                        } else {
                          
                          var token = jwt.sign({ userDetails: userDetails }, config.JWT.secret, { expiresIn: config.JWT.expiresIn });
                          userDetails.accessToken = token;
                          if (resDomain.istrialstarted) {
                            const now = new Date();
                            const trialEndDate = new Date(resDomain.trialPeriodEndDate);
                            const subscriptionEndDate = new Date(resDomain.subscriptionEndDate);

                            if (trialEndDate > now || subscriptionEndDate > now) {
                              var isSubscriptionOver = 0;
                            } else {
                              var isSubscriptionOver = 1;
                            }
                          }
                          if (userDetails.tenantKey === 'ac7153df458aefc2a93fd1a4f7513147') {
                            userDetails.isPBM = 1;
                          } else {
                            userDetails.isPBM = 0;
                          }
                         
                          app.models.Session.create({
                            userId: user.userId,
                            token: token,
                            isMobile: 0,
                            tenantKey: userDetails.tenantKey
                          }, function (err3, tokenUpdate) {
                            if (err3) {
                              responseMsg = commonHelper.commonMsg('failure', '500', constants.sqlErrMsg, null, err3);
                              callback(null, responseMsg);
                            } else {
                              ctx.req.userDetail = userDetails;
                              delete userDetails.tenantKey;
                              responseMsg = commonHelper.commonMsg('success', '200', 'Login successfully', userDetails, '');
                              callback(null, responseMsg);
                            }
                          });
                        }
                      });
                    } else {
                      if (user.isOwner) {
                        var responseMsg = commonHelper.commonMsg('failure', '403', 'Please activate your account in order to log into the system. Follow the instructions emailed during registration', {
                          isActive: 0,
                          userId: user.userId,
                          emailKey: user.newEmailKey
                        }, err);
                        callback(null, responseMsg);
                      } else {
                        var responseMsg = commonHelper.commonMsg('failure', '403', 'Your account is suspended. Please contact your administrator!', {
                          isActive: 0
                        }, err);
                        callback(null, responseMsg);
                      }
                    }
                  } else {
                    var responseMsg = commonHelper.commonMsg('failure', 404, 'User not found!', null, err);
                    callback(null, responseMsg);
                  }
                }
              );
            } else {
            
              var responseMsg = commonHelper.commonMsg('failure', 404, 'User not found!', null, err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 503, 'Domain does not exists');
          callback(null, responseMsg);
        }
      }
    });
  }

  Users.remoteMethod('login', {
    http: {
      verb: 'post'
    },
    description: 'Login of users',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context',
        },
      },
      {
        arg: 'email',
        type: 'string',
        required: true,
        description: 'Enter email'
      },
      {
        arg: 'password',
        type: 'string',
        required: true,
        description: 'Enter password'
      },
      {
        arg: 'domain',
        type: 'string',
        required: true,
        description: 'Enter domain'
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.remoteMethod('refreshToken', {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/refreshToken',
      verb: 'get',
    },
    description: 'Generate new access token',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context',
        },
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true,
    },
  });

  Users.refreshToken = function (ctx, callback) {
    var token = ctx.req.headers.authorization ? ctx.req.headers.authorization : ctx.req.query.access_token;
    var originalDecoded = jwt.decode(token, { complete: true });
    console.log("toke", token, ctx.req.query)
    var newToken = jwt.refresh(originalDecoded, config.JWT.expiresIn, config.JWT.secret);
    var userId = originalDecoded.payload.userDetails.userId.toString();
    var responseMsg;
    app.models.Session.findOne({ token: token }, function (error, sessionRes) {
      if (error) {
        responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, error);
        callback(responseMsg);
      } else {
        app.models.Session.update({ token: token }, { token: newToken, oldToken: token }, function (error, response) {
          if (error) {
            responseMsg = commonHelper.commonMsg('failure', '500', 'Something is wrong', null, error);
            callback(responseMsg);
          } else {
            var sockets = _.where(app.io.sockets.in(userId).sockets, { loginToken: token });
            sockets.forEach((scoket) => {
              scoket.loginToken = newToken;
            });
            var data = {
              accessToken: newToken,
            };
            responseMsg = commonHelper.commonMsg('success', '200', 'Access token', data, '');
            callback(null, responseMsg);
          }
        });
      }
    });
  };

  Users.list = function (ctx, filter, callback) {

      var loginUser = ctx.req.userDetail.userId;
      var tenantKey = ctx.req.userDetail.tenantKey;
      var responseMsg, whereCondition;
      let promise = new Promise(function (resolve, reject) {
        
        if(filter.searchKey){
          whereCondition ={
            and: [{
                or: [{
                  fullName: {
                    like: '%'+filter.searchKey+'%'
                  }
                }, {
                  email: {
                    like: '%'+filter.searchKey+'%'
                  }
                },{
                  mobile: {
                    like: '%'+filter.searchKey+'%'
                  }
                }]
              },
              {
                tenantKey: tenantKey, isDeleted: 0
              }
            ],
          }
        }else{
          whereCondition = {tenantKey: tenantKey, isDeleted: 0};
        }
  
        Users.find({
          fields: {
            userId: true, fullName: true, email: true, mobile: true, userType: true, activated: true
          },
          where: whereCondition,
          limit: filter.pageLimit, skip: filter.pageStart, order: filter.order
        }, 
          function (err, userDetails) {
            if (err) {
              reject(err);
            } else {
              resolve(userDetails);
            }
         });
      }).then(function (userDetails) {
        if (userDetails.length > 0) {
          Users.count({  isDeleted: 0, tenantKey: tenantKey }, function (err, result) {
            if (err) {
              responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', null, err);
              callback(null, responseMsg);
            } else {
              responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', {
                list: userDetails,
                totalCount: result
              }, '');
              callback(null, responseMsg);
            }
          });
        } else {
          
          responseMsg = commonHelper.commonMsg('failure', 404, 'No data found', {
            list: [],
            totalCount: 0
          });
          callback(null, responseMsg);
        }
  
      }).catch(function (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'Error!', null, err);
        callback(null, responseMsg);
      });

  }
  
  Users.remoteMethod('list', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get list of users',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'filter',
        type: 'object',
        'http': {
          source: 'query'
        },
        description: 'Filter defining order, start, limit and search - must be a JSON - encoded string Example format :  {"limit":10,"start":0,"order":"date DESC","search":"xyz"}',
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.remoteMethod('add', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Create a new instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'fullname',
        type: 'string',
        description: 'Please provide full name of employee',
        required: true
      },
      {
        arg: 'employeeId',
        type: 'string',
        description: 'Enter employee Id',
        required: true
      },
      {
        arg: 'email',
        type: 'string',
        description: 'Must be valid email',
        required: true
      },
      {
        arg: 'password',
        type: 'string',
        description:'Password length must be greater than 5 characters',
        required: true
      },
      {
        arg: 'mobile',
        type: 'string',
        description:'Enter mobile',
        required: false
      },
      {
        arg: 'userType',
        type: 'number',
        description: 'Please provide user type (1: Admin, 2:Instructor, 3:User)',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.add = function ( ctx, fullName, employeeId, mobile, email, password, userType, callback) {
    
    //var loginUser = ctx.userId; ctx, 
    var loginUser = 1;
    //var tenantKey=ctx.tenantKey;
    var tenantKey='ac7153df458aefc2a93fd1a4f7513147';
   

    console.log("new data          ",   fullName, employeeId, mobile, email, password, userType)
    //return false;
    commonHelper.hashPassword(password, function (password) {

      var whereEmail = " `email` = '" + email + "' and `tenantKey` = '" + tenantKey + "'";
      var id = null;
      new Promise(function (resolve, reject) {
        commonHelper.checkUpdate('tbl_users', whereEmail, id, (checkUsers) => {
          if (checkUsers) {
            var responseMsg = commonHelper.commonMsg('failure', 400, 'Employee email already exists', '', null);
            //responseMsg.data.EmployeeEmailAlreadyPresent = 1;
            resolve(responseMsg);
          } else {
            var responseMsg = 0;
            resolve(responseMsg);
          }
        });
      }).then(function (responseMsg) {
        if (responseMsg) {
          callback(null, responseMsg);
        }else{
          var dt = dateTime.create();
          var formattedDateTime = dt.format('Y-m-d H:M:S');
          Users.create({
            "fullName": fullName,
            "email": email,
            "password": password,
            "mobile": mobile,
            "userType": userType,
            "activated": 1,
            "tenantKey": tenantKey,
            "isDeleted": 0,
            "banReason":2,
            "createdBy":loginUser,
            "deletedDateTime":formattedDateTime
          },
          function (err1, createRes) {
           // createRes = JSON.parse(JSON.stringify(createRes)); 
            //console.log("createRes    ------------->",createRes);
           // console.log("createRes", createRes)
            if (err1) {
             // console.log("createRes err1", err1)
              var responseMsg = commonHelper.commonMsg('failure', 400, 'Employee create failed', '', err1);
              callback(err1, responseMsg);
            }else{
              var activities = {
                "activity": 'New user created',
                "module": 'user',
                "moduleId": createRes.userId,
                "tenantKey": tenantKey,
                "user": loginUser,
                "value1": email,
                "value2": null,
                "icon": 'fa-user',
                "note": null,
                "status": null,
                'createdBy': loginUser,
              };
              commonHelper.activity(activities, function (err, rows) {
               // console.log("activities", rows)
             
                if (err) {
                 // console.log("activities err", err)
                    responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
                    callback(err, responseMsg)
                } else {
                  app.models.EmailTemplates.find({
                    fields: {
                      subject: true,
                      templateBody: true
                    },
                    where: {
                      tenantKey: tenantKey,
                      emailGroup: 'welcome_email'
                    }
                  },
                  function (templateErr, templateDetails) {
                    if (err) {
                      responseMsg = commonHelper.commonMsg('failure', 500, 'Acitivity not stored', '', templateErr)
                      callback(err, responseMsg)
                    } else {
                      var userData = {
                        "fullName": fullName,
                        "email": email,
                        "password": password,
                        "mobile": mobile,
                        "userType": userType,
                        "activated": 1,
                        //"tenantKey": tenantKey,
                        "isDeleted": 0,
                        "banReason":2,
                        "createdBy":loginUser,
                        "deletedDateTime":null
                      }
                      userData.originalPass = password;
                      sendMail.sendMail('welcome_email', email, userData, templateDetails);
                      delete userData['originalPass'];
                      console.log("userData    ------------->",userData);
                      responseMsg = commonHelper.commonMsg('success', 201, 'Employee added successfully', userData);
                      callback(null, responseMsg);
                    }
                  });
                }
              });
            }
          });
        }
      }).catch(function (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'Error!', null, err);
        callback(null, responseMsg);
      });
    });
  }

  Users.remoteMethod('logout', {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/logout',
      verb: 'post',
    },
    description: 'logout',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context',
        },
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true,
    },
  });

  Users.logout = function (ctx, callback) {

    console.log("===================> ", ctx.userDetail);
    //return false;
    //var token = ctx.req.headers.authorization ? ctx.req.headers.authorization : ctx.req.query.access_token;
    var responseMsg;
    var token = ctx.accessToken;


    app.models.Session.findOne({
      where: {
        token: token
      }
    }, function (error, sessionRes) {
      if (error) {
        responseMsg = commonHelper.commonMsg('failure', '500', 'Logout not done', null, error);
        callback(responseMsg);
      } else {
        if (sessionRes) {
          app.models.Session.destroyById(sessionRes.sessionId, function (error, response) {
            if (error) {
              responseMsg = commonHelper.commonMsg('failure', '500', 'Logout not done', null, error);
              callback(responseMsg);
            } else {
              responseMsg = commonHelper.commonMsg('success', '200', 'You have logout successfully', null, '');
              callback(null, responseMsg);
            }
          });
        } else {
          responseMsg = commonHelper.commonMsg('success', '200', 'You have logout successfully', null, '');
          callback(null, responseMsg);
        }

      }
    });
  };

  function encrypt(text) {
    var key = new Buffer(constants.secret.key, constants.secret.encryption);
    var iv = new Buffer(constants.secret.iv, constants.secret.encryption);
    var cipher = crypto.createCipheriv(constants.secret.cypherCode, key, iv);
    var crypted = cipher.update(text.toString(), constants.secret.encryption, constants.secret.decryption);
    crypted += cipher.final(constants.secret.decryption);
    crypted = new Buffer(crypted, constants.secret.decryption).toString('hex');
    return crypted;
  }

  function decrypt(token) {
    var key = new Buffer(constants.secret.key, constants.secret.encryption);
    var iv = new Buffer(constants.secret.iv, constants.secret.encryption);
    var crypted = new Buffer(token, 'hex').toString(constants.secret.decryption);
    var decipher = crypto.createDecipheriv(constants.secret.cypherCode, key, iv);
    var decoded = decipher.update(crypted, constants.secret.decryption, constants.secret.encryption);
    try {
      decoded += decipher.final(constants.secret.encryption);
      return decoded;
    } catch (Exception) {
      logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Error in cron decryption', Exception);
      return false;
    }
  }

  Users.remoteMethod(
    'delete', {
      http: {
        status: 200,
        errorStatus: 400,
        path: '/delete/:id',
        verb: 'delete'
      },
      description: 'Delete User',
      accepts: [
        {
          arg: 'ctx',
          type: 'object',
          http: {
            source: 'context'
          }
        },
        {
          arg: 'id',
          type: 'string',
          description: 'Enter user id\'s that need to be deleted (use comma separated values for multiple users)',
          required: true
        }
      ],
      returns: [

        {
          arg: "result",
          http: {
            source: 'result'
          }
        }
      ]
    }
  );

  Users.delete = function (ctx, userId, callback) {
    
    var loginUser = ctx.userId;
    var companyKey = ctx.tenantKey;
    var responseMsg = '';
    var dt = dateTime.create();
    var formattedDateTime = dt.format('Y-m-d H:M:S');
    Users.update({
      'userId': userId,
      'tenantKey': companyKey
    }, {
      'isDeleted': 1,
      'deletedDateTime': formattedDateTime,
      'activated': 0
    },
      function (err, result) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'User delete failed ', '', err);
          callback(null, responseMsg);
        } else {
          if (result.count === 1) {

            var activities = {
              'user': loginUser,
              'module': 'user',
              'moduleField': userId,
              'activity': 'activity_delete_user',
              'value1': userId,
              "value2": null,
              "icon": 'fa-circle-o',
              "note": null,
              "status": null,
              "tenantKey": companyKey,
              'createdBy': loginUser,
            };
            //save Acitivty
            commonHelper.activity(activities, function (err1, rows) {
              if (err1) {

                responseMsg = commonHelper.commonMsg('failure', 500, ' User delete failed ', '', err1);
              } else {

                responseMsg = commonHelper.commonMsg('success', 200, ' User deleted successfully ', result);
              }
              callback(null, responseMsg);
            });
          } else {
            responseMsg = commonHelper.commonMsg('failure', 404, 'User not found ');
            callback(null, responseMsg);
          }
        }
      });
  }

  Users.remoteMethod('ban', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Ban User from system',
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },
    {
      arg: 'ban',
      type: 'number',
      description: 'Ban/unban user (1:Ban 0:Unban)',
      required: true
    },
    {
      arg: 'banReason',
      type: 'string',
      description: 'Ban/unban reason',
      required: true
    },
    {
      arg: 'userId',
      type: 'number',
      description: 'Enter user id whos status should be changed',
      required: true
    }
    ],
    returns: [{
      arg: 'result'
    }]
  });

  Users.ban = function (ctx, ban, banReason, userId, callback) {
    var loginUser = ctx.req.userDetail.userId;
    var tenantKey = ctx.req.userDetail.tenantKey;
    Users.update({
      'userId': userId,
      'tenantKey': tenantKey
    }, {
      'banned': ban,
      'banReason': banReason,
    },
      (err, res) => {
        if (err) {
          console.log(err);
          var responseMsg = commonHelper.commonMsg('failure',500,  null, '', err);
          callback(null, responseMsg);
        } else if (res.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'user',
            'moduleField': userId,
            'activity': 'activity_change_status',
            'icon': 'fa-user',
            'value1': ban,
            "value2": null,
            "icon": 'fa-user',
            "note": null,
            "status": null,
            "tenantKey": tenantKey,
            'createdBy': loginUser,
          };

          commonHelper.activity(activities, function (err, rows) {
            if (err) {
              var responseMsg = commonHelper.commonMsg('failure', 500,  null, '', err);
              callback(null, responseMsg);
            } else {
              if (ban) {
                var msg = "banned";
              } else {
                var msg = "unbanned";
              }
              var responseMsg = commonHelper.commonMsg('success', 200, 'User ' + msg + ' successfully!', '', err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 404, 'User not found!');
          callback(null, responseMsg);
        }
      }
    );
  }

  Users.remoteMethod('status',
  {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/status/:status/:id',
      verb: 'post'
    },
    description: 'Change users status (Active/Deactive)',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'status',
        type: 'number',
        description: 'Enter status to be changed (1:Activate 0:Deactivate)',
        required: true
      },
      {
        arg: 'id',
        type: 'number',
        description: 'Enter user id whos status should be changed',
        required: true
      }
    ],
    returns: [{
      arg: 'result'
    }]
  });

  Users.status = function (ctx, status, userId, callback) {
    var loginUser = ctx.req.userDetail.userId;
    var tenantKey = ctx.req.userDetail.tenantKey;
    Users.update({
      'userId': userId,
      'tenantKey': tenantKey
    }, {
      'activated': status,
    },
      (err, res) => {
        if (err) {
          console.log(err);
          var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
          callback(null, responseMsg);
        } else if (res.count > 0) {
          var activities = {
            'user': loginUser,
            'module': 'user',
            'moduleField': userId,
            'activity': 'activity_change_status',
            'icon': 'fa-user',
            'value1': status,
            "value2": null,
            "icon": 'fa-user',
            "note": null,
            "status": null,
            "tenantKey": tenantKey,
            'createdBy': loginUser,
          };

          commonHelper.activity(activities, function (err, rows) {
            if (err) {
              var responseMsg = commonHelper.commonMsg('failure', 500, null, '', err);
              callback(null, responseMsg);
            } else {
              if (status) {
                var msg = "activated";
              } else {
                var msg = "deactivated";
              }
              var responseMsg = commonHelper.commonMsg('success', 200, 'User ' + msg + ' successfully!', '', err);
              callback(null, responseMsg);
            }
          });
        } else {
          var responseMsg = commonHelper.commonMsg('failure', 404, 'User not found!', '', err);
          callback(null, responseMsg);
        }
      }
    );
  }

  Users.remoteMethod('detailsById', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get user details',
    accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'userId',
        type: 'string',
        description: 'Enter user id to fetch details',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.detailsById = function (ctx, userId, callback) {
    var companyKey = ctx.tenantKey;
    var responseMsg;
    Users.findOne({
      fields: { tenantKey: false, isdeleted: false, deletedatetime: false },
      where: {
        userId: userId,
        isdeleted: 0,
        tenantKey: companyKey,
      },
    }, function (err, response) {
      if (err) {
        responseMsg = commonHelper.commonMsg('failure', 500, 'error', '', err);
      } else {
        if (response) {
          responseMsg = commonHelper.commonMsg('success', 200, 'User details fetched successfully', response);
        } else {
          responseMsg = commonHelper.commonMsg('success', 404, 'No user found', response);
        }
      }
      callback(null, responseMsg);
    });
  };

  Users.remoteMethod('updateProfile', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Update user profile',
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    }, {
      arg: 'fullName',
      type: 'string',
      description: 'Enter user full name',
      required: true
    }, {
      arg: 'phone',
      type: 'string',
      description: 'Enter user phone number',
      required: false
    }, {
      arg: 'profileImg',
      type: 'string',
      description: 'Enter profile picture',
      required: false
    }],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.updateProfile = function (ctx, fullName, phone, profileImg, callback) {
    var userId = ctx.req.userDetail.userId;
    var companyKey = ctx.req.userDetail.tenantKey;
    var responseMsg;
    var profileData = {
      'fullName': fullName,
      'phone': phone,
      'avatar': profileImg
    };
    app.models.Accountdetails.update(
      {
        'userId': userId,
        'isDeleted': 0,
        'tenantKey': companyKey,
      }, profileData, function (err, accountResult) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong', '', err);
          callback(null, responseMsg);
        } else {
          var param = {
            "activity": 'activity_update_profile',
            "module": 'settings',
            "moduleField": userId,
            "tenantKey": companyKey,
            "user": userId,
            "value1": 'User fullName -' + fullName,
            "value2": null,
            "icon": 'fa-life-ring',
            "note": null,
            "status": null,
            'createdBy': userId,
          };
          commonHelper.activity(param, function (err, obj) {
            if(err){
              responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong', '', err);
              callback(null, responseMsg);
            }
            else {
              responseMsg = commonHelper.commonMsg('success',200,  'User profile updated successfully', '');
              callback(null, responseMsg);
            }
          });
        }
      });
  };

  Users.remoteMethod('changePassword', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: 'Change password',
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    }, {
      arg: 'oldPassword',
      type: 'string',
      description: 'Enter old password',
      required: true
    }, {
      arg: 'newPassword',
      type: 'string',
      description: 'Enter new password',
      required: true
    }, {
      arg: 'confirmPassword',
      type: 'string',
      description: 'Enter confirm password',
      required: true
    }],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.changePassword = function (ctx, oldPassword, newPassword, confirmPassword, callback) {
    var userId = ctx.req.userDetail.userId;
    var companyKey = ctx.req.userDetail.tenantKey;
    var ds = Users.dataSource;
    var sql = 'SELECT * FROM tbl_users WHERE user_id = ? AND tenant_company_key = ? AND isDeleted = ?';
    commonHelper.hashPassword(oldPassword, function (password) {
      ds.connector.query(sql, [userId, companyKey, 0], function (err, userInfo) {
        if (err) {
          responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong', '', err);
          callback(null, responseMsg);
        } else {
          if (userInfo.length > 0 && userInfo[0].password === password) {
            if (newPassword === confirmPassword) {
              commonHelper.hashPassword(newPassword, function (usrPassword) {
                Users.update({
                  'email': userInfo[0].email,
                  'isdeleted': 0,
                  'tenantKey': companyKey
                }, {
                  'password': usrPassword,
                  'enPass': usrPassword
                }, function (err, response) {
                  if (err) {
                    responseMsg = commonHelper.commonMsg('failure', 500, 'Password not changed', '', err);
                    callback(null, responseMsg);
                  } else {
                    if (response.count > 0) {
                      var activities = {
                        'user': userId,
                        'module': 'Profile',
                        'moduleField': userId,
                        'activity': 'activity_change_password',
                        'icon': 'fa-usd',
                        'value1': userInfo[0].username,
                        'value2': null,
                        'notes': '',
                        'tenantKey': companyKey,
                        'createdBy': userId,
                      };
                      commonHelper.activity(activities, function (err, response) {
                        if (err) {
                          responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong', '', err);
                          callback(null, responseMsg);
                        } else {
                          responseMsg = commonHelper.commonMsg('success', 200, 'Password changed successfully', '');
                          callback(null, responseMsg);
                        }
                      });
                    }
                  }
                });
              });
            } else {
              responseMsg = commonHelper.commonMsg('failure', 404, 'New password and confirm password does not match!', '');
              callback(null, responseMsg);
            }
          } else {
            responseMsg = commonHelper.commonMsg('failure', 404, 'Old Password Does not Match!');
            callback(null, responseMsg);
          }
        }
      });
    });
  }

  Users.remoteMethod('userEmailExists', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Check if email exists',
    accepts: [{
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'email',
        type: 'string',
        required: true,
        description: 'Email id to be checked'
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.userEmailExists = function (ctx, email, callback) {
    Users.find({
      fields: { userId: true},
        where: {
          email: email,
          tenantKey :  ctx.req.userDetail.tenantKey
        }
      },
      function (err, chkEmailExists) {
        if (err) {
          console.error(err);
          var responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', '', err);
          callback(null, responseMsg);
        } else {
          if (chkEmailExists.length > 0) {
            var responseMsg = commonHelper.commonMsg('success', 200, 'Email exists', { emailExists:1});
            callback(null, responseMsg);
          }else{
            var responseMsg = commonHelper.commonMsg('failure', 404, 'Email does not exist', { emailExists:0});
            callback(null, responseMsg);
          }
        }

      });
  }

  Users.remoteMethod('modify', 
  {
    http: {
      status: 201,
      errorStatus: 400,
      path: '/modify/:id',
      verb: 'post'
    },
    description: 'Update a instance of the model and persist it into the data source.',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      },
      {
        arg: 'id',
        type: 'number',
        description: 'Please provide user ID of user',
        required: true
      },
      {
        arg: 'fullName',
        type: 'string',
        description: 'Please provide full name of employee',
        required: true
      },
      {
        arg: 'employeeId',
        type: 'string',
        description: 'Enter employee Id',
        required: true
      },
      {
        arg: 'mobile',
        type: 'string',
        description:'Enter mobile',
        required: false
      },
      {
        arg: 'userType',
        type: 'number',
        description: 'Please provide user type (1: Admin, 2:Instructor, 3:User)',
        required: true
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });

  Users.modify = function ( ctx, userId, fullName, employeeId, mobile, userType, callback) {
    
    var loginUser = ctx.userId;
    //var loginUser = 1;
    var tenantKey=ctx.tenantKey;
    //var tenantKey='ac7153df458aefc2a93fd1a4f7513147';
    var saveData = {
      'fullName': fullName,
      'employeeId': employeeId,
      'userType': userType,
      'mobile': mobile
    };
    Users.update({
        'userId': userId,
        'tenantKey': tenantKey
      }, saveData, (err1, updateRes)=> {
        // createRes = JSON.parse(JSON.stringify(createRes)); 
        //console.log("createRes    ------------->",createRes);
        console.log("createRes", updateRes)
        if (err1) {
          // console.log("createRes err1", err1)
          var responseMsg = commonHelper.commonMsg('failure', 400, 'User update failed', '', err1);
          callback(err1, responseMsg);
        }else{
          var activities = {
            "activity": ' user updated',
            "module": 'user',
            "moduleId": updateRes.userId,
            "tenantKey": tenantKey,
            "user": loginUser,
            "value1": null,
            "value2": null,
            "icon": 'fa-user',
            "note": null,
            "status": null,
            'createdBy': loginUser,
          };
          commonHelper.activity(activities, function (err, rows) {
            // console.log("activities", rows)
          
            if (err) {
              // console.log("activities err", err)
                responseMsg = commonHelper.commonMsg('failure', 400,'Acitivity not stored', '', err)
                callback(err, responseMsg)
            } else {
             
              responseMsg = commonHelper.commonMsg('success', 201, 'User updated successfully', updateRes);
              callback(null, responseMsg);
            }
          });
        }
    });
  }
  
  Users.dropdown = function (ctx, callback) {

    var loginUser = ctx.userId;
    var tenantKey = ctx.tenantKey;
    var responseMsg;

    Users.find({
      fields: {
      },
      where: {
        tenantKey: tenantKey,
        isDeleted: 0
      }
    },
      function (err, userDetails) {
        if (err) {
          console.error(err);
          responseMsg = commonHelper.commonMsg('failure', 500, 'Something went wrong!', null, err);
        } else {
          if(userDetails.length > 0){
            responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', {
              list: userDetails
            }, '');
            //responseMsg = commonHelper.commonMsg('success', 200, 'All list retrieved', userDetails);
          }else{
            responseMsg = commonHelper.commonMsg('failure', 404, 'All list retrieved', userDetails);
          }
          console.log(responseMsg)
        }
        callback(null, responseMsg);

      });
  }
  
  Users.remoteMethod('dropdown', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: 'Get list of users for dropdown',
    accepts: [
      {
        arg: 'ctx',
        type: 'object',
        http: {
          source: 'context'
        }
      }
    ],
    returns: {
      arg: 'data',
      type: [],
      root: true
    }
  });
};
