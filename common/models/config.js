'use strict';
var commonHelper = require('../../helper/commonhelper');
var app = require('../../server/server');
const nodemailer = require('nodemailer');
var constants = require('../../helper/constant');
var moment = require('moment');
var responseMsg;

module.exports = function (Config) {
  Config.disableRemoteMethodByName('create');
  Config.disableRemoteMethodByName('replaceOrCreate');
  Config.disableRemoteMethodByName('upsertWithWhere');
  Config.disableRemoteMethodByName('change_stream');
  Config.disableRemoteMethodByName('upsert');
  Config.disableRemoteMethodByName('updateAll');
  Config.disableRemoteMethodByName('prototype.updateAttributes');
  Config.disableRemoteMethodByName('find');
  Config.disableRemoteMethodByName('findById');
  Config.disableRemoteMethodByName('findOne');
  Config.disableRemoteMethodByName('deleteById');
  Config.disableRemoteMethodByName('confirm');
  Config.disableRemoteMethodByName('replaceById');
  Config.disableRemoteMethodByName('count');
  Config.disableRemoteMethodByName('exists');
  Config.disableRemoteMethodByName('exists_head');
  Config.disableRemoteMethodByName('createChangeStream');


  /***********Config list API starts here */
  Config.remoteMethod('list', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'get'
    },
    description: "List configuration settings",
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },
      {
        arg: 'configKey',
        type: 'string',
        description: 'Enter comma separated config keys',
        required: true
      }
    ],
    returns: [{
      arg: "result",
      http: {
        source: 'result'
      }
    }]
  });

  Config.list = function (ctx, configKeys, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;

    configKeys = configKeys.split(',');
    var whereClause = '';
    for (let index = 0; index < configKeys.length; index++) {
      if (index == 0) {
        whereClause = "config_key = '" + configKeys[index] + "'";
      } else {
        whereClause = whereClause + " OR config_key = '" + configKeys[index] + "'";
      }
    }
    var ds = Config.dataSource;
    var sql = "SELECT config_key, value FROM `tbl_config` WHERE ( " + whereClause + " ) AND tenant_company_key = ?";
    var params = [companyKey];
    ds.connector.query(sql, params, function (err, deleteClient) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - list : Unable to fetch the list ', err);
         responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
        callback(null, responseMsg);
      } else {
        deleteClient = JSON.stringify(deleteClient);
        deleteClient = JSON.parse(deleteClient);
        if (deleteClient.length > 0) {
           responseMsg = commonHelper.commonMsg('success', "Config items fetched successfully", deleteClient);
          callback(null, responseMsg);
        } else {
           responseMsg = commonHelper.commonMsg('failure', "No config items found");
          callback(null, responseMsg);
        }
      }
    });
  }
  /***********Config list API ends here */

  /***********Config company details API starts here */
  Config.remoteMethod('company', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: "Update company details",
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },
      {
        arg: 'companyName',
        type: 'string',
        description: 'Enter company name',
        required: true
      },
      {
        arg: 'companyLegalName',
        type: 'string',
        description: 'Enter company legal name',
        required: true
      },
      {
        arg: 'contactPerson',
        type: 'string',
        description: 'Enter contact person name',
        required: false
      },
      {
        arg: 'companyAddress',
        type: 'string',
        description: 'Enter address',
        required: true
      },
      {
        arg: 'companyCity',
        type: 'string',
        description: 'Enter city',
        required: true
      },
      {
        arg: 'companyState',
        type: 'string',
        description: 'Enter state',
        required: false
      },
      {
        arg: 'companyZipCode',
        type: 'string',
        description: 'Enter zip code',
        required: false,
      },
      {
        arg: 'companyCountry',
        type: 'string',
        description: 'Enter country',
        required: true
      },
      {
        arg: 'companyPhone',
        type: 'string',
        description: 'Enter phone number',
        required: true
      },
      {
        arg: 'pbxAddress',
        type: 'string',
        description: 'Enter pbx address',
        required: false
      },
      {
        arg: 'actonAddress',
        type: 'string',
        description: 'Enter comma separated config keys',
        required: false
      },
      {
        arg: 'companyEmail',
        type: 'string',
        description: 'Enter email',
        required: true
      },
      {
        arg: 'companyVat',
        type: 'string',
        description: 'Enter company vat',
        required: false
      },
      {
        arg: 'companyWebsite',
        type: 'string',
        description: 'Enter company website',
        required: false
      }
    ],
    returns: [{
      arg: "result",
      http: {
        source: 'result'
      }
    }]
  });

  Config.company = function (ctx, companyName, companyLegalName, contactPerson, companyAddress, companyCity, companyState, companyZipCode, companyCountry, companyPhone, pbxAddress, actonAddress, companyEmail, companyVat, companyWebsite, callback) {
    var loginUser = ctx.req.userDetail.userId;
    var companyKey = ctx.req.userDetail.tenantKey;
    var configArray = {
      'company_name': companyName,
      'company_legal_name': companyLegalName,
      'contact_person': contactPerson ? contactPerson : "",
      'company_address': companyAddress,
      'company_city': companyCity,
      'company_state': companyState,
      'company_zip_code': companyZipCode,
      'company_country': companyCountry,
      'company_phone': companyPhone,
      'pbx_address': pbxAddress ? pbxAddress : "",
      'acton_address': actonAddress ? actonAddress : "",
      'company_email': companyEmail,
      'company_vat': companyVat ? companyVat : "",
      'company_website': companyWebsite ? companyWebsite : ''
    };
    var dbName = 'philomath_' + companyName.replace(/ /g, "_").toLowerCase();
    var saveInTenantCompany = {
      'name': companyName,
      'contact': companyPhone,
      'email': companyEmail,
      'db_name': dbName
    };

    Object.keys(configArray).forEach((val, key, keyArr) => {
      var configKey = val;
    var configValue = configArray[val];
      Config.update({
          'tenantKey': companyKey,
          'configKey': configKey
        }, {
          'value': configValue
        }, (err1, obj) => {
          var jsLength = Object.keys(obj).length;
          if (err1) {
             responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err1);
            logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - company : Unable to update ', err1);
            callback(null, responseMsg);
          }
          if (Object.is(keyArr.length - 1, key)) {
            app.models.TenantCompanies.update({
              'companyKey': companyKey
            }, saveInTenantCompany, (error, savedInTenanctCompany) => {
              if (error) {
                 responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, error);
                logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - company : Unable to update in tenant comapnies ', error);
                 callback(null, responseMsg);
              } else {
                if (savedInTenanctCompany.count > 0) {
                   responseMsg = commonHelper.commonMsg('success', 'Company details updated successfully', '', null);
                  callback(null, responseMsg);
                } else {
                   responseMsg = commonHelper.commonMsg('failure', 'Company details updation failed', '', null);
                  callback(null, responseMsg);
                }
              }
            });
            var activities = {
              'user': loginUser,
              'module': 'settings',
              'moduleField': loginUser,
              'activity': 'activity_save_general_settings',
              'value1': contactPerson,
              "value2": null,
              "icon": 'fa-circle-o',
              "note": null,
              "status": null,
              "tenantKey": companyKey,
              'createdBy': loginUser,
            };
            commonHelper.activity(activities, function (err, rows) {
              if (err) {
                 responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
                logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - company : Unable to add activity for general settings  ', err);
                callback(null, responseMsg);
              }
            });
          }
        }
      );
    });
  }
  /***********Config company details API ends here */


  /***********Notification config API starts here */
  Config.remoteMethod('notification', {
    http: {
      status: 201,
      errorStatus: 400,
      verb: 'post'
    },
    description: "Update notification settings",
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },
      {
        arg: 'notificationObject',
        type: 'array',
        description: 'Enter notification object example [{"company_email" : "", "use_postmark" : "", "postmark_api_key" : "", "postmark_from_address" : "", "protocol" : "", "smtp_host" : "", "smtp_user" : "", "smtp_port" : "", "test_email_id" : "", "smtp_encription" : ""}]',
        required: true
      }
    ],
    returns: [{
      arg: "result",
      http: {
        source: 'result'
      }
    }]
  });


  /***********Config list API starts here */
  Config.remoteMethod('listAll', {
    http: {
      status: 200,
      errorStatus: 400,
      verb: 'get'
    },
    description: "List all configuration settings",
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    }
    ],
    returns: [{
      arg: "result",
      http: {
        source: 'result'
      }
    }]
  });

  
  Config.listAll = function (ctx, callback) {
    var companyKey = ctx.req.userDetail.tenantKey;
    Config.find({
      fields: {tenantKey: false, isDeleted: false, deleteDateTime: false},
      where: {
      tenantKey: companyKey,
      isDeleted: 0
    }}, function (err, response) {
      if (err) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - List All : Unable to fetch data  ', err);
        responseMsg = commonHelper.commonMsg('failure', 'Something went wrong', '', err);
        callback(null, responseMsg);
      } else {
        response = JSON.stringify(response);
        response = JSON.parse(response);
        responseMsg = commonHelper.commonMsg('success', 'Config All list got successfully', response, '');
        callback(null, responseMsg);
      }

    });

  }
  /***********Config list all API ends here */


  Config.remoteMethod('sendNotificationEmail', {
    http: {
      verb: 'post'
    },
    description: 'Email configuration settings',
    accepts: [{
      arg: 'ctx',
      type: 'object',
      http: {
        source: 'context'
      }
    },{
      arg: 'message',
      type: 'string',
      description: 'Enter message',
      required: true
    }, {
      arg: 'subject',
      type: 'string',
      description: 'Enter subject',
      required: true
    }, {
      arg: 'toEmailId',
      type: 'string',
      description: 'Enter to address',
      required: true
    }],
    returns: [{
      arg: 'result',
      http: {
        source: 'result'
      }
    }]
  });

  Config.sendNotificationEmail = function (ctx, message, subject, toEmailId, callback) {
    var status = 1;
    var companyKey = ctx.req.userDetail.tenantKey;
    var loginUser = ctx.req.userDetail.userId;

    var params = {};
    params.template = message;
    params.subject = subject;
    params.recipient = toEmailId;

    Config.find({
      fields: {
        configKey: true,
        value: true
      },
      where: {
        and: [{
          or: [{
            configKey: {
              like: '%smtp%'
            }
          }, {
            configKey: {
              like: '%company%'
            }
          }]
        },{
            tenantKey: 'ac7153df458aefc2a93fd1a4f7513147'
        }]
      }
    }, function (configError, configObject) {
      if (configError) {
        logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - Send notificatiom Email : Configuration Failed. Please check your server address, port, username and password and try again ', configError);
        responseMsg = commonHelper.commonMsg('failure', null, '', configError);
        callback(null, responseMsg);
      } else {
        if (configObject !== '') {
          var smtpHost, smtpPass, smtpPort, smtpUser, companyEmail;
          configObject.forEach(obj => {
            if(obj.configKey === 'smtp_host'
        )
          {
            smtpHost = obj.value;
          }
          if (obj.configKey === 'smtp_pass') {
            smtpPass = obj.value;
          }
          if (obj.configKey === 'smtp_port') {
            smtpPort = obj.value;
          }
          if (obj.configKey === 'smtp_user') {
            smtpUser = obj.value;
          }
          if (obj.configKey === 'company_notification_email') {
            companyEmail = obj.value;
          }
        });
          var mailOptions = {
            from: companyEmail,
            to: params.recipient,
            cc: params.cc,
            bcc: params.bcc,
            subject: params.subject,
            attachments: params.attachmentArr,
            html: params.template
          };
              //send mail via node mailer
              // create reusable transporter object using the default SMTP transport
              var transporter = nodemailer.createTransport({
                host: smtpHost,
                port: smtpPort,
                secure: true,
                auth: {
                  user: smtpUser,
                  pass: smtpPass
                }
              });
              transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                  logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - Send notificatiom Email : Configuration Failed. Please check your server address, port, username and password and try again ', error);
                  responseMsg = commonHelper.commonMsg('failure', 'Configuration Failed. Please check your server address, port, username and password and try again', '', error);
                  callback(null, responseMsg);
                } else {
                  var activities = {
                    'user': loginUser,
                    'module': 'Settings',
                    'moduleField': loginUser,
                    'activity': 'activity_send_settings_email',
                    'value1': null,
                    'value2': null,
                    'icon': 'fa-envelope',
                    'note': null,
                    'status': null,
                    'tenantKey': companyKey,
                    'createdBy': loginUser,
                  };
                  commonHelper.activity(activities, function (err, rows) {
                    if (err) {
                      logger.error(moment().format('YYYY-MM-DD hh:mm:ss') + ' Config - Send notificatiom Email : Activity for notification email not created ', err);
                       responseMsg = commonHelper.commonMsg('failure', constants.sqlErrMsg, null, err);
                      callback(null, responseMsg);
                    } else {
                      responseMsg = commonHelper.commonMsg('success', 'Test email sent successfully', info, '');
                      callback(null, responseMsg);
                    }
                  });
                }
              });
        } else{
          responseMsg = commonHelper.commonMsg('failure',constants.noData, '', '');
          callback(null, responseMsg);
        }
      }
    });
  };

};
