var fs = require('fs');
// const mysql = require('mysql');
var app = require('../../../server/server');
const { isObject } = require('util');
var scorm = {};
var Scorm = app.models.Scorm;
// var mdb = null;
// mdb = mysql.createConnection({
//     host: 'localhost',
//     user: 'admin',
//     password: 'admin',
//     database: 'lmsv2'
// });

// mdb.connect(err => {
//     if (err)
//         return (err);
//     // throw err;
//     // console.log("connected");
// });

function init() {
    // var config = JSON.parse(fs.readFileSync(__dirname + '/../config.json'));
    // console.log(config);
    // var mongo = require('mongodb');
    // var monk = require('monk');
    // mdb = monk(config.db.host + ':' + config.db.port + '/' + config.db.name, {
    //     username: config.db.user,
    //     password: config.db.password
    // });

    // return mdb;
};

scorm.getOne = function (data) {
    console.log("getOne called");

    // console.log(data.query.cmiUFF0EcoreUFF0Estudent_id);
    // var db = (mdb == null ? init() : mdb);
    var SCOInstanceID = (data.query.SCOInstanceID == undefined ? false : data.query.SCOInstanceID);
    var courseId = (data.query.courseId == undefined ? false : data.query.courseId);
    var ctx = (data.query.ctx == undefined ? false : data.query.ctx);
    Scorm.list(ctx, SCOInstanceID, courseId, function (response) {
        console.log(response);
        if (response.status == 'success') {
            data.success(response);
        } else {
            data.error(response.data);
        }
    });
    // // db.query('SELECT moduleFile FROM tbl_module')

    // db.query('select * from tbl_scorm_details where SCOInstanceID=' + SCOInstanceID + ' AND courseId=' + courseId + ' AND `cmi.core.student_id`=' + data.query.cmiUFF0EcoreUFF0Estudent_id, (err, row) => {

    //     if (!err) {
    //         console.log("no error");
    //         console.log(row);
    //         data.success(row[0]);
    //     } else {
    //         console.log("no error");
    //         console.log(err);
    //         data.error(err);
    //     }
    // });

};

scorm.insert = function (data) {
    console.log("insert called");
    // var db = (mdb == null ? init() : mdb);
    var SCOInstanceID = (data.data.data.SCOInstanceID == undefined ? false : data.data.data.SCOInstanceID);
    var courseId = (data.data.data.courseId == undefined ? false : data.data.data.courseId);
    var ctx = (data.data.data.ctx == undefined ? false : data.data.data.ctx);
    var type = 'open';
    var status = data.data.data.cmiUFF0EcoreUFF0Elesson_status;
    var attempt = 1;

    Scorm.add(ctx, courseId, type, attempt, SCOInstanceID, status, function (response) {
        console.log(response);
        if (response.status == 'success') {
            var id = response.data.insertId;
            data.success(id);
        } else {
            data.error(response.data)
        }
    });
    // db.query("INSERT INTO `tbl_scorm_details`(`courseId`, `userId`, `type`, `attempt`, `SCOInstanceID`,`cmi.core.student_id`,`cmi.core.lesson_status`) VALUES (" + courseId + "," + data.data.data.cmiUFF0EcoreUFF0Estudent_id + ",'open',1," + SCOInstanceID + "," + data.data.data.cmiUFF0EcoreUFF0Estudent_id + ",'" + data.data.data.cmiUFF0EcoreUFF0Elesson_status + "')", (err, row) => {
    //     if (!err) {
    //         console.log(row);
    //         id = row.insertId;
    //         data.success(id);
    //     } else {
    //         console.log(err);
    //         data.error(err);
    //     }
    // });
};

scorm.update = function (data) {
    console.log("update callled");
    console.log(data);
    var SCOInstanceID = (data.query.SCOInstanceID == undefined ? false : data.query.SCOInstanceID);
    var courseId = (data.query.courseId == undefined ? false : data.query.courseId);
    var ctx = (data.query.ctx == undefined ? false : data.query.ctx);
    console.log(typeof (data));
    Scorm.updateScorm(ctx, courseId, SCOInstanceID, data, function (response) {
        if (response.status == 'success') {
            data.success()
        } else {
            data.error(response.data);
        }
    });
    // var db = (mdb == null ? init() : mdb);
    // var SCOInstanceID = (data.query.SCOInstanceID == undefined ? false : data.query.SCOInstanceID);
    // var courseId = (data.query.courseId == undefined ? false : data.query.courseId);
    // var where = "";
    // where += (data.set['data.cmiUFF0EcoreUFF0Elesson_status'] != undefined) ? "`cmi.core.lesson_status`='" + data.set['data.cmiUFF0EcoreUFF0Elesson_status'] + "'," : '';
    // where += (data.set['data.cmiUFF0EcoreUFF0Eexit'] != undefined) ? "`cmi.core.exit`='" + data.set['data.cmiUFF0EcoreUFF0Eexit'] + "'," : '';
    // where += (data.set['data.cmiUFF0Esuspend_data'] != undefined) ? "`cmi.suspend_data`='" + data.set['data.cmiUFF0Esuspend_data'] + "'" : '';
    // where = where.replace(/,\s*$/, "");
    // // console.log("lates status====" + data.set['data.cmiUFF0EcoreUFF0Elesson_status']);
    // var student_id = (data.set['data.cmiUFF0EcoreUFF0Estudent_id'] != undefined) ? data.set['data.cmiUFF0EcoreUFF0Estudent_id'] : data.query['data.cmiUFF0EcoreUFF0Estudent_id']
    // if (where.length > 0) {
    //     db.query("UPDATE tbl_scorm_details SET" + where + " WHERE `cmi.core.student_id` = " + student_id + " AND SCOInstanceID=" + SCOInstanceID + " AND courseId=" + courseId + " AND userId=" + student_id, (err, row) => {
    //         if (!err) {
    //             console.log(row);
    //             data.success();
    //         } else {
    //             console.log(err);
    //             data.error(err);
    //         }
    //     });
    // } else {
    //     data.success();
    // }


};

module.exports = scorm;
