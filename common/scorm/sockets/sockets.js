var sockets = {};

sockets.load = function (io) {
    // console.log("inside sockets");
    // console.log(io);
    sockets.io = io;
    sockets.clients = 0;

    var scormSocket = require('./scorm');
    var room = scormSocket.start(io);
};

module.exports = sockets;