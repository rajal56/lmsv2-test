var course = require('../../common/service/course');
var modules = require('../../common/service/modules');
var user = require('../../common/service/user');
var category = require('../../common/service/category');
var team = require('../../common/service/team');
var scorm = require('../../common/service/scorm');

// var content = require('../../common/service/content');
var form = require('../../common/service/form');
var Formquestions = require('../../common/service/question');
var app = require("../server");
var session = require('express-session');



app.use(session({
    secret: 'lms2',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 28800000 }
}));

module.exports = function (app) {

    app.get('/sco/*', scorm.getScormFile);

    app.post('/api/commit/:sco/:SCOInstanceID/:courseId', scorm.scormCommit);

    app.get('/scorm/:id/:SCOInstanceID/:courseId', scorm.getScormPlayer);

    app.post('/scorm/api/initialize/:sco/:SCOInstanceID/:courseId', scorm.scormInitialize);

    app.get('/', (req, res) => {
        if (req.session.ctx) {
            res.redirect('/content');
        } else {
            res.redirect('/login');
        }
    });

    app.get('/login', (req, res) => {
        res.render('login');
    });


    app.get('/modules/settings', (req, res) => {
        res.render('settings');
    });
    app.get('/form-add/:type/:id', form.formAdd);
    app.post('/form_save', form.formSave);
    app.get('/question/:formId', Formquestions.questionAdd);
    app.post('/question_save', Formquestions.questionSave);
    app.get('/modules/:id', course.courseModule);
    app.post('/save_module/:type', modules.moduleSave);
    app.get('/logout', user.userLogout);
    app.post('/validate_login', user.userLogin);
    app.get('/index', course.courseList);
    app.get('/course_add', course.courseAdd);
    app.post('/course_save', course.courseSave);
    // app.post('/course_module', course.courseSave);
    app.get('/content', course.courseContent);
    app.get('/course/:id', course.courseReport);
    app.get('/course_edit/:id', course.courseEdit);
    app.post('/course_update', course.courseUpdate);
    app.get('/course_delete/:id', course.courseDelete);
    //app.get('/content', content.contentList);
    app.get('/users', user.usersList);
    app.get('/create_user', user.userAdd);
    app.post('/user_save', user.add);
    app.get('/user_edit/:id', user.edit);
    app.get('/user_delete/:id', user.delete);
    app.post('/user_update/:id', user.update);
    app.get('/category_list', category.list);
    app.get('/category_form', category.form);
    app.post('/category_save', category.add);
    app.get('/category_edit/:id', category.edit);
    app.get('/category_delete/:id', category.delete);
    app.get('/team_list', team.list);
    app.get('/team_form', team.form);
    app.post('/team_save', team.add);
    app.get('/team_edit/:id', team.edit);
    app.get('/team_delete/:id', team.delete);
    app.post('/team_update/:id', team.update);
}