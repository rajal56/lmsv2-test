'use strict';

var jwt = require('jsonwebtoken');
var config = require('./../config');
var app = require('../server');

module.exports = function () {
 
  return function authGateway(req, res, next) {
    if (req.headers.authorization || req.query.access_token) {
      let token = req.headers.authorization ? req.headers.authorization : req.query.access_token;
      validateAccessTokenInDB(token, function (responseValid) {
        if (responseValid) {
          jwt.verify(token, config.JWT.secret, function (err, verifyRes) {
            if (err) {
              if (err.name === 'TokenExpiredError') {
                if (req.baseUrl == '/api/Users/refreshToken') {
                  next();
                } else {
                  return res.status(419).json({message: 'Token Expired'});
                }
              } else {
                return res.status(401).json({message: 'Malformed User'});
              }
            } else {
              
              var decoded = jwt.decode(token);
              req.userDetail = decoded.userDetails;
              
              checkUserIsActive(req.userDetail, function(err, isActive){
                if(err){
                  return res.status(401).json({message: 'Malformed User'});
                }else {
                  if(isActive){
                    next();
                  }else {
                    removeInActiveUserSessions(req.userDetail.userId, function(err, response) {
                      return res.status(401).json({message: 'Malformed User'});
                    });
                  }
                }
              });
            }
          });
        } else {
          return res.status(401).json({message: 'Malformed User'});
        }
      });
    } else {
      return res.status(401).json({message: 'Malformed User'});
    }
  };

  function validateAccessTokenInDB(token, callback) {
    app.models.Session.findOne({where: {token: token}}, function (error, sessionRes) {
      if (error) {
        callback(false);
      } else {
        if (sessionRes) {
          callback(true);
        } else {
          app.models.Session.findOne({where: {oldToken: token}}, function (error, sessionOldTokenRes) {
            if (error) {
              callback(false);
            } else {
              if (sessionOldTokenRes) {
                callback(true);
              } else {
                callback(false);
              }
            }
          });
        }
      }
    });
  }

  function checkUserIsActive(userDetail, callback){
    app.models.Users.findOne({ where : { userId : userDetail.userId }}, function (error, user) {
      if(error){
        callback(error, null);
      }else {
        callback(null, user.activated);
      }
    });
  }

  function removeInActiveUserSessions(userId, callback){
    app.models.Session.destroyAll({userId: userId}, function (error, response) {
      if (error) {
        callback(false);
      } else {
        callback(true);
      }
    });
  }
};
