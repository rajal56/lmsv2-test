const path = require('path')

module.exports = {
    apps: [{
        name: 'app',
        script: 'server/server.js',
        instances: 1,
        autorestart: true,
	ignore_watch: 'node_modules',
        watch: process.env.NODE_ENV !== 'production' ? path.resolve(__dirname, 'server') : false,
        max_memory_restart: '1G'
    }]
}
